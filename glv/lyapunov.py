from glv import base
import numpy as np

class MLESeries(base.Observable):
    ''' Generates a time-series of the (local) maximum Lyapunov exponent by measuring the divergence 
    of a perturbed simulation.
    
    If the main simulation is run with <record_interval> steps between observable updates, then 
    the comparison between reference and perturbed simulation occurrs in intervals of <record_interval>;
    the perturbed simulation is renormalized every <record_interval> actual simulation steps.   

    Parameters
    ----------
    sys_pert
        dynamical system to simulate the perturbed 
    delta0
        magnitude of perturbation
    transient_steps
        number of MLEs discarded as transient in summary statistics   
    
    Attributes
    ----------
    vec_mle
        vector of MLE values
    global_mle
        average MLE over time series (discarding <transient_steps>)
    hist_mle 
        
    '''

    def __init__(self, delta0=10**-13, label=None):
        self.delta0 = delta0
        super().__init__(label)

    def _initialize(self, ds, N):
        self._vec_xref0 = ds.vec_x.copy()
        self._vec_xref1 = None
        
        self._step0 = ds.current_step
        self._step1 = None
        
        if not hasattr(self, '_vec_xpert0'): #continued from previous state
            self._vec_xpert0 = self._vec_xref0 + self.delta0 * np.ones(ds.S)/ np.sqrt(ds.S)
        self._vec_xpert1 = None

        self.vec_mle = np.zeros(N)
        self._n = 1
    
    def _update(self, ds):
        # save state
        self._vec_xref1 = ds.vec_x.copy()
        self._step1 = ds.current_step
        
        # reset state
        ds.current_step = self._step0
        #ds.vec_x = self._vec_xref0

        #perturb
        ds.vec_x = self._vec_xpert0
        
        # run perturbed sim
        steps = self._step1 - self._step0
        ds.step(steps)
        self._vec_xpert1 = ds.vec_x.copy()
        
        # calculate MLE
        vec_delta = self._vec_xpert1 - self._vec_xref1
        mle = 1/(ds.dt * steps) * np.log( np.linalg.norm(vec_delta) / self.delta0 )
        self.vec_mle[self._n] = mle 
        
        # swap old for new variables
        self._n += 1
        self._vec_xref0 = self._vec_xref1.copy()
        self._step0 = self._step1
        self._vec_xpert0 = self._vec_xref1 + self.delta0 * vec_delta / np.linalg.norm(vec_delta)

        # reset to beginning
        ds.vec_x = self._vec_xref1

    def _finalize(self, ds):
        pass
