'''
This module contains observables related to abundance distributions
'''

import numpy as np
from glv import model
from glv import base
from glv import metrics
from glv import fit
import warnings

class AbundanceDistributions(base.Observable):
    '''
    Represents the abundance fluctuation distribution AFD for each species, as well
    as the average distribution, and representations of the per-species deviations
    from the average; power-law fit to the average AFD distributions within set limits;

    Parameters
    ----------
    bins
        edges of abundnace bins (N+1 for N bins)
    logspace : bool
        set to True if bins are logarithmically spaced
    powerlaw_cutoff :
        left and right abundnace limits between which powerlaw slope is calculated for the distribution
    batch_size : int
        steps of data to accumulate before updating histogram counts (for inline generation method)

    Attributes
    ----------
    bins
        abundance bin edges (same as input)
    bins_mid
        midpoints of the abundance bins (on a log-scale if log_bins==True)
    mat_dist
        abundance distribution (row=species, col=bin)
    vec_dist
        specices-averaged abundance distribution
    nu
        fitted slope of powerlaw
    c 
        intersect of the line to match powerlaw slope in the log x vs log P space
    r2
        coefficient of determination of nu fit
    
    '''

    def __init__(self, bins, logspace=True, powerlaw_cutoff=[10**-6,10**-2], batch_size=1000, label=None):
        super().__init__(label)
        self.batch_size = batch_size
        self.bins = bins
        self.logspace = logspace

        if self.logspace:
            self.bins_mid =  10**( (np.log10(self.bins)[0:-1] + np.log10(self.bins)[1:])/2 )
        else:
            self.bins_mid = (self.bins[0:-1] + self.bins[1:])/2    

        self.powerlaw_cutoff = powerlaw_cutoff

    def _initialize(self, ds, N):        
        self.n_in_batch = 0
        self.mat_x = np.zeros(shape=(self.batch_size, ds.S))
        self.mat_x[0,:] = ds.vec_x
        self.mat_counts = np.zeros(shape=(ds.S, self.bins.size - 1))

    def _update(self, ds):        
        if self.n_in_batch == self.batch_size - 1:
            for i in range(ds.S):
                hist, _ = np.histogram(self.mat_x[:,i], bins=self.bins)
                self.mat_counts[i,:] += hist

            self.n_in_batch = 0
            
        else:
            self.n_in_batch += 1
        
        self.mat_x[self.n_in_batch, :] = ds.vec_x.copy()
          
  
    def _finalize(self, ds):   
        # if last batch was not completely filled filled
        if self.n_in_batch <= self.batch_size - 1:
            for i in range(ds.S):
                hist, _ = np.histogram(self.mat_x[0:self.n_in_batch+1, i], bins=self.bins)
                self.mat_counts[i,:] += hist

        self.mat_dist = (self.mat_counts / np.diff(self.bins) ) / np.sum(self.mat_counts,axis=1)[:,None]
        del self.mat_counts


    def from_trajectory(self, ds): 
        self.mat_dist = np.zeros(shape=(ds.S, self.bins.size - 1))
        for i in range(ds.mat_x.shape[1]):
            hist, _ = np.histogram(ds.mat_x[:, i], bins=self.bins, density=True)
            self.mat_dist[i,:] = hist
        
    @base.lazy_property
    def vec_dist(self):
        return np.mean(self.mat_dist, axis=0)

    @base.lazy_property
    def _power_law_fit(self):

        idx_low = np.sum(self.bins_mid < self.powerlaw_cutoff[0] )
        idx_high = np.sum(self.bins_mid < self.powerlaw_cutoff[1] )
                
        if np.amin(self.vec_dist[idx_low:idx_high]) <= 0:
            warnings.warn(f"Abundance distribution lacks full support within cutoffs!")
            return None
        else:
            vec_logP = np.log10(self.vec_dist[idx_low:idx_high])
            vec_logx = np.log10(self.bins_mid[idx_low:idx_high])

            lin_fit = fit.linear_fit(vec_logP, vec_logx)
            return lin_fit

    @base.lazy_property
    def nu(self):
        pl_fit = self._power_law_fit
        return pl_fit['k']    

    @base.lazy_property
    def c(self):
        pl_fit = self._power_law_fit
        return pl_fit['c']

    @base.lazy_property
    def r2(self):
        pl_fit = self._power_law_fit
        return pl_fit['r2']
    
    @base.lazy_property
    def vec_typicality(self):
        vec_dx = np.diff(self.bins)
        
        mat_F = np.cumsum(self.mat_dist * vec_dx, axis=1)
        vec_F = np.cumsum( self.vec_dist * vec_dx ) # average 
        vec_typ = 1 - np.amax( np.abs(mat_F - vec_F), axis=1 )

        return vec_typ

    def plot_on_axes(self, ax, **kwargs):
        kwargs.setdefault('color', 'grey')
        kwargs.setdefault('lw', 0.2)
        kwargs.setdefault('zorder', -2)
        kwargs.setdefault('alpha', 0.2)
        for i in range(self.mat_dist.shape[0]):
            ax.plot(self.bins_mid, self.mat_dist[i,:], **kwargs)
        ax.plot(self.bins_mid, self.vec_dist, color='k')


# class RankAbundanceDistribution(base.Observable):
#     ''' Time-averaged rank abundance distribution
    
#     Parameters
#         relative : consider relative abundances
#         r_min : lowest rank considered

#     Properties
#         vec : vector of abundances
#         vec_cum : cumulative distribution
#         vec_r : vector of ranks 
#     '''
    
#     def __init__(self, relative=True, r_min=None, label=None):
#         self.relative = relative
#         self.r_min = r_min
#         super().__init__(label)
        
        
#     def _initialize(self, ds, N):
      
#         self._vec = np.zeros(ds.S)[:self.r_min]
    
#         self.vec_r = (np.arange(ds.S) + 1)[:self.r_min]
        
#         self._count = 0
#         self._update(ds)

#     def _update(self, ds):
#         vec = ds.vec_x.copy()
#         if self.relative:
#             vec /= np.sum(vec)

#         if self.r_min is None:    
#             self._vec += np.sort(vec)[::-1]
#         else:
#              # last r_min elements are the r_min largest, sorted ascending
#             vec_sorted = np.partition(vec, np.arange(ds.S - self.r_min, ds.S))
#             # the largest r_min elements, sorted descending
#             vec_sorted = vec_sorted[:-self.r_min-1:-1] 
#             self._vec += vec_sorted
#         self._count += 1


#     def _finalize(self, ds):
#         self._vec /= self._count

#     @property
#     def vec(self):
#         return self._vec

#     @property
#     def vec_cum(self):
#         return np.cumsum(self._vec) 

#     @classmethod
#     def from_trajectory(ds):
#         pass

#     def plot_on_axes(self, ax, **kwargs):
#         ax.plot(self.vec_r / np.amax(self.vec_r), self.vec_cum, **kwargs)
