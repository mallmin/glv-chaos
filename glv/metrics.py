""""
Simple statistical metrics
"""

import numpy as np

def bray_curtis(x,y):
    if not (x.shape == y.shape):
        raise ValueError("incompatible dimensions")
        
    def _bc(x,y):
        return np.sum( 2 * np.minimum(x,y)) / np.sum(x + y)
                
    if x.ndim == y.ndim == 1:
        return _bc(x,y)
    elif x.ndim == y.ndim == 2:
        T = x.shape[0]
        vec_bc = np.zeros(T)
        for t in range(T):
             vec_bc[t] = _bc(x[t,:], y[t,:])
        return vec_bc
    else:
         raise ValueError("unsupported array dimension")
              

def eff_size(mat_x):
    return 1 / np.sum( relative_abundance(mat_x) ** 2, axis=1)

def similarity_to_final(mat_x):
        vec_x = mat_x[-1,:]
        vec_L = 2 * mat_x @ vec_x / ( vec_x @ vec_x + np.linalg.norm(mat_x,axis=1)**2 )
        return vec_L

def relative_abundance(mat_x):
    '''
    Converts absolute to relative abundance.
    '''
    mat_p = mat_x / np.sum(mat_x, axis=1)[:,None]
    return mat_p

def rank_idx(vec):
    return np.argsort(vec)[::-1]

def ranks(vec):
    '''
    Returns a vectors of ranks and and the vector of their values
    '''
    rank_idxs = np.argsort(vec)[::-1]
    vec_r = np.arange(vec.size) + 1
    return vec_r, vec[rank_idxs]