import time
import datetime
import warnings

class Tracker():
    '''Class to time tasks during code execution'''

    def __init__(self, depth=0, file=None, verbose=False):
        self.t0 = None
        self.t1 = None
        self.label = None
        self.verbose = verbose
        self.file = file
        
        self.depth = depth
        self.trackers = []
        
    def start(self, label):
        if self.t0 is not None:
            warnings.warn(f"Timer '{self.label}' already started")
        
        self.label = label
        self.t0 = time.time()
        start_str = self.depth*"  " + f"START: {self.label}" 
        if self.verbose:
            start_str += f"\t(at {datetime.datetime.now()})"        
        print(start_str, file=self.file)
        return self
        
    def start_sub(self, label):
        t = Tracker(verbose=self.verbose, depth=self.depth + 1)
        self.trackers.append(t)
        return t.start(label)
        
    def end(self, result=None):
        if self.t1 is not None:
            warnings.warn(f"Timer '{self.label}' already stopped")

        self.t1 = time.time()
        for t in self.trackers:
            if t.t1 is None:
                t.end()
        dt = self.dt        
        mins = int(dt / 60)
        secs = int(dt - mins*60)
        
        end_str = "  "*self.depth + "END:   " + (f"{mins}m{secs}s" if mins > 0 else f"{dt:.2f}s")
        if result is not None:
            end_str += f"{result}"
        if self.verbose:
            end_str += f"\t(at {datetime.datetime.now()})"
        
        print(end_str, file=self.file)

    @property
    def dt(self):
        if self.t1 is not None:
            return self.t1 - self.t0
        else:
            return None