"""
Module defining a basic interface for dynamical systems with fixed integration time step.
(And some utility functions)

`DynamicalSystem' is the simulation base class, and represents the evolution rule, model 
parameters, and the current state of the system. It contains methods to move the simulation
forward, generate a trajectory, etc.

An `Observable' represents any data that can be generated from a trajectory of the system.
In particular, trajectories themselves are `Observable's, implemented by `SystemSeries'.

An `Observable` can be implented so as to generate is data iteratively during simulation via
`generate_observable`, and/or be generated from a completed `SystemSeries` object. The 
first approach may conserve memory for cumulative statistics computed over long time series
where the full time series need not be stored. 

Specific dynamical systems inherit `DynamicalSystem' and implement the `_single_step` function.
They may also define sub-`SystemSeries' that in addition to the phase trajectory records
simulation parameters specific to `DynamicalSystem' sublcass that should be associated with
the stored trajectory.
"""

import numpy as np
from numpy.random import default_rng
from math import ceil
import datetime
import pickle
import colorsys


class DynamicalSystem():
    '''
    The `DynamicalSystem` represents the evolution rule, parameters, and the current state of the system.
    
    `vec_x` is an S-array of the current state of the main system; `vec_aux` is an M-array
    of the current state of auxiliary variables (e.g. noise or forcing)
    
    A particular dynamics is implemented by overloading the `single_step` function, which
    should update `vec_x` and `vec_aux` by one time-step of size `dt`.
    
    Attributes
    ----------
    S : int
        dimension of main state vector
    M : int
        dimension of auxiliary state vector
    dt : float
        integration time step
    dtype
        data type (eg float) of state variables
    current_step : int
        current discrete time step of simulation
    vec_x : numpy array
        current main state vector
    vec_aux : numpy array
        current auxiliary state vector
       
    '''

    def __init__(self, S=100, M=0, dt=0.01, dtype=np.float64):
        '''
        Initializes all attributes
        '''                
        self.S = S  # dimension      
        self.M = M
        self.dt = dt 
        self.dtype = dtype #same for x and aux for now
        self.vec_x = np.ones(S, self.dtype) / S #current state
        if M>0:
            self.vec_aux = np.zeros(M, dtype)
        else:
            self.vec_aux = None
        self.current_step = 0 #

        # 
        self.SeriesClass = SystemSeries
 
   
    def initialize_uniform(self, low=10**-30, high=1., seed=None):
        '''
        Initialize state vector from uniform distribution.
        
        Parameters
        ----------
        low : float
            lower bound
        high : float
            upper bound
        seed
            random seed used; if None draws from system entropy
        '''

        rng = np.random.default_rng(seed)
        self.vec_x = rng.uniform(low=low, high=high, size=self.S)
    

    def discard_transient(self, steps, check_NaN=True):
        '''
        Runs `steps` steps and checks if NaNs were generated
        
        Parameters
        ----------
        steps
            number of steps to discard
        check_NaN : bool
            whether to check each step for NaN values in vec_x

        Returns
        -------
        nondivergent : bool
            whether NaN was found in the main state vector
        '''
        
        found_NaN = False

        if check_NaN:
            for n in range(steps):
                self._single_step()
                if np.isnan(self.vec_x).any():
                    found_NaN = True
                    break
        else:
            self.step(steps=steps)
            found_NaN = np.isnan(self.vec_x).any()

        if not found_NaN:
            self.reset_time()

        return not found_NaN


    def reset_time(self):
        '''
        Resets time to zero
        '''

        self.current_step = 0


    def step(self, steps=1):
        '''
        Steps the system forward.

        Parameters
        ----------
        steps : int
            number of steps to move forward

        '''
        if steps == 1:
            self._single_step()
        else:
            for n in range(steps):
                self._single_step()


    def _single_step(self):
        '''
        Defines the dynamical rule: result of a single step forward.
        
        Should update `vec_x`, `vec_aux`, and perform `current_step += 1`.
        '''

        raise NotImplementedError("Must be implemented by subclass of DynamicalSystem.")



    def generate_observables(self, observables, steps, record_interval=1):
        '''
        Runs a simulation and generates data for observables step by step.

        Parameters
        ----------
        observables : Observable or list of
            observables for which data will be generated step by step
        steps : int
            number of steps to move simulation forward
        record_interval : int
            interval between time steps for which observable data is generated
        '''

        if isinstance(observables, list):
            list_observables = observables
        else:
            list_observables = [observables]

        recorded_steps = steps // record_interval

        for obs in list_observables:
            obs._initialize(self, recorded_steps)
        
        for recorded_step in range(1, recorded_steps):            
            for step_in_interval in range(0, record_interval):
                self._single_step()
            for obs in list_observables:
                obs._update(self)
        for obs in list_observables:
            obs._finalize(self)
        
    
    def trajectory(self, steps, transient=0, record_interval=100):
        
        traj = self.SeriesClass()
        
        if transient > 0:
            self.discard_transient(transient)
        self.generate_observables(traj, steps=steps, record_interval=record_interval)
        
        return traj

    def description_string(self):
        return f"S={self.S},M={self.M},dt={self.dt}"
    


class Observable:
    '''Observables defined for a `DynamicalSystem`. 

    Attributes
    ----------
    lanel : str
    origin : str
        description of which data generated the observable
    '''

    def __init__(self, label=None):
        '''
        Set parameters of the observable that are independent of `DynamicalSystem` observed.
        
        Parameters
        ----------
        label : str
            label string for the observable, eg differentiate between same observable from separate simulations
        '''
        self.label = label
        self.origin = ""

    def _initialize(self, ds, N):
        '''
        Initialize variables of the observable when simulation is started.
        
        Parameters
        ----------
        ds : DynamicalSystem
            from which simulation state and parameters are accesse
        N : int
            number of simulation steps. allows appropriate size allocation of arrays
        '''
        
    def _update(self, ds):
        '''
        Update observable by one step

        Parameters
        ----------
        ds : DynamicalSystem
            from which simulation state and parameters are accessed
        '''
        raise NotImplementedError("Not implemented")

    def _finalize(self, ds):
        '''Perform final operations leading to valid data fields.
        
        Parameters
        ----------
        ds : DynamicalSystem
            from which simulation state and parameters are accessed
        '''
        raise NotImplementedError("Not implemented")
    
    def from_trajectory(self, traj):
        '''Generate observable directly from trajectory data.
        
        Parameters
        ----------
        traj : trajectory object (eg AbundanceSeries)
            generally must contain mat_x, and whatever field used from
            DynamicalSystem or subclasses for the observable. 
        '''
        try:
            origin = traj.description_string()
        except:
            pass
        
    def save(self, path):
        '''
        Save as a pickle.
        
        Parameters
        ----------
        path : path, str
            abslute save path
        '''
        with open(path,'wb') as f:
            pickle.dump(self, f)

    @classmethod
    def load(cls, path):
        '''Load an observable from a pickle.
        
        Can be invoked generically as Observable.load, or as
        ObservableSubclass.load to test explcitly that the pickle is
        of a given subclass.

        Parameters
        ----------
        path : path, str
            path from which to load pickle

        Returns
        -------
        obs : Observable
            the observable object loaded from data 
        '''
        
        with open(path,'rb') as f:
            obs = pickle.load(f)
            if not isinstance(obs, cls):
                raise RuntimeError(f"data loaded is not a {cls} instance")
            return obs


class SystemSeries(Observable):
    """
    Stores only x, not the auxiliary state
    """
    def _initialize(self, ds, N):
        self.mat_x = np.zeros(shape=(N, ds.S), dtype=ds.dtype)
        self.mat_x[0,:] = ds.vec_x
        self.S = ds.S
        self.vec_t = np.zeros(N)
        self.vec_t[0] = ds.dt * ds.current_step
        self._n = 1
        self.time_started = str(datetime.datetime.now())

    def _update(self, ds):
        self.mat_x[self._n, :] = ds.vec_x
        self.vec_t[self._n] = ds.dt * ds.current_step
        self._n += 1

    def _finalize(self, ds):
        self.time_ended = str(datetime.datetime.now())

    def colors(self, type='bright', seed=12345):
        """
        Return S random colors. Based on https://stackoverflow.com/questions/14720331/how-to-generate-random-colors-in-matplotlib

        type: {bright, soft}

        """
        
        color_rng = default_rng(seed)

        if type not in ('bright', 'soft'):
            print ('Please choose "bright" or "soft" for type')
            return

        # Generate color map for bright colors, based on hsv
        if type == 'bright':
            randHSVcolors = [(color_rng.uniform(low=0.0, high=1),
                            color_rng.uniform(low=0.2, high=1),
                            color_rng.uniform(low=0.9, high=1)) for i in range(self.S)]

            # Convert HSV list to RGB
            randRGBcolors = []
            for HSVcolor in randHSVcolors:
                randRGBcolors.append(colorsys.hsv_to_rgb(HSVcolor[0], HSVcolor[1], HSVcolor[2]))
            #random_colormap = LinearSegmentedColormap.from_list('new_map', randRGBcolors, N=self.S)
    
        # Generate soft pastel colors, by limiting the RGB spectrum
        if type == 'soft':
            low = 0.15
            high = 0.95
            randRGBcolors = [(color_rng.uniform(low=low, high=high),
                            color_rng.uniform(low=low, high=high),
                            color_rng.uniform(low=low, high=high)) for i in range(self.S)]
            #random_colormap = LinearSegmentedColormap.from_list('new_map', randRGBcolors, N=self.S)
        
        #return random_colormap
        return randRGBcolors
    

def load_observable(path):
    return Observable.load(path)

# note: may be an issue if one applies .from_trajectory several times to same observable instance!
def lazy_property(fn):
    """
    Make read-only, lazily-evaluated property
    """
    attr_name = '_lazy_' + fn.__name__
    @property
    def _lazyprop(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, fn(self))
        return getattr(self, attr_name)
    @_lazyprop.setter
    def _lazyprop(self,value):
        raise AttributeError("This property is read-only")
    return _lazyprop