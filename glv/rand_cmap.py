#https://stackoverflow.com/questions/14720331/how-to-generate-random-colors-in-matplotlib

def rand_cmap(nlabels, type='bright', first_color_black=False, last_color_black=False, verbose=False, seed=12345):
    """
    Creates a random colormap to be used together with matplotlib. Useful for segmentation tasks
    :param nlabels: Number of labels (size of colormap)
    :param type: 'bright' for strong colors, 'soft' for pastel colors
    :param first_color_black: Option to use first color as black, True or False
    :param last_color_black: Option to use last color as black, True or False
    :return: colormap for matplotlib
    """
    from matplotlib.colors import LinearSegmentedColormap
    import colorsys
    import numpy as np
    from numpy.random import default_rng

    color_rng = default_rng(seed)

    if type not in ('bright', 'soft'):
        print ('Please choose "bright" or "soft" for type')
        return

    # Generate color map for bright colors, based on hsv
    if type == 'bright':
        randHSVcolors = [(color_rng.uniform(low=0.0, high=1),
                          color_rng.uniform(low=0.2, high=1),
                          color_rng.uniform(low=0.9, high=1)) for i in range(nlabels)]

        # Convert HSV list to RGB
        randRGBcolors = []
        for HSVcolor in randHSVcolors:
            randRGBcolors.append(colorsys.hsv_to_rgb(HSVcolor[0], HSVcolor[1], HSVcolor[2]))

        if first_color_black:
            randRGBcolors[0] = [0, 0, 0]

        if last_color_black:
            randRGBcolors[-1] = [0, 0, 0]

        random_colormap = LinearSegmentedColormap.from_list('new_map', randRGBcolors, N=nlabels)
        

    # Generate soft pastel colors, by limiting the RGB spectrum
    if type == 'soft':
        low = 0.15
        high = 0.95
        randRGBcolors = [(color_rng.uniform(low=low, high=high),
                          color_rng.uniform(low=low, high=high),
                          color_rng.uniform(low=low, high=high)) for i in range(nlabels)]

        if first_color_black:
            randRGBcolors[0] = [0, 0, 0]

        if last_color_black:
            randRGBcolors[-1] = [0, 0, 0]
        random_colormap = LinearSegmentedColormap.from_list('new_map', randRGBcolors, N=nlabels)
    
    #return random_colormap
    return randRGBcolors