'''
*** NOTE: the base classes in .sim were moved to .base, and GLV classes to .model. However, 
some finished computationally expensive data sets were based on simulation code using this module
which is therefore kept for reference ***


Module to simulate generalized Lotka-Volterra systems
'''

'''Mathematics imports '''
import numpy as np
from numpy.random import default_rng
import numba # for jit compiling to enhance performance
from math import ceil

from . import obs

'''GPU imports'''
# For GPU use
from numba import cuda    
from numba import config 
#import cupy
config.CUDA_LOW_OCCUPANCY_WARNINGS = 0
config.CUDA_WARN_ON_IMPLICIT_COPY = 0

import time

import datetime

import argparse # to process command line arguments
from scipy import integrate
   
import pickle



###############################################################################
#   BASE CLASSES
###############################################################################


class DynamicalSystem():
    '''
    Base class for dynamical systems. The `DynamicalSystem` represents the evolution rule, parameters, 
    the current state of the system.
    
    `vec_x` is an S-array of the current state of the main system; `vec_aux` is an M-array
    of the current state of auxiliary variables (e.g. noise or forcing)
    
    A particular dynamics is implemented by overloading the `single_step` function, which
    should update `vec_x` and `vec_aux` by one time-step of size `dt`.
    
    Attributes
    ----------
    S : int
        dimension of main state vector
    M : int
        dimension of auxiliary state vector
    dt : float
        integration time step
    dtype
        data type (eg float) of state variables
    current_step : int
        current discrete time step of simulation
    vec_x : numpy array
        current main state vector
    vec_aux : numpy array
        current auxiliary state vector
       
    '''

    def __init__(self, S=100, M=0, dt=0.01, dtype=np.float64):
        '''
        Initializes all attributes
        '''                
        self.S = S  # dimension      
        self.M = M
        self.dt = dt 
        self.dtype = dtype #same for x and aux for now
        self.vec_x = np.ones(S, self.dtype) / S #current state
        if M>0:
            self.vec_aux = np.zeros(M, dtype)
        else:
            self.vec_aux = None
        self.current_step = 0 #
 
   
    def initialize_uniform(self, low=10**-30, high=1., seed=None):
        '''
        Initialize state vector from uniform distribution.
        
        Parameters
        ----------
        low : float
            lower bound
        high : float
            upper bound
        seed
            random seed used; if None draws from system entropy
        '''

        rng = np.random.default_rng(seed)
        self.vec_x = rng.uniform(low=low, high=high, size=self.S)
    

    def discard_transient(self, steps, check_NaN=True):
        '''
        Runs <steps> steps and checks if NaNs were generated
        
        Parameters
        ----------
        steps
            number of steps to discard
        check_NaN : bool
            whether to check each step for NaN values in vec_x

        Returns
        -------
        nondivergent : bool
            whether NaN was found in the main state vector
        '''
        
        found_NaN = False

        if check_NaN:
            for n in range(steps):
                self._single_step()
                if np.isnan(self.vec_x).any():
                    found_NaN = True
                    break
        else:
            self.step(steps=steps)
            found_NaN = np.isnan(self.vec_x).any()

        if not found_NaN:
            self.reset_time()

        return not found_NaN


    def reset_time(self):
        '''
        Resets time to zero
        '''

        self.current_step = 0


    def step(self, steps=1):
        '''
        Steps the system forward.

        Parameters
        ----------
        steps : int
            number of steps to move forward

        '''
        if steps == 1:
            self._single_step()
        else:
            for n in range(steps):
                self._single_step()


    def _single_step(self):
        '''
        Defines the dynamical rule: result of a single step forward.
        
        Should update `vec_x`, `vec_aux`, and perform `current_step += 1`.
        '''

        raise NotImplementedError("Must be implemented by subclass of DynamicalSystem.")


    def trajectory(self, steps, record_interval=1, check_NaN = False, store_aux=False):
        '''
        Generates a trajectory of the model.

        Parameters
        ----------
        steps : int
            number of discrete integration time steps of the trajectory
        record_interval : int
            separation between time points that are stored in the trajectory data
        check_NaN : bool
            whether to check for NaNs with each time step
        store_aux : bool
            whether to 

        Returns
        -------
        vec_t : numpy array
            vector of times (multiples of dt)
        mat_x : numpy NxS array
            matrix of state vectors (S-vectors) vs time
        mat_aux : numpy NxM array, None
            matrix of auxiliary states; only returned if store_aux==True        
        '''

        def traj_without_aux():
                
            recorded_steps = steps // record_interval

            vec_t = (self.current_step + np.arange(0, steps, record_interval) ) * self.dt       
            mat_x = np.zeros(shape=(recorded_steps, self.S), dtype=self.dtype)    

            vec_x0 = self.vec_x.copy()

            found_nan = False
            for recorded_step in range(1, recorded_steps):            
                for step_in_interval in range(0, record_interval):
                    self._single_step()
                mat_x[recorded_step] = self.vec_x.copy()
                
                if check_NaN and np.isnan(self.vec_x).any():
                    # If NaN is found we break, leaving vec_x with the NaN entry and setting the rest of mat_x to NaN
                    mat_x[recorded_step+1:,:] = np.NaN
                    found_nan = True
                    break
                    
            if not found_nan: 
                self._single_step() #last step is missed by loop boundary
                
            mat_x[0,:] = vec_x0
                
            return vec_t, mat_x

        def traj_with_aux():
                
            recorded_steps = steps // record_interval

            vec_t = (self.current_step + np.arange(0, steps, record_interval) ) * self.dt       
            mat_x = np.zeros(shape=(recorded_steps, self.S), dtype=self.dtype)
            mat_aux = np.zeros(shape=(recorded_steps, self.M), dtype=self.dtype)

            vec_x0 = self.vec_x.copy()
            vec_aux0 = self.vec_aux.copy()

            found_nan = False
            for recorded_step in range(1, recorded_steps):            
                for step_in_interval in range(0, record_interval):
                    self._single_step()
                mat_x[recorded_step] = self.vec_x.copy()
                mat_aux[recorded_step] = self.vec_aux.copy()
                                
                if check_NaN and np.isnan(self.vec_x).any():
                    # If NaN is found we break, leaving vec_x with the NaN entry and setting the rest of mat_x to NaN
                    mat_x[recorded_step+1:,:] = np.NaN
                    found_nan = True
                    break
                    
            if not found_nan: 
                self._single_step() #last step is missed by loop boundary
            else:
                print("Divergent!")
                
            mat_x[0,:] = vec_x0
            mat_aux[0,:] = vec_aux0
            
                
            return vec_t, mat_x, mat_aux

        if store_aux:
            return traj_with_aux()
        else:
            return traj_without_aux()


    def generate_observables(self, observables, steps, record_interval=1):
        '''
        Runs a simulation and generates data for observables step by step.

        Parameters
        ----------
        observables : Observable or list of
            observables for which data will be generated step by step
        steps : int
            number of steps to move simulation forward
        record_interval : int
            interval between time steps for which observable data is generated
        '''

        if isinstance(observables, list):
            list_observables = observables
        else:
            list_observables = [observables]

        recorded_steps = steps // record_interval

        for obs in list_observables:
            obs._initialize(self, recorded_steps)
        
        for recorded_step in range(1, recorded_steps):            
            for step_in_interval in range(0, record_interval):
                self._single_step()
            for obs in list_observables:
                obs._update(self)
        for obs in list_observables:
            obs._finalize(self)
        
    def description_string(self):
        return f"S={self.S},M={self.M},dt={self.dt}"

    def trajectory2(self, steps, transient=0, record_interval=100):
        traj = obs.AbundanceSeries()
        if transient > 0:
            self.discard_transient(transient)
        self.generate_observables(traj, steps=steps, record_interval=record_interval)
        




class UnitGLVSystem(DynamicalSystem):
    '''
    Represents a generalized Lotka-Volterra system with unit carrying capacities and growth rates.

    Attributes
    ----------
    S : int
        number of species
    dt : float
        integration time step
    mu : float
        mean of interaction parameter distribution
    sig : float
        standard deviation
    gam : float
        correlation between diagonally opposite interaction parameters
    lam : float, str
        total immigration rate. If str of format '<number>/S' will be numerically evaluated accordingly 
    alpha_seed
        random seed used to generate interaction matrix from Gaussian(mu,sig;gam)
    mat_alpha : SxS array, optional
        specific interaction matrix to use; overrides generation from Gaussian(mu,sig;gam)
    scheme : {'logistic', 'expanded_logistic', 'log', 'euler', 'default'}
        integration scheme to use; 'default' is 'logistic'
    dtype
        data type for abundances
    target : {'cpu', 'gpu'}
        wether to run on cpu or gpu

    '''

    def __init__(self, S=100, dt=0.01, mu=0.5, sig=0.3, gam=0., lam=10**-8, alpha_seed=12345, dtype=np.float64, mat_alpha=None, scheme='logistic'):
        '''
        Initializes simulation parameters, in particular the interaction matrix         
        '''
        
        super().__init__(S, 0, dt, dtype)
        
        self.mu = mu
        self.sig = sig
        self.gam = gam

        self.lam = lam
        
        self.alpha_seed = alpha_seed
        self.target = 'cpu'

        self.set_scheme(scheme)
        
        if mat_alpha is None:
            self.mat_alpha = gaussian_random_matrix(self.S, self.mu, self.sig, self.gam,
                                                       seed=self.alpha_seed, dtype=dtype)
        else:
            self.mat_alpha = mat_alpha


    #~~ Integration Schemes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def set_scheme(self, scheme):
        '''
        Sets the integration scheme.

        Parameters
        ----------
        scheme :  {'default', 'logistic', 'expanded_logistic', 'log', 'euler'}
            integration scheme to use; 'default' is 'logistic'
        '''

        if scheme == 'default':
            scheme = 'logistic'

        if scheme == 'expanded_logistic':
            self._single_step = self._single_step_expanded_logistic
        elif scheme == 'logistic':
            self._single_step = self._single_step_logistic
        elif scheme == 'log':
            self._single_step = self.single_step_log
        elif scheme == 'euler':
            raise NotImplementedError("Euler scheme has not been implemented yet")
            self._single_step = self.single_step_euler
        else:
            raise ValueError("Non-existent numerical integration scheme specified")
        self.scheme = scheme

    @staticmethod
    @numba.njit(parallel=False)
    def _single_step_expanded_logistic_compiled(vec_x, mat_alpha, lam, dt):
        vec_kappa = 1 - mat_alpha @ vec_x
        return vec_x / ( 1 + (vec_x - vec_kappa) * dt * (1 - vec_kappa * dt / 2) ) + lam * dt

    def _single_step_expanded_logistic(self):        
        self.vec_x = UnitGLVSystem._single_step_expanded_logistic_compiled(self.vec_x, self.mat_alpha, self.lam, self.dt)
        self.current_step += 1

    @staticmethod
    @numba.njit(parallel=False)
    def _single_step_logistic_compiled(vec_x, mat_alpha, lam, dt):
        vec_kappa = 1 - mat_alpha @ vec_x
        vec_exp = np.exp(- vec_kappa * dt)
        return vec_x * vec_kappa / ( vec_kappa * vec_exp + vec_x * (1 - vec_exp )) + lam * dt

    def _single_step_logistic(self):        
        self.vec_x = UnitGLVSystem._single_step_logistic_compiled(self.vec_x, self.mat_alpha, self.lam, self.dt)
        self.current_step += 1

    @staticmethod
    @numba.njit(parallel=False)
    def _single_step_log_compiled(vec_x, mat_alpha, lam, dt):
        vec_y = np.log(vec_x)
        vec_y_next = vec_y + dt * ( 1 - mat_alpha @ vec_x - vec_x + lam / vec_x)
        return np.exp(vec_y_next)

    def single_step_log(self):
        self.vec_x = UnitGLVSystem._single_step_log_compiled(self.vec_x, self.mat_alpha, self.lam, self.dt)
        self.current_step += 1
    
    def _single_step(self):        
        raise RuntimeError("Attempting to step without single_step fuction set")
        #self.vec_x = UnitGLVSystem.compiled_step(self.vec_x, self.mat_alpha, self.lam, self.dt)
        #self.current_step += 1

    def description_string(self):
        return f"S={self.S},mu={self.mu},sig={self.sig},gam={self.gam},dt={self.dt}"
    

class GLVSystem(DynamicalSystem):
    def __init__(self, S=100, dt=0.01, lam=10**-8, mat_alpha=None, vec_r = None, vec_K = None, scheme='logistic'):
        super().__init__(S, 0, dt, np.float64)

        self.lam = lam
        self.target = 'cpu'

        #self.set_scheme(scheme)
        
    
    
class UnitGLVSystemGPU(UnitGLVSystem):
    '''Like UnitGLVSystem but runs on the GPU'''

    # Haven't maintained in a while

    def __init__(self, S=100, dt=0.01, mu=0.5, sig=0.3, gam=0., alpha_seed=12345, lam=10**-8):
        super().__init__(S=S, dt=dt, mu=mu, sig=sig, gam=gam, alpha_seed=alpha_seed, lam=lam, dtype=np.float32)
        self.target = 'gpu'
    
    def run(self):

        # Arrays on host


        @cuda.jit
        def k_carry_cap(d_mat_alpha, d_vec_x, S, d_vec_kappa):
            i = cuda.threadIdx.x + cuda.blockDim.x * cuda.blockIdx.x
            kappa_i = np.float32(0)    
            if i < S:
                for j in range(0,S):
                    kappa_i += d_mat_alpha[i, j] * d_vec_x[j]
                kappa_i = 1 - kappa_i #this step last to minimize error
                d_vec_kappa[i] = kappa_i

        @cuda.jit
        def k_update_abundances(d_vec_x, d_vec_kappa, dt, lam, S, d_vec_x_next ):
            i = cuda.threadIdx.x + cuda.blockDim.x * cuda.blockIdx.x
            if i < S:
                x_i = d_vec_x[i]
                kappa_i = d_vec_kappa[i]
                x_next_i = x_i / ( 1 + (x_i - kappa_i) * dt * (1 - kappa_i * dt / 2) ) + lam * dt
                d_vec_x_next[i] = x_next_i     
       
        # Carry kap step
        threads_per_block_1 = 32*8
        blocks_1 = ceil( self._S / threads_per_block_1)
        # Abundance step
        threads_per_block_2 = 128
        blocks_2 = ceil( self._S / threads_per_block_2)
               

        # Arrays on device
        d_mat_alpha = cuda.to_device(self._mat_alpha)
        d_vec_x = cuda.to_device(self._vec_x0)
        d_vec_x_next = cuda.device_array(self._S, dtype="float32")
        d_vec_kappa = cuda.device_array(self._S, dtype="float32")

        # Could add temporary storage on GPU and make transfers in bulk to host....
        time_steps = int(self._T / self._dt)    
        recorded_time_steps = time_steps // self._record_interval

        for recorded_step in range(1, recorded_time_steps):            
            for step_in_interval in range(0, self._record_interval):
                # Calculate effective carrying capacities on gpu
                k_carry_cap[blocks_1, threads_per_block_1](d_mat_alpha, d_vec_x, self._S, d_vec_kappa)
                # Update abundances on gpu
                k_update_abundances[blocks_2, threads_per_block_2](d_vec_x, d_vec_kappa, self._dt, self._lam, self._S, d_vec_x_next )
                d_vec_x_next = d_vec_x     
            # Copy abundance to host for recorded time steps
            vec_x = d_vec_x_next.copy_to_host()
            self._mat_x[recorded_step] = vec_x   



def gaussian_random_matrix(S, mean, std, gamma=0, dtype=np.float64, seed = None):
    '''
    Generates a Gaussian random matrix.

    Parameters
    ----------
    mean : float
        mean of elements
    std : float
        std of elements
    gamma : float in interval [-1,1]
        correlation beetween diagonally opposed elements
    dtype
        data type
    seed
        seed used for random number generator; drawn from system entropy if None

    
    '''

    if abs(gamma) > 1.:
        raise ValueError("gamma>1 or gamma<1 not allowed for correlation")
    if gamma != 0.:
        factor = (1-np.sqrt(1-gamma**2))/gamma
    else:
        factor = 0.
    
    rng = default_rng(seed)
    M = rng.standard_normal((S,S), dtype=dtype)    
    Z = ( M + factor * M.T ) / np.sqrt( 1 + factor ** 2 )
    mat_alpha = mean + std * Z  
    np.fill_diagonal(mat_alpha, 0)  # null diagonal
    
    return mat_alpha




class IntermittencyModel(DynamicalSystem):
    '''
    Stochastic logistic growth with correlated noise.

    Attributes
    ----------
    k : float
        the mean of the noise is -k
    u : float
        the std of the noise
    lam : float
        immigration rate
    dt : float
        integration time step
    dtype : {float32, float64}
        data type
    seed
       seed used for the noise random number generator; drawn from system entropy if None  

    '''

    def __init__(self, k, u, tau, lam, dt=0.01, dtype=np.float64, seed=None):
        super().__init__(S=1,M=1,dt=dt,dtype=dtype)
        self.k = k
        self.u = u
        self.tau = tau
        self.lam = lam
        self.seed = seed
        self._rng = default_rng(seed)
        
        self.vec_aux[:] = -k # aux is the noise eta 
        self.vec_x[:] = lam/k
        
    def _single_step(self):
        self.vec_aux = (1 - self.dt / self.tau) * self.vec_aux + np.sqrt(2 / self.tau * self.dt ) * self._rng.standard_normal()
        kappa = - self.k + self.u * self.vec_aux
        self.vec_x = self.vec_x / ( 1 + (self.vec_x - kappa) * self.dt * (1 - kappa * self.dt / 2) ) + self.lam * self.dt
        self.current_step += 1

    def trajectory(self, steps, record_interval=1, check_NaN=False):
        vec_t, mat_x, mat_eta = super().trajectory(steps, record_interval, check_NaN, store_aux=True)
        vec_x = mat_x.flatten()
        vec_eta = mat_eta.flatten()
        return vec_t, vec_x, vec_eta

    def ucna(self):
        nu_ucna = -1 + self.k / (self.tau * self.u**2)

        def q(x,sign):
            return (x + (1/self.tau + sign * self.k))**2 / (2 * self.u**2)

        def normalization_constant():
            def integrand(y,k):
                return np.exp( k*y - q(self.lam * np.exp(-y), -1) - q(np.exp(y), +1) )

            def integrand1(y):
                return integrand(y, 1 + nu_ucna)
            def integrand2(y):
                return integrand(y, 2 + nu_ucna)
            def integrand3(y):
                return integrand(y, nu_ucna)

            y_min = - 20 * np.log(10)
            y_max = 4 * np.log(10)

            integral1 = integrate.quad(integrand1,y_min, y_max)
            integral2 = integrate.quad(integrand2,y_min, y_max)
            integral3 = integrate.quad(integrand3,y_min, y_max)
            N1 = (1 / self.tau) * integral1[0]
            N2 = integral2[0]
            N3 = self.lam * integral3[0]

            # # Check separate parts
            # print("Integrals:")
            # print(f"\t{integral1}")
            # print(f"\t{integral2}")
            # print(f"\t{integral3}")
            # print("N parts:")
            # print(f"\t{N1}")
            # print(f"\t{N2}")
            # print(f"\t{N3}")

            return N1 + N2 + N3

        N = normalization_constant()

        def P(x):
            return np.exp( -q(self.lam/x, -1) - q(x,+1)) * (x**nu_ucna) * (1/self.tau + x + self.lam/x) / N

        return P
        # vec_P = np.array([P(x) for x in bins_mid])

