'''
This module contains observables related to study of the dominant component of species
'''

import numpy as np
from . import model
from glv import base
from . import metrics


class DominantComponent(base.Observable):
    '''
    Gives an abundance threshold `thresh` for being in the top `ceil(Neff)` ranked species.
    Use `>= thres` on the abundance vector yields `ceil(Neff)` true, > yields `bot(Neff)`.

    Parameters
    ----------
    rel
        set to true if mat_x already in relative abundance

    Attributes
    ----------
    vec_pmin
        relative abundance threshold vs time for being in the dominant component
    vec_xmin
        absolute abundance threshold vs time for being in the dominant component
    vec_domfrac
        fraction of time spent as part of the dominant community
    '''
    
    def from_trajectory(self, traj):
        # number of dominant species
        self.vec_ndom = np.ceil(metrics.eff_size(traj.mat_x)) 
        
        # prepare vector of relative abundance thresholds
        (T, S) = traj.mat_x.shape
        self.vec_xmin = np.zeros(T)
                
        # populate
        for t in range(T):
            k = self.vec_ndom[t]

            # absolute abundance of kth ranked species 
            self.vec_xmin[t] = np.partition(traj.mat_x[t,:], int(S - k) ) [int(S - k)]

        self.vec_pmin = self.vec_xmin / np.sum(traj.mat_x, axis=1)

        self.vec_domfrac = np.mean( traj.mat_x > self.vec_xmin[:,None], axis = 0)

        self.vec_z = np.sum(traj.mat_z, axis=1) / np.sqrt(traj.S)

class QuasiEquilibria(base.Observable):
    '''
    Measures the BC similarity of an abundance vector x(t) and the stationary abundance (assuming
    it is reached) of a simulation starting from x0 = x(t), versus time t.

    Parameters
    ----------
    sys
        DynamicalSystem used to run the simulations

    Attributes
    ----------
    vec_bc
        closeness time series
    vec_fix
        1/0 vector of if converged to fixed point
    mat_xfp
        matrix of the successive equilibria
    '''

    def __init__(self, sys, sep, steps=100*200, steps_transient=100*500, record_interval=100):
        self.s = sys # should have lam set to 0
        self.sep = sep
        self.steps = steps
        self.steps_transient = steps_transient
        self.record_interval = record_interval

    def from_trajectory(self, traj):
        
        N = traj.vec_t.size // self.sep
        
        self.vec_bc = np.zeros(N)
        self.vec_fix = np.zeros(N)
        self.mat_xfp = np.zeros((N,self.s.S))

        dom = DominantComponent()
        dom.from_trajectory(traj)

        for n in range(N):
            print(f"Progress {int(100*n/N)}%", end='\r')

            # set initial point
            t = n * self.sep
           
            vec_x0 = ( traj.mat_x[t,:] >= dom.vec_xmin[t] ) * traj.mat_x[t,:]
            self.s.vec_x = vec_x0
            # run simulation
            tr = model.AbundanceSeries()
            self.s.discard_transient(self.steps_transient)
            self.s.generate_observables(tr, steps=self.steps, record_interval=self.record_interval)
           
            self.vec_bc[n] = metrics.bray_curtis(self.s.vec_x, vec_x0)
            self.vec_fix[n] = 1 if tr.lim_idx == 0 else 0
            self.mat_xfp[n,:] = self.s.vec_x
        print()

