"""
Definition of generalized and disordered Lotka Volterra models.
"""

import numpy as np
from numpy.random import default_rng
import numba # for jit compiling to enhance performance
import warnings

from . import base
from . import corr
from . import fit
from . import metrics


class GLVSystem(base.DynamicalSystem):
    '''
    Represents a generalized Lotka-Volterra system with species-specific nominal carrying capacities
    and growth rates, arbitrary pairwise interaction matrix and constant
    immigration rate.

    ``
    d x_i / dt = r_i * x_i * ( 1 - x_i - sum_{j(!=i)} alpha_{ij} K_j / K_i * x_j ) + lambda_i
    ``

    Two integrations schemes are implemented. 

    Some code implementation has been guided by wanting to avoid some redunant storage/artithmetic 
    in the standard case K=1, r=1

    Attributes
    ----------
    S : int
        number of species
    dt : float
        integration time step
    mu : float
        mean of interaction parameter distribution
    sig : float
        standard deviation
    gam : float
        correlation between diagonally opposite interaction parameters
    lam : float, str
        total immigration rate. If str of format '<number>/S' will be numerically evaluated accordingly 
    alpha_seed
        random seed used to generate interaction matrix from Gaussian(mu,sig;gam)
    mat_alpha : SxS array, optional
        specific interaction matrix to use; overrides generation from Gaussian(mu,sig;gam)
    vec_K : S-array, optional
        nominal carrynig capacities; default to unity
    vec_r : S-array, optional
        nominal growth rates; default to unity
    scheme : {'logistic', 'log', 'euler', 'default'}
        integration scheme to use; 'default' is 'log'
    dtype
        data type for abundances
    target : {'cpu', 'gpu'}
        wether to run on cpu or gpu

    '''

    def __init__(self, S=100, dt=0.01, lam=0, dtype=np.float64, mat_alpha=None, vec_K = 1, vec_r = 1, scheme='default'):
 
        super().__init__(S, 0, dt, dtype)
        
        self.lam = lam
        self._r = vec_r
        self._K = vec_K
        if mat_alpha is None:
            self._mat_alpha = np.zeros((S,S))
        else:
            self._mat_alpha = mat_alpha
        self.scheme = scheme # will set integrator
        
        self.SeriesClass = AbundanceSeries

    # Note: we update the integrator every time parameters are modified because that affects scaled versions used in integration

    @property
    def vec_r(self):
        # if r is a constant array, we store `_vec_r` as a `float` because this will be better interation schemes
        if type(self._r) == np.ndarray:
            return self._r
        else:
            return np.full(self.S, self._r)
    
    @vec_r.setter
    def vec_r(self, val):
        if np.all(val == 1):
            self._r = 1.
        else:
            self._r = val
        self._set_integrator()

    @property
    def vec_K(self):        
        if type(self._K) == np.ndarray:
            return self._K
        else:
            return np.full(self.S, self._K)
    
    @vec_K.setter
    def vec_K(self, val):
        self._K = val
        self._set_integrator()

    @property
    def mat_alpha(self):
        return self._mat_alpha
    
    @mat_alpha.setter
    def mat_alpha(self, mat):
        self._mat_alpha = mat
        self._set_integrator()

    @property
    def scheme(self):
        return self._scheme
    
    @scheme.setter
    def scheme(self, val):
        if val not in ['default', 'logistic', 'log', 'euler']:
            raise ValueError("Not a valid scheme")
        elif val == 'default':
            self._scheme = 'log'
        else:
            self._scheme = val
        self._set_integrator()


    def _set_integrator(self):
        '''
        Sets the integration step function and initializes and rescaled parameters used by the scheme        
        '''
        if self.scheme == 'log':
            # note: if K=1, r=1, essentially a redundant interaction matrix with diagonal 1 is stored 

            if not np.all(self.vec_K == 1):
                self._mat_alpha_scaled = self.mat_alpha * self.vec_K / self.vec_K[:,None]
            else:
                self._mat_alpha_scaled = self._mat_alpha.copy()
            np.fill_diagonal(self._mat_alpha_scaled, 1)
            self._mat_alpha_scaled *= self.vec_r[:,None] 

            self._single_step = self._single_step_log

        elif self.scheme == 'logistic':
            if not np.all(self.vec_K == 1):                    
                self._mat_alpha_scaled = self.mat_alpha * self.vec_K / self.vec_K[:,None]
            else:
                #does not make a copy!
                self._mat_alpha_scaled = self._mat_alpha
            self._single_step = self._single_step_logistic
        
        elif  self.scheme == 'euler':
            if not np.all(self.vec_K == 1):
                self._mat_alpha_scaled = self.mat_alpha * self.vec_K / self.vec_K[:,None]
            else:
                self._mat_alpha_scaled = self._mat_alpha.copy()
            np.fill_diagonal(self._mat_alpha_scaled, 1)
            self._mat_alpha_scaled *= self.vec_r[:,None]

            self._single_step = self._single_step_euler
            
    def _single_step(self):        
        raise RuntimeError("Attempting to step without single_step fuction set")

    @staticmethod
    @numba.njit(parallel=False)
    def _SINGLE_STEP_LOG_COMPILED(vec_x, mat_alpha_scaled, vec_r, lam, dt):
        return vec_x * np.exp( (vec_r - mat_alpha_scaled @ vec_x) * dt) + lam * dt
    
    def _single_step_log(self):
        self.vec_x = GLVSystem._SINGLE_STEP_LOG_COMPILED(self.vec_x, self._mat_alpha_scaled, self._r, self.lam, self.dt)
        self.current_step += 1

    @staticmethod
    @numba.njit(parallel=False)
    def _SINGLE_STEP_LOGISTIC_COMPILED(vec_x, mat_alpha_scaled, vec_r, lam, dt):
        vec_g = 1 - mat_alpha_scaled @ vec_x
        vec_exp =  np.exp(- (vec_r*dt) * vec_g) 
        return vec_g * ( vec_x / (vec_x + (vec_g - vec_x)*vec_exp ) ) + lam*dt

    def _single_step_logistic(self):        
        self.vec_x = GLVSystem._SINGLE_STEP_LOGISTIC_COMPILED(self.vec_x, self._mat_alpha_scaled, self._r, self.lam, self.dt)
        self.current_step += 1

    @staticmethod
    @numba.njit(parallel=False)
    def _SINGLE_STEP_EULER_COMPILED(vec_x, mat_alpha_scaled, vec_r, lam, dt):
        return (vec_r * vec_x * (1 - mat_alpha_scaled @ vec_x) + lam) * dt

    def _single_step_euler(self):        
        self.vec_x = GLVSystem._SINGLE_STEP_EULER_COMPILED(self.vec_x, self._mat_alpha_scaled, self._r, self.lam, self.dt)
        self.current_step += 1
    
    def is_unitary(self):
        '''Yes if r=1, K=1 '''
        return np.all(self._K == 1) and np.all(self._r == 1)

    def description_string(self):
        return f"S={self.S},mu={self.mu},sig={self.sig},gam={self.gam},dt={self.dt}"
    


class DisorderedLVSystem(GLVSystem):
    def __init__(self, S=100, mu=None, sig=None, gam=0, lam=0, dt=0.01, dtype=np.float64, alpha_seed=None, mat_alpha=None, vec_K=1, vec_r=1, scheme='default'):
        super().__init__(S, dt, lam, dtype, mat_alpha, vec_K, vec_r, scheme)
        self.mu = mu
        self.sig = sig
        self.gam = gam

        if (mu is not None) and (sig is not None) and (mat_alpha is None):
            self.set_gaussian_interactions(mu,sig,gam, seed=alpha_seed)

        self.SeriesClass = DisorderedLVSeries

    
    def set_gaussian_interactions(self, mu=0.5, sig=0.3, gam=0, seed=None):
        self.mu = mu
        self.sig = sig
        self.gam = gam
        self.alpha_seed = seed
        self.mat_alpha = gaussian_random_matrix(self.S, mu,sig,gam,seed=seed)
    


def gaussian_random_matrix(S, mean, std, gamma=0., dtype=np.float64, seed = None):
    '''
    Generates a Gaussian random matrix.

    Parameters
    ----------
    mean : float
        mean of elements
    std : float
        std of elements
    gamma : float in interval [-1,1]
        correlation beetween diagonally opposed elements
    dtype
        data type
    seed
        seed used for random number generator; drawn from system entropy if None

    
    '''

    if abs(gamma) > 1.:
        raise ValueError("gamma>1 or gamma<1 not allowed for correlation")
    if gamma != 0.:
        factor = (1-np.sqrt(1-gamma**2))/gamma
    else:
        factor = 0.
    
    rng = default_rng(seed)
    M = rng.standard_normal((S,S), dtype=dtype)    
    Z = ( M + factor * M.T ) / np.sqrt( 1 + factor ** 2 )
    mat_alpha = mean + std * Z  
    np.fill_diagonal(mat_alpha, 0)  # null diagonal
    
    return mat_alpha




class AbundanceSeries(base.SystemSeries):
    '''
    A time series of abundances for any GLVSystem.

    Several useful metrics of abundance time series are implemented as properties
    of this class (lazily evaluated and stored if costly)


    Attributes
    ----------
    S : int
        species count
    T : int
        time point count
    mat_x : TxS array
        matrix of abundancs
    vec_t : T array
        vector of times
    mat_p:
        vector of relative abundances
    lim_idx:
        and index characterizing the dynamical beahviour of the trajectory (fixed point, cycle, etc)
    vec_X:
        total abundance series
    vec_Nedd:
        effective community size series
    time_started : str
        time data generation started
    time_ended : str
        time data generation ended
    '''
   
    def _initialize(self, ds, N):
        super()._initialize(ds,N)

        # Note: below vars will not be included in old saved traj data...
        self._tau = None # autocorr decay rate; somewhat expensive to compute
        self._r2 = None # goodness of fit


    @property # don't want to store!
    def mat_p(self):
        return metrics.relative_abundance(self.mat_x)

    @base.lazy_property
    def lim_idx(self):
        '''
        Counts the number of times the past abundance vector comes 'close' to the final one.
        Interpreted as: 
            -1 : NaN deteceted
            0 : fixed point
            1 : persistent fluctuations (chaos)
            2,3,4,... : limit cycle, heterclinic cycle, or a switch to/from fp or cycles within the time window
        '''
        return self.limit_index()


    @property
    def vec_X(self):
        return np.sum(self.mat_x, axis=1)

    @property
    def vec_Neff(self):
        """
        Time series of effective community size
        """
        mat_p = self.mat_p
        return 1 / np.sum( mat_p ** 2, axis=1)

    @base.lazy_property
    def tau(self):
        '''
        The decay rate of the autocorrelation of the abundance vector
        '''
       
        lags=500
        mat_acf = corr.autocorr_fft(self.mat_x, lags=lags, target='cpu')
        vec_var = np.var(self.mat_x,axis=0)
        vec_w = vec_var / np.mean(vec_var) 
        vec_acf = np.mean(mat_acf * vec_w, axis=1)
        edf = fit.exponential_decay_fit(vec_acf, self.vec_t[:lags])
        self._tau = edf['tau']
        self._r2 = edf['r2']

        setattr(self, "_lazy_r2", edf.r2)

        return edf.tau

    @base.lazy_property
    def r2(self):
        self.tau
        return getattr(self, "_lazy_r2")

    def limit_index(self, T=0, tolerance=10**-4):
        '''        
        Counts the number of times the past abundance vector comes 'close' to the final one.
        Interpreted as: 
            -1 : NaN deteceted
            0 : fixed point
            1 : persistent fluctuations (chaos)
            2,3,4,... : limit cycle, heterclinic cycle, or a switch to/from fp or cycles within the time window

        Parameters
        ----------
        T
            Final T data-points checked. 0 means all
        tolerance
            sets a similarity threshold ``1-tolerance`` above which two compositions are treated as identical

        Returns
        -------
        index
            the limit index
        '''
        if np.isnan(self.mat_x[-T:,:]).any():
            return -1
        
        vec_L = metrics.similarity_to_final(self.mat_x[-T:,:])
        vec_L_sign = np.sign(vec_L - (1 - tolerance))
        index = np.sum( vec_L_sign[:-1] * vec_L_sign[1:] < 0)        
        
        return index
   
    # Plotting


class DisorderedLVSeries(AbundanceSeries):
    def _initialize(self, glvs, N):
        super()._initialize(glvs, N)
        self.vec_K = glvs.vec_K.copy()
        self.vec_r = glvs.vec_r.copy()
        self.mat_alpha = glvs.mat_alpha.copy()
        self.mu = glvs.mu
        self.sig = glvs.sig
        self.gam = glvs.gam
        self.lam = glvs.lam
        self.dt = glvs.dt
        self.scheme = glvs.scheme

    @property
    def mat_z(self):
        if self.sig == 0:
            warnings.warn("mat_z is ill-defined because sig=0")
            _mat_z = np.empty(self.mat_alpha.shape) 
            _mat_z[:] = np.NaN  
        else:
            _mat_z = (self.mat_alpha - self.mu)/self.sig
            np.fill_diagonal(_mat_z, 0)
        return _mat_z

    @property
    def vec_rho(self):
        if self.sig == 0:
            warnings.warn("vec_rho is ill-defined because sig=0")
            _vec_rho = np.empty(self.mat_x.shape[0])
            _vec_rho[:] = np.NaN
        else:
            mat_p = self.mat_p
            _vec_rho = - np.sum(mat_p * (mat_p @ self.mat_z.T), axis=1)       
        return _vec_rho

    @property
    def rho(self):
        if self.sig == 0:
            warnings.warn("rho is ill-defined because sig=0")
            return np.NaN
        else:                
            mat_p = self.mat_p
            return -np.sum(self.mat_z * (mat_p.T @ mat_p)) / mat_p.shape[0]
        
    @property
    def vec_a(self):
        return 1 - self.mu * self.vec_X
    
    @property
    def vec_b(self):
        return self.sig * np.sqrt ( np.sum(self.mat_x ** 2, axis=1) )

