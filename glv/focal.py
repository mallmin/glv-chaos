"""
Defintion of the focal species model derived for chaotic UnitGLVSystem
"""

import numpy as np
from numpy.random import default_rng
from .base import DynamicalSystem, Observable
from .model import AbundanceSeries
from scipy import integrate
from . import fit
from . import corr

class StochasticFocal(DynamicalSystem):
    '''
    Stochastic logistic growth with correlated noise.

    Attributes
    ----------
    k : float
        the mean of the noise is -k
    u : float
        the std of the noise
    lam : float
        immigration rate
    dt : float
        integration time step
    dtype : {float32, float64}
        data type
    seed
       seed used for the noise random number generator; drawn from system entropy if None  

    '''

    def __init__(self, k, u, tau, lam, dt=0.01, dtype=np.float64, seed=None):
        super().__init__(S=1,M=1,dt=dt,dtype=dtype)
        self.k = k
        self.u = u
        self.tau = tau
        self.lam = lam
        self.seed = seed
        self._rng = default_rng(seed)
        
        self.vec_aux[:] = -k # aux is the noise eta 
        self.vec_x[:] = lam/k

        self.SeriesClass = FocalSeries
        
    def _single_step(self):
        self.vec_aux = (1 - self.dt / self.tau) * self.vec_aux + np.sqrt(2 / self.tau * self.dt ) * self._rng.standard_normal()
        kappa = - self.k + self.u * self.vec_aux
        self.vec_x = self.vec_x / ( 1 + (self.vec_x - kappa) * self.dt * (1 - kappa * self.dt / 2) ) + self.lam * self.dt
        self.current_step += 1

    def ucna(self):
        nu_ucna = -1 + self.k / (self.tau * self.u**2)

        def q(x,sign):
            return (x + (1/self.tau + sign * self.k))**2 / (2 * self.u**2)

        def normalization_constant():
            def integrand(y,k):
                return np.exp( k*y - q(self.lam * np.exp(-y), -1) - q(np.exp(y), +1) )

            def integrand1(y):
                return integrand(y, 1 + nu_ucna)
            def integrand2(y):
                return integrand(y, 2 + nu_ucna)
            def integrand3(y):
                return integrand(y, nu_ucna)

            y_min = - 20 * np.log(10)
            y_max = 4 * np.log(10)

            integral1 = integrate.quad(integrand1,y_min, y_max)
            integral2 = integrate.quad(integrand2,y_min, y_max)
            integral3 = integrate.quad(integrand3,y_min, y_max)
            N1 = (1 / self.tau) * integral1[0]
            N2 = integral2[0]
            N3 = self.lam * integral3[0]

            # # Check separate parts
            # print("Integrals:")
            # print(f"\t{integral1}")
            # print(f"\t{integral2}")
            # print(f"\t{integral3}")
            # print("N parts:")
            # print(f"\t{N1}")
            # print(f"\t{N2}")
            # print(f"\t{N3}")

            return N1 + N2 + N3

        N = normalization_constant()

        def P(x):
            return np.exp( -q(self.lam/x, -1) - q(x,+1)) * (x**nu_ucna) * (1/self.tau + x + self.lam/x) / N

        return P
    

class FocalSeries(AbundanceSeries):
    '''Time series data and parameters from a StochasticFocal simulation'''
    def _initialize(self, sf, N):
        super()._initialize(sf, N)
        self.k = sf.k
        self.u = sf.u
        self._tau = sf.tau
        self.lam = sf.lam
        self.dt = sf.dt
    
    @property
    def tau(self):
        # Note: overrides tau, which is the empirical autocorrelation time scale of the series
        return self._tau
    
    @property
    def vec_x(self):
        return self.mat_x[:,0]



class EffectiveNoise(Observable):
    '''
    Extracts the effective noise (definition in paper) from a `UnitGLVSeries`.

    Attributes
    ----------
    mat_eta 
        time series of each species normalized effective noise
    vec_a
    vec_b
    k
    u
    vec_t
    vec_acf
    mat_acf
    tau
    r2
    
    '''
    
    def from_trajectory(self, traj, lags=500, target='cpu'):
        '''
        Parameters
        ----------
        traj : UnitGLVSeries
        '''

        self.vec_a = traj.vec_a
        self.vec_b = traj.vec_b
        self.k = - np.mean(self.vec_a)
        self.u = np.mean(self.vec_b)

        self.mat_eta = (1 - traj.mat_x @ traj.mat_alpha.T - self.vec_a[:,None]) / self.vec_b[:,None]
  
        # Correlation time of noise
        self.vec_t = traj.vec_t[:lags]
        self.mat_acf = corr.autocorr_fft(self.mat_eta, lags=lags, target=target)
        
        self.vec_acf = np.mean(self.mat_acf, axis=1)
        edf = fit.exponential_decay_fit(self.vec_acf, self.vec_t)
        self.tau = edf['tau']
        self.r2 = edf['r2']


        # #do weighted version too
        # vec_w = np.var(self.mat_eta, axis=0)
        # vec_w = vec_w / np.mean(vec_w)
        # self.vec_acfw = np.mean(self.mat_acf * vec_w[:,None], axis=1)
        # edfw = fit.exponential_decay_fit(self.vec_acf, self.vec_t)
        # self.tau_w = edf.tau
        # self.r2_w = edf.r2


    def all_tau(self, lags=500):
        '''Calculate all tau fits'''
        S,T = self.mat_acf.shape
        if lags > T:
            raise RuntimeError("Too many lags")      
        vec_tau = np.zeros(S)
        for i in range(S):
            edf = fit.exponential_decay_fit(self.mat_acf[:lags,i], self.vec_t[:lags])
            vec_tau[i] = edf['tau']
        
        return vec_tau
