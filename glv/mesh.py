'''Functionality for analyzing and plotting glv data across the parameter (mu, sigma, ...) phase space '''

import numpy as np
import warnings
import numpy.ma as ma
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import pandas as pd

### MESHGRID PLOTTING #############################


def meshgridXY(dat, colx, coly, check_regular=False):
    '''Takes a dataframe and returns an X,Y meshgrid based on colx and coly '''

    vec_mu_ordered = np.sort(dat[colx].drop_duplicates().to_numpy(np.float64))
    vec_sig_ordered = np.sort(dat[coly].drop_duplicates().to_numpy(np.float64))

    X, Y = np.meshgrid(vec_mu_ordered, vec_sig_ordered)

    if check_regular:
        mat_mu_sig_unique = dat.loc[:,["mu","sig"]].drop_duplicates().to_numpy(np.float64)
        if mat_mu_sig_unique.shape[0] != X.size:
            warnings.warn("mu-sig data not in regular grid form")

    return X, Y

def meshgridXYZ(dat, colx, coly, colz, reduction=np.mean, mask_nan=True):
    '''Takes a dataframe and returns X,Y,Z meshgrid based on colx, coly, colz, with
    z-values reduced to a scalar using reduction if (x,y) is associated with multple z'''

    X, Y = meshgridXY(dat, colx, coly)
   
    Z = np.empty(shape=X.shape, dtype=np.float64)
    Z[:,:] = np.NaN

    for i in range(Z.shape[1]):
        for j in range(Z.shape[0]):
            x = X[0,i]
            y = Y[j,0]
            selection = dat[ (dat[colx] == x) & (dat[coly] == y)]  
            vec_z = selection[colz].to_numpy(np.float64)
            if vec_z.size == 0:
                Z[j,i] = np.NaN
            else:
                Z[j,i] = reduction(vec_z)
    
    if mask_nan:
        Z = ma.masked_where(np.isnan(Z), Z)

    return X, Y, Z



def colorbar(mappable):
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    return fig.colorbar(mappable, cax=cax)

def capped_colormesh(dat, colx, coly, colz, reduction=np.mean, vmin=None, vmax=None,
 cbar=True, equal_aspect=True, **kwargs):
    '''Returns plot handles for a pcolormesh where values outside [vmin, vmax] are greyed out'''

    X, Y, Z = meshgridXYZ(dat, colx, coly, colz, mask_nan = True)  
    
    Z_light = None
    if vmax is not None:
        Z_light = np.ma.masked_where( (Z < vmax), 0.2 * np.ones(Z.shape) )
    Z_dark = None
    if vmin is not None:
        Z_dark = np.ma.masked_where( Z > vmin, 0.35 * np.ones(Z.shape) )

    fig, ax = plt.subplots()
    fig.set_size_inches(8, 8)
    pc = ax.pcolormesh(X,Y,Z, cmap='spring', vmin=vmin, vmax=vmax, **kwargs)

    if equal_aspect:
        ax.set_aspect('equal', adjustable='box')
    if cbar:
        colorbar(pc)

    if Z_dark is not None:
        ax.pcolormesh(X,Y,Z_dark, cmap="Greys", vmin=0, vmax=1)
    if Z_light is not None:
        ax.pcolormesh(X,Y,Z_light, cmap="Greys", vmin=0, vmax=1)

    return fig, ax



