import matplotlib.animation as animation
import numpy as np
from . import rand_cmap as rcm
import matplotlib.pyplot as plt


class Animator():
    pass


def phase_xk(sim, start=20, stop=-1-20, trail=20, interval=100, tag=None): #should add optional start, stop args
    ''' Animates kappa_i vs x_i for all i '''

    #trail: umber of time steps for the trailing trajectory line 
    S = sim._S
    lam = sim._lam

    # abundance data
    mat_x = sim._mat_x[start:stop, 0:S]
    # eff carry cap data
    mat_y = 1 - mat_x @ (sim._mat_alpha[0:S,:].T)
    
    # MAIN AXES (phase portrait)
    
    fig = plt.figure()
    ax = plt.axes(xlim=(lam/10., 1.), ylim=(-1.5, 1.5))
    ax.set_xscale("log")
    #ax.set_title("ECC vs abundance phase portrait")
    ax.set_xlabel("$x_i(t)$ (abundance)")
    ax.set_ylabel("$k_i(t)$ (eff. carrying capacity)")
    colors = rcm.rand_cmap(S, type='soft')

    # Text box
    ax.text(0.45,0.05, fr"$S={sim._S}$, $\mu={sim._mu}$, $\sigma={sim._sig}$, $\lambda=10^{{{np.log10(sim._lam)}}}$", transform=ax.transAxes)

    # Get phase trajectories and current points
    dots = ax.scatter(mat_x[0,:], mat_y[0,:], s = 2, color='red', alpha=1)
    trajs = []
    for j in range(S):
        traj, = ax.plot([],[], alpha=0.1, color="lightgrey")
        trajs.append(traj)
    ax.hlines(0,np.log10(lam)-1, 100, linestyles='dashed', color='gray')

    # plot reference curve
    vec_x_ref = np.logspace(np.log10(lam)-1, 0, 100) 
    vec_y_ref = vec_x_ref - lam / vec_x_ref
    ax.plot(vec_x_ref, vec_y_ref, color='black')
 
    # INSET AXES

    vec_t = sim._vec_t[start-trail:stop+trail]
    vec_X = np.sum(sim._mat_x[start-trail:stop+trail, :], axis=1)

    left, bottom, width, height = [0.2, 0.65, 0.3, 0.15]
    ax_inset = fig.add_axes([left, bottom, width, height])
    line_X, = ax_inset.plot(vec_t[0:2*trail], vec_X[0:2*trail])
    ax_inset.set_ylim(np.amin(vec_X), np.amax(vec_X))
    ax_inset.vlines(vec_t[trail], 0, 5, linestyles='dashed')
    ax_inset.set_title("Total abundance")
    ax_inset.xaxis.set_visible(False)
   
    frames = mat_y.shape[0] - 1

    def next_frame(i):
        for j, traj in enumerate(trajs):
            i0 = max(0, i - trail)
            traj.set_data(mat_x[i0:i, j], mat_y[i0:i, j])
        dots.set_offsets( np.vstack((mat_x[i, :], mat_y[i, :])).T )
        line_X.set_data(vec_t[0:2*trail], vec_X[i:i+2*trail])
        #ax_inset.set_xlim(vec_t[i],vec_t[i+2*trail])
              
        return  trajs + [line_X, dots]
    
    anim = animation.FuncAnimation(fig, next_frame, frames=frames, interval=interval, blit=True)
    anim.save(anim_name("./glv-general/", "phase_xk", tag), writer="Pillow")

#Flagged for removal
def phase_nolog(sim, start=20, stop=-1-20, trail=20, interval=100, tag=None): #should add optional start, stop args
    ''' Animates kappa_i vs x_i for all i '''

    #trail: umber of time steps for the trailing trajectory line 
    S = sim._S
    lam = sim._lam

    # abundance data
    mat_x = sim._mat_x[start:stop, 0:S]
    # eff carry cap data
    mat_y = 1 - mat_x @ (sim._mat_alpha[0:S,:].T)
    
    # MAIN AXES (phase portrait)
    
    fig = plt.figure()
    ax = plt.axes(xlim=(-0.1, 1.), ylim=(-1.5, 1.5))
    ax.set_xlabel("$x_i(t)$ (abundance)")
    ax.set_ylabel("$k_i(t)$ (eff. carrying capacity)")
    colors = rcm.rand_cmap(S, type='soft')

    # Text box
    ax.text(0.45,0.05, fr"$S={sim._S}$, $\mu={sim._mu}$, $\sigma={sim._sig}$, $\lambda=10^{{{np.log10(sim._lam)}}}$", transform=ax.transAxes)

    # Get phase trajectories and current points
    dots = ax.scatter(mat_x[0,:], mat_y[0,:], s = 2, color='red', alpha=1)
    trajs = []
    for j in range(S):
        traj, = ax.plot([],[], alpha=0.1, color="lightgrey")
        trajs.append(traj)
    ax.hlines(0,np.log10(lam)-1, 100, linestyles='dashed', color='gray')

    frames = mat_y.shape[0] - 1

    def next_frame(i):
        for j, traj in enumerate(trajs):
            i0 = max(0, i - trail)
            traj.set_data(mat_x[i0:i, j], mat_y[i0:i, j])
        dots.set_offsets( np.vstack((mat_x[i, :], mat_y[i, :])).T )
              
        return  trajs + [dots]
    
    anim = animation.FuncAnimation(fig, next_frame, frames=frames, interval=interval, blit=True)
    anim.save(anim_name("./glv-general/", "phase_nolog", tag), writer="Pillow")



def anim_name(path, name, tag = None):
    return f"{path}anim_{name}{('_' + tag) if tag else ''}.gif"