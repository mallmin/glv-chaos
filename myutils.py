import time
import datetime
import pickle
import warnings

class Timer:
    def __init__(self, msg=None, format="m:s", absolute=False):
        self.msg = msg
        self.format = format
        self.absolute = absolute
        self.t0 = None
    
    def start(self):
        self.t0 = time.time()
        str_absolute = f" at {datetime.datetime.now()}" if self.absolute else ""
        print(f"{self.msg}:\t started{str_absolute}")
        return self

    def stop(self, result_str=""):
        self.t1 = time.time()
        if self.t0 is None:
            raise RuntimeError("Cannot stop Timer that has not started.")

        dt = self.t1 - self.t0 
        str_absolute_time = f" at {datetime.datetime.now()}" if self.absolute else ""
        

        str_tot_time = None
        if self.format == "m:s":
            minutes = int(dt / 60)
            seconds = int(dt- minutes*60)
            str_tot_time = f" ({minutes}m{seconds}s)" if minutes > 0 else f" ({dt:.2f}s)"

            #if minutes > 0:
            #    print(f" {self.msg}:\t completed ({minutes}m{seconds}s)")
            #else:
            #    print(f"{self.msg}:\t completed ({dt:.2f}s)")
        elif self.format == "s":
            str_tot_time = f" ({dt}s)"
        
        print(f"{self.msg}:\t completed{str_tot_time}{str_absolute_time}")
        if result_str != "":
            print(f"{self.msg}:\t{result_str}")

        return dt


class Tracker():
    '''Class to time tasks during code execution'''

    def __init__(self, depth=0, file=None, verbose=False):
        self.t0 = None
        self.t1 = None
        self.label = None
        self.verbose = verbose
        self.file = file
        
        self.depth = depth
        self.trackers = []
        
    def start(self, label):
        if self.t0 is not None:
            warnings.warn(f"Timer '{self.label}' already started")
        
        self.label = label
        self.t0 = time.time()
        start_str = self.depth*"  " + f"START: {self.label}" 
        if self.verbose:
            start_str += f"\t(at {datetime.datetime.now()})"        
        print(start_str, file=self.file)
        return self
        
    def start_sub(self, label):
        t = Tracker(verbose=self.verbose, depth=self.depth + 1)
        self.trackers.append(t)
        return t.start(label)
        
    def end(self, result=None):
        if self.t1 is not None:
            warnings.warn(f"Timer '{self.label}' already stopped")

        self.t1 = time.time()
        for t in self.trackers:
            if t.t1 is None:
                t.end()
        dt = self.dt        
        mins = int(dt / 60)
        secs = int(dt - mins*60)
        
        end_str = "  "*self.depth + "END:   " + (f"{mins}m{secs}s" if mins > 0 else f"{dt:.2f}s")
        if result is not None:
            end_str += f"{result}"
        if self.verbose:
            end_str += f"\t(at {datetime.datetime.now()})"
        
        print(end_str, file=self.file)

    @property
    def dt(self):
        if self.t1 is not None:
            return self.t1 - self.t0
        else:
            return None




def printif(cond, str, *args, **kwargs):
    if cond: print(str, *args, **kwargs)

# move to figformat
def adjust_fig_size(fig, scale_of_a4_width, aspect_ratio):
        width_inches = scale_of_a4_width * 21 / 2.54
        height_inches = width_inches / aspect_ratio
        fig.set_size_inches(width_inches, height_inches)

class Container():
    '''
    Simple class for saving and loading via pickles.
    '''
    def save(self, path):
        '''Save as a pickle.'''
        with open(path,'wb') as f:
            pickle.dump(self, f)
    
    @classmethod
    def load(cls, path):
        '''Load from pickle.'''        
        with open(path,'rb') as f:
            obs = pickle.load(f)
        return obs


    