
import os, sys
sys.path.append('./')
sys.path.append('./figurecode/')

from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import matplotlib.pyplot as plt
from timer import Timer
import argparse
import glv
import myutils as utl


traj = glv.obs.Observable.load("./data/traj_strong_ref.pkl")


ten = glv.corr.crosscorr_fft(traj.mat_x, target='gpu')

vec_avg = np.mean(ten,axis=(1,2))

fig, ax = plt.subplots()

for i in range(500):
    for j in range(i+1,500):
        ax.plot(vec_avg)

fig.savefig(subpath("ccf_avg"),dpi=600)




