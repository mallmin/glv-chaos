#!/bin/bash

#SBATCH -p gpu  
#SBATCH -J dmft 
#SBATCH -N 1 
#SBATCH -n 1
#SBATCH --mem-per-cpu 5000 
#SBATCH -t 10:00:00
#SBATCH -o ./glv-chaos/dmft/log/out.out
#SBATCH -e ./glv-chaos/dmft/log/err.err
 
python3 ./glv-chaos/dmft/dmft.py -T 1000000 -L 5 -r 1.0