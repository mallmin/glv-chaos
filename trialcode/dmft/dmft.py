'''
Script to produce the data and graphs demonstrating how the behaviour of the chaotic phase scales with S
'''

import os, sys
sys.path.append('./glv-chaos/')
import glv
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer
import argparse

gt = Timer("Script").start()

parser = argparse.ArgumentParser()
parser.add_argument("--load", action = "store_true")
parser.add_argument("--sim", action = "store_true")
parser.add_argument("--plot", action = "store_true")
parser.add_argument("-T", type=int, default=2000)
parser.add_argument("-L", type=int, default=10)
parser.add_argument("-r", type=float, default=0.2)
args = parser.parse_args()

mu = 0.5
sig = 0.3
S = 500
lam = 10**-8

a = -0.254
b = 0.258
tau = 34.2
r = 0.2

print(f"a : {a}\t b : {b}\t tau : {tau}")


for l in range(args.L):    

    s = glv.sim_legacy.EffectiveSystem(a, b, tau, lam)
    traj = s.trajectory(100*args.T, record_interval=100)
    vec_x = traj.mat_x[:,0]
    vec_acf = glv.comp.vec_autocorr_fft(traj.mat_x[:,0])
    vec_t = traj.vec_t[0:vec_acf.size] - traj.vec_t[0]

    a_sim = 1 - mu * S * np.mean(vec_x)
    b_sim = sig * np.sqrt( S * np.mean( vec_x**2) ) 
    tau_sim = glv.comp.fitted_exp_decay_rate(vec_acf, vec_t)
    
    # a = r * a_sim + (1 - r)*a
    # b = r * b_sim + (1 - r)*b
    # tau = r * tau_sim + (1 - r)*tau

    # print(f"a : {a}\t b : {b}\t tau : {tau}")

    print(f"a : {a_sim}\t b : {b_sim}\t tau : {tau_sim}")

#vec_t = traj.vec_t[0:vec_acf.size] - traj.vec_t[0]

if args.plot:

    fig, ax = plt.subplots()
    ax.plot(traj.vec_t, traj.mat_x[:,0])
    ax.set_yscale('log')
    fig.savefig(joinpath(currentdir, "graph_traj"))


    tau_fit = glv.comp.fitted_exponential_decay_rate(vec_acf, vec_t)
    tau_avg = glv.comp.averaged_exponential_decay_rate(vec_acf)

    fig, ax = plt.subplots()
    ax.plot(vec_t, vec_acf)
    ax.plot(vec_t, np.exp(-vec_t / tau_fit), color='k')
    ax.plot(vec_t, np.exp(-vec_t / tau_avg), color='k', linestyle="dashed")
    fig.savefig(joinpath(currentdir, "graph_acf"))


gt.stop()