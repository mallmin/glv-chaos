import os, sys
sys.path.append('./')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import glv
from matplotlib import colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import myutils

df = pd.read_csv("data/phase-chaos/data.csv")
df['tau_ratio'] = df['tau_n'] / df['tau_x']
df['nu_err'] = (df['nu_eff'] - df['nu'] ) / df['nu']

df['Cuk'] = df['u'] / df['k']
df['Ckt'] = df['k'] * df['tau_n']
df['Cut'] = df['u'] * df['tau_n']

df['rho_min'] = (1 - df['mu']) / df['sig'] / df['Neff']

df['rho_norm'] = df['rho'] / df['rho_min']

def plot_props():
    def plot_prop(ax, prop, extend='neither', **kwargs):

        X,Y,Z = glv.phase.meshgridXYZ(df, 'mu', 'sig', prop)
        #ind = np.unravel_index(np.argmax(Z, axis=None), Z.shape)
        
        pc = ax.pcolormesh(X,Y,Z, cmap='viridis', **kwargs)
        cb = ax.figure.colorbar(pc, orientation="vertical", extend=extend)

    props = ['rho_norm','rho','X','Neff','tau_ratio','Cuk','Ckt','Cut','k','u','tau_n', 'nu', 'nu_err']
    lims = {'tau_ratio' : (0.7,1.3),'k' : (None,20), 'Cuk' : (0.8,1.5),'Ckt': (None,20),'Cut': (None,20)}

    for prop in props:
        print(f"Making {prop}")

        fig, ax = plt.subplots()

        extend = 'neither'
        if prop in lims.keys():
            vmin, vmax = lims[prop]
            if (vmin is not None) and (vmax is not None):
                extend = 'both'
            elif vmin is not None:
                extend = 'min'
            elif vmax is not None:
                extend = 'max'

        else:
            vmin, vmax = (None, None)

        plot_prop(ax, prop, extend=extend, vmin=vmin, vmax=vmax)
        fig.savefig(joinpath(currentdir,f"fig_{prop}.png"), dpi=600, bbox_inches='tight',pad_inches=0,transparent=True)


def plot_tau_Neff():
    
    fig, ax = plt.subplots()

    vec_tau = df['tau_n'].to_numpy()
    vec_f = df['Neff'].to_numpy() / 500
    ax.scatter(vec_f, vec_tau, s=1)

    fig.savefig(subpath("fig_tau_vs_Neff.png"))

def plot_laws():
    vec_mu = df['mu'].to_numpy()
    vec_sig = df['sig'].to_numpy()
    vec_tau_n = df['tau_n'].to_numpy()
    vec_k = df['k'].to_numpy()
    vec_u = df['u'].to_numpy()
    vec_tau_x = df['tau_x'].to_numpy()

    fig, axs = plt.subplots(4)

    ax = axs[0]
    ax.scatter(vec_k, vec_u, s=0.1)
    #fig.savefig(joinpath(currentdir,f"law_ku.png"), dpi=600, bbox_inches='tight',pad_inches=0,transparent=True)
    ax.set_title("u vs k")

    ax = axs[1]
    ax.scatter(1 / vec_tau_n, vec_k, s=0.1)
    ax.set_title("k vs 1 / tau")
    

    ax = axs[2]
    ax.scatter(1 / vec_tau_n, vec_u, s=0.1)
    ax.set_title("u vs 1 / tau")
        
    ax = axs[3]
    ax.scatter(vec_tau_x, vec_tau_n, s=0.1)
    ax.set_title("tau_n vs tau_x")
    ax.set_aspect(1)
    ax.plot([0,1000],[0,1000],linestyle='dashed',color='k')
    
    myutils.adjust_fig_size(fig,0.5,2/5)

    fig.savefig(joinpath(currentdir,f"laws.png"), dpi=300, bbox_inches='tight',pad_inches=0,transparent=True)

plot_tau_Neff()
#plot_props()
#plot_laws()