import os, sys
sys.path.append('./glv-chaos/')
import glv
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(p): return joinpath(currentdir, p)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer
import argparse

gt = Timer("Script").start()

dat = glv.obs.Observable.load("./glv-chaos/ref-data/strong_chaos_500.pkl")
lags=501
vec_t = dat.vec_t[:lags]

class Turnover():
    def __init__(self, mat):
        
        self.mat_acf = glv.comp.autocorr_fft(mat, lags=lags, target='gpu')
        self.vec_acf = np.mean(self.mat_acf, axis=1) 

        self.vec_tau = np.zeros(dat.S)
        self.vec_r2 = np.zeros(dat.S)
        
        for i in range(dat.S):
            self.vec_tau[i], self.vec_r2[i] = glv.comp.exponential_decay_fit(self.mat_acf[:,i], vec_t)
        
        self.tau, self.r2 = glv.comp.exponential_decay_fit(self.vec_acf, vec_t)

### Data

mat_eta = glv.obs.EffectiveNoise.mat_eta_from(dat)
dat_eta = Turnover(mat_eta)
dat_x = Turnover(dat.mat_x)

vec_var = np.std(dat.mat_x, axis=0)
vec_w = vec_var / np.mean(vec_var)
vec_acf = np.mean(dat_x.mat_acf * vec_w, axis=1)
tau = glv.comp.exponential_decay_fit(vec_acf, vec_t)

print(dat_x.tau)
print(dat_eta.tau)
print(tau)

print(np.mean(vec_w))
print(np.std(vec_w))


### Plotting

# 
# fig, ax = plt.subplots()
# ax.plot(dat_x.vec_tau, dat_eta.vec_tau_x)
# ax.scatter(vec_tau_x, vec_tau_eta)
# ax.scatter(tau_x, tau_eta, marker='x',s=10)
# fig.savefig(subpath("graph_tau_comp.png"))

# Histograms
fig, ax = plt.subplots()
ax.hist(dat_x.vec_tau, bins=50, density=True, alpha=0.5)
ax.hist(dat_eta.vec_tau, bins=50, density=True, alpha=0.5)
fig.savefig(subpath("graph_tau_comp_2.png"))

# ACFs


fig, ax = plt.subplots()

ax.plot(vec_t, dat_x.vec_acf)
ax.plot(vec_t, dat_eta.vec_acf)
ax.plot(vec_t, vec_acf)

fig.savefig(subpath("graph_acf.png"))




gt.stop()