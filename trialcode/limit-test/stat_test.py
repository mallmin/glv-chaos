import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import glv
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import default_rng
from timer import Timer
import argparse


gt = Timer("Whole script").start()

parser = argparse.ArgumentParser()
parser.add_argument("-S", type = int, default = 500)
parser.add_argument("-m", "--mu", "--mean", type = np.float64, default = 0.5,  dest = 'mu')
parser.add_argument("-s", "--sig", "--sigma", type = np.float64, default = 0.3,  dest = 'sig')
parser.add_argument("-l", "--lam", "--lambda", type = np.float64, default = 10**-8, dest='lam')
parser.add_argument("--tag", default = "")
parser.add_argument("--load", action="store_true")

args = parser.parse_args()

tm = Timer("Chaos").start()
s_chaos = None
if args.load:
    s_chaos = glv.sim.UnitGLVSystem.load("./glv-chaos/pd-code/s_chaos.npz")
else:
    s_chaos = glv.sim.UnitGLVSystem(S=500, mu=0.5, sig=0.3)
    s_chaos.initialize_stationary()
    s_chaos.recorded_trajectory(100*20000,store=True, record_interval=100)
    s_chaos.save("./glv-chaos/pd-code/s_chaos")
vec_L_chaos = glv.comp.similarity_to_final(s_chaos.mat_x)
#vec_F_chaos = np.abs(np.fft.fft(vec_L_chaos))**2
tm.stop()

tm = Timer("Fixed point").start()
s_fp = None
if args.load:
    s_fp = glv.sim.UnitGLVSystem.load("./glv-chaos/pd-code/s_fp.npz")
else:
    s_fp = glv.sim.UnitGLVSystem(S=500, mu=1.25, sig=0.5)
    s_fp.initialize_stationary()
    s_fp.recorded_trajectory(100*20000,store=True, record_interval=100)
    s_fp.save("./glv-chaos/pd-code/s_fp")
vec_L_fp = glv.comp.similarity_to_final(s_fp.mat_x)
#vec_F_fp = np.abs(np.fft.fft(vec_L_fp))**2
tm.stop()

tm = Timer("Limit cycle").start()
s_cycle = None
if args.load:
    s_cycle = glv.sim.UnitGLVSystem.load("./glv-chaos/pd-code/s_cycle.npz")
else:
    s_cycle = glv.sim.UnitGLVSystem(S=500, mu=1.25, sig=0.6)
    s_cycle.initialize_stationary()
    s_cycle.recorded_trajectory(100*20000,store=True, record_interval=100)
    s_cycle.save("./glv-chaos/pd-code/s_cycle")
vec_L_cycle = glv.comp.similarity_to_final(s_cycle.mat_x)
#vec_F_cycle = np.abs(np.fft.fft(vec_L_cycle))**2
tm.stop()

#vec_c = glv.comp.count_above_relative_threshold(s.mat_x, 0.1)

# Number dominant
#fig, axs = plt.subplots(1,2)
#fig.set_size_inches(6.4*2, 4.8)
#axs[0].plot(s.vec_t, vec_c)
#axs[1].hist(vec_c)
#fig.savefig(f"./glv-chaos/pd-code/graph_count_{args.tag}")











# Classifying limit

plt.rc('font', size=16) 

fig, axs = plt.subplots(2,3)
fig.set_size_inches(6.4*2.5, 4.8*2.2)
N = s_chaos.mat_x.shape[0]
start = 0

ax = axs[0,0]
ax.plot(s_chaos.vec_t[start:], vec_L_chaos[start:])
ax.set_ylabel("$L(t,t_F)$")
ax.set_xlabel("$t$")
ax.set_title("Persistent chaos")
ax.set_xticks([0, N/2, N])

ax = axs[0,1]
ax.plot(s_fp.vec_t[start:], vec_L_fp[start:])
ax.set_xlabel("$t$")
ax.set_title("Fixed point")
ax.set_yticks([])
ax.set_xticks([0, N/2, N])

ax = axs[0,2]
ax.plot(s_cycle.vec_t[start:], vec_L_cycle[start:])
ax.set_xlabel("$t$")
ax.set_title("Limit cycle")
ax.set_yticks([])
ax.set_xticks([0, N/2, N])


### Lower row 

start = 19000

ax = axs[1,0]
ax.plot(s_chaos.vec_t[start:], vec_L_chaos[start:])
ax.set_ylabel("$L(t,t_F)$")
ax.set_xlabel("$t$")
ax.set_xticks([start, (N+start)/2, N])


ax = axs[1,1]
ax.plot(s_fp.vec_t[start:], vec_L_fp[start:])
ax.set_xlabel("$t$")
ax.set_yticks([])
ax.set_xticks([start, (N+start)/2, N])


ax = axs[1,2]
ax.plot(s_cycle.vec_t[start:], vec_L_cycle[start:])
ax.set_xlabel("$t$")
ax.set_yticks([])
ax.set_xticks([start, (N+start)/2, N])


fig.savefig(f"./glv-chaos/pd-code/graph_limit_2")

#glv.plot.stack(s,savepath="./glv-chaos/pd-code/",tag=f"{args.tag}")



# Fourier test
start = 19000
vec_sd_cycle = np.abs(np.fft.fft(vec_L_cycle[start:]))**2
vec_sd_fp = np.abs(np.fft.fft(vec_L_fp[start:]))**2
vec_sd_chaos = np.abs(np.fft.fft(vec_L_chaos[start:]))**2
fig, axs = plt.subplots(1,3)
fig.set_size_inches(6.4*2.5, 4.8*1.2)
axs[0].plot(vec_sd_chaos)
axs[1].plot(vec_sd_fp)
axs[2].plot(vec_sd_cycle)
fig.savefig("./glv-chaos/pd-code/graph_fourier")


gt.stop()

print("FP")
print(glv.comp.limit_index(s_fp.mat_x[start:,:]))
print("CHAOS")
print(glv.comp.limit_index(s_chaos.mat_x[start:,:]))
print("LIMIT CYCLE")
print(glv.comp.limit_index(s_cycle.mat_x[start:,:]))