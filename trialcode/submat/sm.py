'''
Here I look at the submatric of interactions formed by the dominant community,
and compare its statistics to a ranodm submatrix
'''


# Paths
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(".")
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

import glv
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import default_rng
from timer import Timer
import argparse
from scipy.signal import argrelextrema, argrelmax, argrelmin
import myutils as utl
from matplotlib.gridspec import GridSpec


parser = argparse.ArgumentParser()
parser.add_argument("-l", action = "store_true")
#parser.add_argument("-f", type=str, default=joinpath(currentdir,"ad.pkl"))
parser.add_argument("-t", type=str, default=joinpath(currentdir,""))
args = parser.parse_args()


gt = utl.Timer("Script").start()

traj = glv.obs.UnitGLVSeries.load("./data/traj_strong_ref.pkl")


def dominant_submatrix(vec_x, mat_alpha):
    '''Returns the submatrix of the dominant species in ``vec_x``'''

    vec_x = vec_x.reshape(1,vec_x.size)
    vec_p = vec_x / np.sum(vec_x)

    vec_thresh = glv.obs.DominantComponent.dominance_threshold(vec_x)



    idx = np.argwhere(vec_p[0,:] > vec_thresh[0]).flatten()

    #print(idx)
    
    mat_sub = mat_alpha[idx][:,idx]

    #print(mat_sub)


    return mat_sub


t_range = np.arange(1,100000,10)

vec_mu = np.zeros(t_range.shape)
vec_sig = np.zeros(t_range.shape)

mat_alpha = traj.mat_alpha
mat_x = traj.mat_x

for i, t in enumerate(t_range):
    sub_mat = dominant_submatrix(mat_x[t,:], mat_alpha)
    Ndom = sub_mat.shape[0]
    vec_mu[i] = np.sum(sub_mat) / Ndom / (Ndom - 1 )

    mat_sq = (sub_mat - vec_mu[i]) ** 2
    np.fill_diagonal(mat_sq, 0)
    vec_sig[i] = np.sqrt( np.sum(mat_sq) / Ndom / (Ndom - 1 ) )

print(f"mu = {np.mean(vec_mu)} +- {np.std(vec_mu)}")
print(f"sig = {np.mean(vec_sig)} +- {np.std(vec_sig)}")