import os, sys
sys.path.append('./glv-chaos/')
import glv
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(p): return joinpath(currentdir, p)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer
import argparse

gt = Timer("Script").start()

dat = glv.obs.Observable.load("./glv-chaos/ref-data/strong_chaos_500.pkl")

def colvec(vec):
    return vec[:,None]

vec_a = glv.obs.EffectiveNoise.vec_a_from(dat)
vec_b = glv.obs.EffectiveNoise.vec_b_from(dat)
mat_eta = glv.obs.EffectiveNoise.mat_eta_from(dat)
mat_eta_tilde = mat_eta

vec_Y = np.sqrt( np.sum(dat.mat_x ** 2, axis=1) )
mat_q = dat.mat_x / colvec(vec_Y)
mat_z = (dat.mat_alpha - dat.mu)/dat.sig
np.fill_diagonal(mat_z, 0)

mat_Xi = - ((mat_q * ( mat_eta - mat_q)) @ mat_z.T ) * colvec(vec_b)

mat_mid = mat_q* mat_q * (mat_eta - mat_q)

vec_norm =  np.sqrt(2 * np.sum(mat_mid, axis=1) * vec_b)

mat_xi = mat_Xi / colvec(vec_norm)

mat_Xi = mat_xi

lags = 200
mat_acf = glv.comp.autocorr_fft(mat_Xi, lags=lags, target='gpu')
vec_acf = np.mean(mat_acf, axis=1)


fig, ax = plt.subplots()

ax.plot(dat.vec_t[:1000], mat_Xi[:1000,:])
fig.savefig(subpath("graph_Xi"))

fig, axs = plt.subplots(2)
fig.set_size_inches(10,6)

ax = axs[0]
ax.hist(mat_Xi.flatten(),bins=100, density=True)
vec_rng = np.linspace(-3,3,100)
ax.plot(vec_rng, np.sqrt(1/(2*np.pi)) * np.exp(-vec_rng**2 / 2))

ax = axs[1]
ax.plot(dat.vec_t[:lags], vec_acf)

fig.savefig(subpath("graph_Xi_stat"))

#############

vec_h = np.sum( mat_q**2 * ( mat_eta - mat_q), axis=1) * vec_b

fig, ax = plt.subplots()
ax.plot(dat.vec_t[:1000], vec_h[:1000])
fig.savefig(subpath("graph_h"))

print(np.mean(1/vec_h))

gt.stop()