# Paths
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
grandparentdir = os.path.dirname(parentdir)
sys.path.append(parentdir)
sys.path.append(grandparentdir)
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

import numpy as np
import matplotlib.pyplot as plt
import glv
import argparse
from matplotlib import colors
from timer import Timer

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--load", action="store_true")
args = parser.parse_args()

gt = Timer("Script").start()

S = 500
dat = glv.obs.UnitGLVSeries.load(f"./glv-chaos/ref-data/strong_chaos_{S}.pkl")


if args.load:
    d = np.load(joinpath(currentdir,"ccf.npz"))
    ten_ccf = d['ten_ccf']
else:
    tm = Timer("Computing CCF").start()
    ten_ccf = glv.comp.crosscorr_fft(dat.mat_x, target='gpu')
    ten_ccf[np.isnan(ten_ccf)] = 0
    #np.savez(joinpath(currentdir,"ccf.npz"), ten_ccf=ten_ccf)
    tm.stop()


mat_c0 = ten_ccf[0,:,:]
mat_maxidx = np.argmax(ten_ccf, axis=0)
mat_cmax = np.zeros((S,S))
mat_maxt = np.zeros((S,S))
for i in range(S):
    for j in range(S):
        mat_cmax[i,j] = ten_ccf[mat_maxidx[i,j],i,j]
        mat_maxt[i,j] = dat.vec_t[mat_maxidx[i,j]]

ranks = 499
mat_rankedc0 = np.zeros((ranks,S))
mat_rankedcmax = np.zeros((ranks,S))

for i in range(S):
    vec_c0 = mat_c0[i,:]
    vec_c0.sort()    
    mat_rankedc0[:,i] = vec_c0[S-1-ranks:S-1][::-1]
    
    vec_cmax = mat_cmax[i,:]
    vec_cmax.sort()
    mat_rankedcmax[:,i] = vec_cmax[S-1-ranks:S-1][::-1]

vec_mean0 = np.mean(mat_rankedc0, axis=1)
vec_std0 = np.std(mat_rankedc0, axis=1)

vec_meanmax = np.mean(mat_rankedcmax, axis=1)
vec_stdmax = np.std(mat_rankedcmax, axis=1)

# PLOT IT

plt.rc('font', size=14) 

fig, ax = plt.subplots()
fig.set_size_inches((5.5,4))

vec_rank = np.arange(ranks) + 1

ax.hlines(0, -10,510, color='k',lw=1)

ax.fill_between(vec_rank, vec_mean0 + vec_std0, vec_mean0 - vec_std0, color='bisque')
ax.plot(vec_rank, vec_mean0, color='darkorange', lw=1)

ax.fill_between(vec_rank, vec_meanmax + vec_stdmax, vec_meanmax - vec_stdmax, color='lightskyblue')
ax.plot(vec_rank, vec_meanmax, color='midnightblue', lw=1)

ax.set_xlim([-10,ranks+10])
ax.set_xticks([1,250,499])
ax.set_aspect(500/0.4)
ax.set_yticks([-0.05, 0, 0.1,0.2,0.3])


fig.savefig(subpath("graph_c0"), dpi=300)

gt.stop()