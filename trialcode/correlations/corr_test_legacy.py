
import os, sys
sys.path.append('./glv-chaos/')
import glv
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(p): return joinpath(currentdir, p)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer
import argparse

gt = Timer("Script").start()

dat = glv.obs.Observable.load("./glv-chaos/ref-data/strong_chaos_500.pkl")
mat_eta = glv.obs.EffectiveNoise.mat_eta_from(dat)

def acf(mode):
    tm = Timer(mode).start()
    vec = glv.comp.avg_autocorr(dat.mat_x, 1000, mode)
    tm.stop()
    return vec

S = 500

tm = Timer("GPU").start()
ten_ccf = glv.comp.crosscorr_fft(dat.mat_x, target='gpu', progress=True)
tm.stop()


# tm = Timer("CPU").start()
# mc = glv.comp.autocorr_fft(mat_eta)
# vc = np.mean(mc,axis=1)
# tm = Timer("CPU").start()

# tm = Timer("GPU").start()
# mg = glv.comp.autocorr_fft(mat_eta,target='gpu')
# vg = np.mean(mg,axis=1)
# tm.stop()

#vec_cpu_fft = acf('cpu-fft')
# # vec_cpu_direct = acf('cpu-direct')
# # np.savez(joinpath(currentdir,"direct.npz"), x = vec_cpu_direct)
# vec_cpu_direct = np.load(joinpath(currentdir,"direct.npz"))['x']
# vec_cpu_scipy = acf('cpu-scipy')
# vec_gpu_fft = acf('gpu-fft')

# # tm = Timer("GPU").start()
# # ten_corr = glv.comp.correlation_tensor_gpu(mat_eta, print_progress=True)
# # vec_acf_gpu = np.zeros(200)
# # for i in range(dat.S):
# #     vec_acf_gpu += ten_corr[:,i,i]
# # vec_acf_gpu /= dat.S
# # tm.stop()


fig, ax = plt.subplots()

tm = Timer("Plot").start()
# ax.plot(vc)
# ax.plot(vg)
for i in range(S):
    for j in range(i,S):
        ax.plot(ten_ccf[:,i,j])

tm.stop()

fig.savefig(subpath("DIST"),dpi=100)

gt.stop()