'''
Script to produce the data and graphs demonstrating how the behaviour of the chaotic phase scales with S
'''

import os, sys
sys.path.append('./glv-chaos/')
import glv
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer
import argparse

gt = Timer("Script").start()

parser = argparse.ArgumentParser()
parser.add_argument("--load", action = "store_true")
parser.add_argument("--sim", action = "store_true")
parser.add_argument("--plot", action = "store_true")
parser.add_argument("-T", type=int, default=2000)
parser.add_argument("-S", type=int, nargs='+')
parser.add_argument("--mu",  type = np.float64, default = 0.5)
parser.add_argument("--sig", type = np.float64, default = 0.3)
parser.add_argument("--lam", type = str)
parser.add_argument("--regime", type=str, default="strong")
parser.add_argument("--save-dir", type=str, default="pickles") # relative!
parser.add_argument("--graph-dir", type=str, default="graphs") # relative!


args = parser.parse_args()


# All observable types
dct_afd = {} # Abundance fluctuation distribution
dct_cumrd = {} # Cumulative rank distribution
dct_rect = {} # recurrence times
dct_spvar = {} # species variability 

dct_obs = { 'afd' : {}, 'cumrd' : {}, 'rect' : {}, 'spvar' : {}}

savedir = joinpath(currentdir, args.save_dir)

if args.sim:

    # bins for AFD
    bins = np.logspace(-12, 2, 76)
 
    for S in args.S:

        tm = Timer(f"Simulating S={S}").start()

        afd = glv.obs.AbundanceDistributions(bins, batch_size=2000,label=f"afd_{S}")
        cumrd = glv.obs.RankAbundanceDistribution(r_min=50, label=f"cumrd_{S}")
        rect = glv.obs.RecurrenceDistribution(0.01, label=f"rect_{S}")
        spvar = glv.obs.SpeciesVariability(bins, label=f"spvar_{S}")
        all_obs = [afd, cumrd, rect, spvar]

        dct_obs['afd'][S] = afd
        dct_obs['cumrd'][S] = cumrd
        dct_obs['rect'][S] = rect
        dct_obs['spvar'][S] = spvar 

        if args.regime == 'strong':
            sc = 1
        elif args.regime == 'weak':
            sc = S

        mu = args.mu / sc
        sig = args.sig / np.sqrt(sc)
        
        s = glv.sim_legacy.UnitGLVSystem(S, mu=mu, sig=sig, gam=0, lam=args.lam)
        s.discard_transient(100*1000)
        s.generate_observables(all_obs, 100*args.T, record_interval=100)
        
        if not os.path.exists(savedir):
            os.mkdir(savedir)
        
        for obs in all_obs:            
            obs.save(joinpath(savedir, obs.label+".pkl"))
        
        tm.stop()

if args.load:
    list_sv = []

    for filename in os.listdir(savedir):
        f = joinpath(savedir, filename)
        if os.path.isfile(f):
            key_obs, S_str = filename.split('.')[0].split('_')
            S = int(S_str)
            if S in args.S:
                dct_obs[key_obs][S] = glv.obs.Observable.load(f)


if args.plot:

    DPI = 500
    DEFAULT_FONTSIZE = 12
    XSIZE = 3
    YSIZE = 3
    XSIZE_INSET = 1
    YSIZE_INSET = 1

    graphdir = joinpath(currentdir,args.graph_dir)
    if not os.path.exists(graphdir):
            os.mkdir(graphdir)

    def format_size(a):
        a.set_size_inches(XSIZE, YSIZE)

    plt.rc('font', size=DEFAULT_FONTSIZE) 

    t10 = plt.cm.tab10
    colors = {320 : t10(0), 640 : t10(1), 1280 : t10(2), 2560 : t10(3), 5120 : t10(4), 10240 : t10(5)}


    def plot_afd(ax):    
        for S, afd in dct_obs['afd'].items():
            #ax.plot(np.log10(afd.bins_mid), np.ma.log10(np.ma.masked_less_equal(afd.vec_dist,0)), color=colors[S])
            ax.plot(afd.bins_mid, afd.vec_dist, color=colors[S])
        ax.set_xscale('log')
        ax.set_yscale('log')
        # ax.set_xlabel("log$_{10}$( abundance )")
        # ax.set_ylabel("log$_{10}$( density )")
        ax.set_xlabel("abundance")
        ax.set_ylabel("density")

    def plot_cumrd(ax):      
        for S, cumrd in sorted(dct_obs['cumrd'].items()):
            ax.plot(cumrd.vec_r[:20], cumrd.vec_cum[:20], marker='o', label=f"{S}", color=colors[S])
        ax.set_xlim([1,20])
        ax.set_ylim([0,1])
        ax.set_xticks([1,5,10,15,20])
        # ax.set_yticks()
        ax.set_xlabel("rank")
        ax.set_ylabel("cum. rel. abundance")
        ax.legend(title="species, S")

    def plot_rect(ax):
        for S, rect in dct_obs['rect'].items():
            if rect.vec_intervals.size > 0:
                t_max = np.max(rect.vec_intervals)*1.05            
            else:
                t_max = 100
            bins = np.linspace(0,t_max,51)
            bins_mid, dist = rect.interval_distribution(bins)
            #ax.plot(bins_mid, np.ma.log10(np.ma.masked_less_equal(dist,0)), color=colors[S])
            ax.plot(bins_mid, dist, color=colors[S])
        ax.set_yscale('log')
        ax.set_xlabel("rarity time (x<0.01)")
        ax.set_ylabel("density")
        ax.locator_params(axis='x', nbins=4)

    def plot_pert(ax):
        for S, rect in dct_obs['rect'].items():
            if rect.vec_durations.size > 0:
                t_max = np.max(rect.vec_durations)*1.05            
            else:
                t_max = 100
            bins = np.linspace(0,t_max,51)
            bins_mid, dist = rect.duration_distribution(bins)
            ax.plot(bins_mid, dist, color=colors[S])
        ax.set_yscale('log')
        ax.set_xlabel("abundance time (x>0.01)")
        ax.set_ylabel("density")

    def plot_spvar(ax):   
        for S, spvar in dct_obs['spvar'].items():
            #ax.plot(np.log10(np.arange(args.T)[1:]), np.log10(spvar.vec_V[1:]), color=colors[S])
            ax.plot(np.arange(args.T)[1:], spvar.vec_V[1:], color=colors[S])

        ax.set_xscale('log')
        ax.set_yscale('log')
        # ax.set_xlabel("log$_{10}$( time )")
        # ax.set_ylabel(r"log$_{10}$( species variability )")
        ax.set_xlabel("time")
        ax.set_ylabel("species variability")
        
    def plot_ind_afd(ax):
        for S, afd in dct_obs['afd'].items():
            for i in range(50):#range(afd.mat_dist.shape[0]):
                ax.plot(afd.bins_mid, afd.mat_dist[i,:], color=colors[S],lw=0.1)
        ax.set_xscale('log')
        ax.set_yscale('log')
        # ax.set_xlabel("log$_{10}$( abundance )")
        # ax.set_ylabel("log$_{10}$( density )")
        ax.set_xlabel("abundance")
        ax.set_ylabel("density")


    plt.rc('xtick', labelsize=8)
    plt.rc('ytick', labelsize=8)

    # 2x2 tableau

    fig, axs = plt.subplots(2,2, figsize=(8,8))
    fig.tight_layout(pad=2)
    plot_afd(axs[0,0])
    plot_cumrd(axs[0,1])
    plot_rect(axs[1,0])
    plot_spvar(axs[1,1])
    fig.savefig(joinpath(graphdir, f"graph_scaling"),dpi=DPI)


    # fig, ax = plt.subplots()
    # plot_pert(ax)
    # fig.savefig(joinpath(currentdir, f"graph_PersistenceTime"), dpi=DPI)

    # fig.savefig(joinpath(currentdir, f"graph_AFD"),dpi=DPI)
    # fig.savefig(joinpath(currentdir, f"graph_CumRD"), dpi=DPI)
    # fig.savefig(joinpath(currentdir, f"graph_RecTime"), dpi=DPI)
    # fig.savefig(joinpath(currentdir, f"graph_SpeciesVar"),bbox_inches='tight', dpi=DPI)
    
    # fig, ax = plt.subplots()
    # plot_ind_afd(ax)
    # fig.savefig(joinpath(currentdir, f"graphs-{args.regime}", f"graph_IndAFD"), dpi=DPI)
    

gt.stop()