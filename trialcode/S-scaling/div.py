'''Test if the numerical formula min(A+I)1>0 predicts the divergence curve'''

import os, sys
sys.path.append('./glv-chaos/')
import glv
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer


S = 10000
A = glv.sim.gaussian_random_matrix(S, 0.5/S,0.3/np.sqrt(S))
I = np.eye(S)
vec_1 = np.ones(S)


vec_x = np.linalg.inv(A+I) @ vec_1

min = np.amin( vec_x )
max = np.amax( vec_x )

print(min)
print(max)




