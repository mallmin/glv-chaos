#!/bin/bash

#SBATCH -p gpu  
#SBATCH -J S-sc 
#SBATCH -N 1 
#SBATCH -n 1
#SBATCH --mem-per-cpu 5000 
#SBATCH -t 72:00:00
#SBATCH -o ./glv-chaos/S-scaling/log/out.out
#SBATCH -e ./glv-chaos/S-scaling/log/err.err
 
python3 ./glv-chaos/S-scaling/scaling.py -T 200000 -S 10240 --sim