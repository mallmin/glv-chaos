


import os, sys
sys.path.append('./glv-chaos/')
import glv
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer



s = glv.sim.UnitGLVSystem(S=500, mu=0.5, sig=0.3, lam=10**-8)
s.discard_transient(100*1000)

s.trajectory(100*5000, store=True)

T = s.mat_x.shape[0]
S = s.mat_x.shape[1]

mat_p = s.mat_x / np.sum(s.mat_x, axis=1)[:,None]



Xt = np.sum(s.mat_x, axis=1)
X = np.sum(s.mat_x) / T

phi =  np.sum(mat_p ** 2) / T
phi_real = np.sum( np.sum(mat_p ** 2, axis=1) * Xt / X )/ T

phi_prime = np.sum( mat_p * (mat_p @ (s.mat_alpha.T - s.mu)/s.sig  ) ) / T
phi_prime_real = np.sum(np.sum( mat_p * (mat_p @ (s.mat_alpha.T - s.mu)/s.sig  ), axis=1) * Xt / X) / T

igr = s.lam * S * np.sum( 1 / Xt ) / T

X_pred = 1 / (s.mu + (1- s.mu)*phi + s.sig * phi_prime)

print(f"igr={igr}")
print(f"phi={phi}, phi_real={phi_real}")
print(f"phi_prime={phi_prime}, phi_prime_real={phi_prime_real}")
print(f"X={X}, X_pred={X_pred}")