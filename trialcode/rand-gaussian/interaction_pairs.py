import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import glv
import numpy as np
import matplotlib.pyplot as plt
from timer import Timer
from numpy.random import default_rng
from matplotlib.gridspec import GridSpec
import argparse
from scipy import integrate
import pandas as pd

gt = Timer("Script").start()

parser = argparse.ArgumentParser()
parser.add_argument("--load", action= 'store_true')
args = parser.parse_args()

dat_path = "./glv-chaos/rand-gaussian/dat.pkl"
graph_dir = "./glv-chaos/rand-gaussian/"

if args.load:
    tab = pd.read_pickle(dat_path) 
else:
    gam = 0.0
    infty = 10.
    vec_mu = np.arange(0.05, 1.2, 0.05)
    vec_sig = np.arange(0.05, 1.2, 0.05)
    list_mu_sig = []
    for mu in vec_mu:
        for sig in vec_sig:
            list_mu_sig.append((mu,sig))
    N = len(list_mu_sig)

    dat = {'mu': np.empty(N), 'sig' : np.empty(N), 'f_com':np.empty(N), 'f_para' : np.empty(N), 'f_mut' : np.empty(N),
     'mu_com' : np.empty(N), 'mu_pred' : np.empty(N), 'mu_prey' : np.empty(N), 'mu_mut' : np.empty(N), 'a_com' : np.empty(N), 'a_para' : np.empty(N), 'a_mut' : np.empty(N) }

    def p2(x,y,mu,sig,gam):
        norm = 2*np.pi*sig**2 * np.sqrt(1-gam**2)
        exponent = 1/( 2* sig**2 * (1 - gam**2)) * ( (x - mu)**2 - 2*gam*(x-mu)*(y-mu) + (y - mu)**2 )
        return np.exp(- exponent) / norm

    for i, (mu,sig) in enumerate(list_mu_sig):
        print(f"{i}/{len(list_mu_sig)}")

        p2_temp = lambda x,y: p2(x,y, mu, sig, gam)
        xp2_temp = lambda x,y: x * p2(x,y, mu, sig, gam)
        asym_p2_temp = lambda x,y: np.abs(x-y) * p2(x,y, mu, sig, gam)

        c = lambda a: (lambda x: a)
        def double_quad(f, xlims, ylims):
            return integrate.dblquad(f, ylims[0], ylims[1], c(xlims[0]), c(xlims[1]))[0]
        
        f_com = double_quad(p2_temp, (0, infty), (0, infty))
        f_para = 2 * double_quad(p2_temp, (0, infty), (-infty, 0))
        f_mut = double_quad(p2_temp, (-infty, 0), (-infty, 0))
        f_all = double_quad(p2_temp, (-infty, infty), (-infty, infty))

        mu_com = double_quad(xp2_temp, (0, infty), (0, infty)) / f_com
        mu_pred = double_quad(xp2_temp, (-infty, 0), (0, infty)) / (f_para / 2)
        mu_prey = double_quad(xp2_temp, (0, infty), (-infty, 0)) / (f_para / 2)
        mu_mut = double_quad(xp2_temp, (-infty, 0), (-infty, 0)) / f_mut

        a_com = double_quad(asym_p2_temp, (0, infty), (0, infty)) / f_com
        a_para = double_quad(asym_p2_temp, (-infty, 0), (0, infty)) / (f_para / 2)
        a_mut = double_quad(asym_p2_temp, (-infty, 0), (-infty, 0)) / f_mut

        dat['mu'][i] = mu
        dat['sig'][i] = sig
        dat['f_com'][i] = f_com
        dat['f_para'][i] = f_para
        dat['f_mut'][i] = f_mut
        dat['mu_com'][i] = mu_com
        dat['mu_pred'][i] = mu_pred
        dat['mu_prey'][i] = mu_prey
        dat['mu_mut'][i] = mu_mut
        dat['a_com'][i] = a_com
        dat['a_para'][i] = a_para
        dat['a_mut'][i] = a_mut

    tab = pd.DataFrame(dat)
    tab.to_pickle(dat_path)


##### PLOT

list_mu_unique = tab["mu"].drop_duplicates().to_numpy(float).tolist()
list_sig_unique = tab["sig"].drop_duplicates().to_numpy(float).tolist()
vec_mu_ordered = np.sort(np.array(list_mu_unique))
vec_sig_ordered = np.sort(np.array(list_sig_unique))
N1 = len(list_mu_unique)
N2 = len(list_sig_unique)

Z_f_com = np.zeros((N1,N2))
Z_f_para =  np.zeros((N1,N2))
Z_f_mut =  np.zeros((N1,N2))
Z_mu_com =  np.zeros((N1,N2))
Z_mu_pred =  np.zeros((N1,N2))
Z_mu_prey =  np.zeros((N1,N2))
Z_mu_mut =  np.zeros((N1,N2))
Z_a_com =  np.zeros((N1,N2))
Z_a_para =  np.zeros((N1,N2))
Z_a_mut =  np.zeros((N1,N2))

for i in range(N1):
    for j in range(N2):
        mu = vec_mu_ordered[i]
        sig = vec_sig_ordered[j]

        row = tab[(tab['mu'] == mu) & (tab['sig'] == sig)]

        Z_f_com[i,j] = row.loc[:,'f_com'].values[0]
        Z_f_para[i,j] = row.loc[:,'f_para'].values[0]
        Z_f_mut[i,j] = row.loc[:,'f_mut'].values[0]
        Z_mu_com[i,j] = row.loc[:,'mu_com'].values[0]
        Z_mu_pred[i,j] = row.loc[:,'mu_pred'].values[0]
        Z_mu_prey[i,j] = row.loc[:,'mu_prey'].values[0]
        Z_mu_mut[i,j] = row.loc[:,'mu_pred'].values[0]
        Z_a_com[i,j] = row.loc[:,'a_com'].values[0]
        Z_a_para[i,j] = row.loc[:,'a_para'].values[0]
        Z_a_mut[i,j] = row.loc[:,'a_mut'].values[0]

Z = {'f_com': Z_f_com, 'f_para' : Z_f_para, 'f_mut' : Z_f_mut, 'mu_com' : Z_mu_com, 'mu_pred' : Z_mu_pred, 'mu_prey' : Z_mu_prey, 'mu_mut' : Z_mu_mut, 'a_com' : Z_a_com, 'a_para' : Z_a_para, 'a_mut' : Z_a_mut}
labels = {'f_com': "fraction competing", 'f_para' : "fraction predator-prey", 'f_mut' : "fraction mutualists", 'mu_com' : "mean competing", 'mu_pred' : "mean predator",
 'mu_prey' : "mean prey", 'mu_mut' : "mean mutualists", 'a_com' : "asymmetry competing", 'a_para' : "asymmetry predator-prey", 'a_mut' : "asymmetry mutualists"}

X, Y = np.meshgrid(vec_mu_ordered, vec_sig_ordered)

for prop in labels:
    fig, ax = plt.subplots()
    fig.set_size_inches(8, 8)
    pc = ax.pcolormesh(X, Y, Z[prop].T, cmap='spring',  shading='gouraud')#plt.cm.jet)
    ax.set_aspect('equal', 'box')
    ax.yaxis.set_ticks([0.0,0.2,0.4,0.6,0.8,1.0,1.2]) 
    ax.xaxis.set_ticks([0.0,0.2,0.4,0.6,0.8,1.0,1.2]) 
    fig.colorbar(pc, orientation='horizontal', label=labels[prop])    
    fig.savefig(f"{graph_dir}graph_{prop}")


fig, axs = plt.subplots(3,1)
fig.set_size_inches(8, 8)

## Line plots

selected_sigs = [0.2, 0.4, 0.4+0.05+0.05]

for i in range(3):
    for j in range(1):
        tab_sig = tab[tab['sig'] == selected_sigs[i]].sort_values(by=['mu'])
        vec_mu = tab_sig['mu'].to_numpy()
        vec_f_com = tab_sig['f_com'].to_numpy()
        vec_f_para = tab_sig['f_para'].to_numpy()
        vec_f_mut = tab_sig['f_mut'].to_numpy()
        
        mat_f = np.empty((3, vec_mu.size))
        mat_f[0,:] = vec_f_com
        mat_f[1,:] = vec_f_para
        mat_f[2,:] = vec_f_mut

        #axs[i].stackplot(vec_mu, mat_f)
        axs[i].plot(vec_mu, mat_f.T)
        axs[i].set_yscale('log')

fig.savefig(f"{graph_dir}graph_frac")



# Competitors, non-competitors

from mpl_toolkits.axes_grid1 import make_axes_locatable

def colorbar(mappable):
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    return fig.colorbar(mappable, cax=cax)

plt.rc('font', size=16) 


fig, axs = plt.subplots(1,2)
fig.set_size_inches(14, 6)
ax = axs[0]
ax.set_aspect('equal', adjustable='box')
pc = ax.pcolormesh(X, Y, Z_f_com.T, cmap='spring',  shading='gouraud', vmin=0., vmax=1.)
colorbar(pc)   
ax.set_xlabel("$\mu$",fontsize=20)
ax.set_ylabel("$\sigma$",fontsize=20)

ax = axs[1]
Z_f_rel_mut = Z_f_mut / (Z_f_mut + Z_f_para) #fraction of mutualists out of all non-competitors 
ax.set_aspect('equal', adjustable='box')
pc = ax.pcolormesh(X, Y, Z_f_rel_mut.T, cmap='winter',  shading='gouraud', vmin=0.)
colorbar(pc)
ax.set_xlabel("$\mu$",fontsize=20)
ax.set_yticks([])

fig.savefig(os.path.join(graph_dir, "graph_f"))

#lower right
print(f"{(Z_f_com)[-1,0]}, {(Z_f_para)[-1,0]}, {(Z_f_mut)[-1,0]}")
#middle
idx1 = int(N1/2)
idx2 = int(N2/2)
print(f"{(Z_f_com)[idx1,idx2]}, {(Z_f_para)[idx1,idx2]}, {(Z_f_mut)[idx1,idx2]}")
#upper left
print(f"{(Z_f_com)[0,-1]}, {(Z_f_para)[0,-1]}, {(Z_f_mut)[0,-1]}")
## Maxwell-plot

# C_f = np.zeros(shape=(N1,N2,3))

# for i in range(N1):
#     for j in range(N2):
#         C_f[i,j,0] = 1. #Z_f_com[i,j]
#         C_f[i,j,1] = 1. #Z_f_para[i,j]
#         C_f[i,j,2] = Z_f_mut[i,j]
#         C_f[i,j,:] /= np.sum(C_f[i,j,:])

# print(np.min(C_f.flatten()))
# print(np.max(C_f.flatten()))

# print(Z_f_mut)

# fig, ax = plt.subplots()
# fig.set_size_inches(8, 8)
# ax.imshow(C_f, extent=[X[0,0], X[0,-1], Y[0,0], Y[-1,0]])
# fig.savefig(os.path.join(graph_dir, "graph_f2"))





gt.stop()