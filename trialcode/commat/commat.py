import os, sys
sys.path.append('./glv-chaos/')
import glv
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--load", action = "store_true")
parser.add_argument("--sim", action = "store_true")
parser.add_argument("--plot", action = "store_true")
parser.add_argument("-T", type=int, default=1000)
parser.add_argument("-S", type=int, nargs='+')
parser.add_argument("--mu",  type = np.float64, default = 0.5)
parser.add_argument("--sig", type = np.float64, default = 0.3)
parser.add_argument("--lam", type = np.float64,  default = 10**-8)
parser.add_argument("--regime", type=str, default="strong")
args = parser.parse_args()


gt = Timer("Script").start()

def community_matrix(mat_x, log=True):
    mat_p = mat_x / np.sum(mat_x,axis=1)[:,None]
    vec_m = np.mean(mat_x, axis=0) 
    vec_p_ordered = mat_p[:, np.argsort(vec_m)[::-1]]
    if log:
        return np.log10(vec_p_ordered)
    else:
        return vec_p_ordered 


T = 5000

dat_s = glv.obs.Observable.load("./glv-chaos/ref-data/strong_chaos_500.pkl")
mat_m_s = community_matrix(dat_s.mat_x[:T,:])
dat_w = glv.obs.Observable.load("./glv-chaos/ref-data/weak_chaos_500.pkl")
mat_m_w = community_matrix(dat_w.mat_x[:T,:])

vmin = min( np.amin(mat_m_s), np.amin(mat_m_w))
vmax = max( np.amax(mat_m_s), np.amax(mat_m_w))


fig, axs = plt.subplots(2,1)

ax = axs[0]
ims = ax.imshow(mat_m_s.T,cmap='Greys_r',vmin=vmin,vmax=vmax)
ax.set_yticks([0,499],[1,500])
ax.set_xticks([])
#plt.colorbar(ims,orientation='horizontal',ax=ax)


ax = axs[1]
ims = ax.imshow(mat_m_w.T, cmap='Greys_r',vmin=vmin,vmax=vmax)
ax.set_yticks([0,499],[1,500])
plt.colorbar(ims,orientation='horizontal',ax=ax)

plt.tight_layout()

fig.savefig(joinpath(currentdir,"graph_SvsT"), dpi=400)

gt.stop()