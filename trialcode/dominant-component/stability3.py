import os, sys
sys.path.append('./')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(p): return joinpath(currentdir, p)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer
import argparse
import glv


gt = Timer("Script").start()

dat = glv.obs.Observable.load("./data/traj_strong_ref.pkl")

ts = 1 # spacing between slices
ns = 250 # number of slices
teq = 500 # "equilibration" time
tmax = dat.vec_t.size # maximum length of data
tfix = 100 # last times of equilibration period whose start and end are compared to test convergence
assert(ns*ts < tmax)

colors = glv.rand_cmap(dat.S, type='soft')


vec_ts = dat.vec_t[:ns*ts:ts]
vec_d_bc = np.zeros(ns)
vec_d_alt = np.zeros(ns)
vec_fix = np.zeros(ns)

def dist_bray_curtis(x,y):
    return 2 * np.sum( np.minimum(x,y)) / (np.sum(x+y))

def dist_stretched(x,y):
    return 2 * np.dot(x,y) / (np.linalg.norm(x)**2 + np.linalg.norm(y)**2 )

def has_fixated(mat_x, T=100, tol=10**-4):
    '''
    1 : yes
    0 : no
    -1 : diverged 
    '''
    T = min(T,mat_x.shape[0])

    mat_x = mat_x[-T:,:]
    if np.isnan(mat_x).any():
        return -1
    
    vec_x_final = mat_x[-1,:]
    vec_L = 2 * mat_x @ vec_x_final / ( vec_x_final @ vec_x_final + np.linalg.norm(mat_x,axis=1)**2 )
    vec_L_sign = np.sign(vec_L - (1 - tol))
    index = np.sum( vec_L_sign[:-1] * vec_L_sign[1:] < 0)
    
    if index == 0:
        return 1
    else:
        return 0


########

vec_k = np.ceil( np.round(1 / glv.obs.Diversity.Simpson(dat.mat_x)) )   # number of dominant

s = glv.sim_legacy.UnitGLVSystem(dat.S, dat.dt, dat.mu, dat.sig, gam=0., lam=0., mat_alpha = dat.mat_alpha )

for n in range(ns):
    t = n*ts
    print(f"Progress {int(100*n/ns)}%", end='\r')

    # find abundance of nth largest
    
    k = vec_k[t]

    # Get the abundance of the kth greatest element
    min_x = np.partition(dat.mat_x[t,:], int(dat.S - k) ) [int(dat.S - k) ]

    # initial vector with abundances of S-k-1 smallest set to zero
    vec_x0_dom = dat.mat_x[t,:] * (dat.mat_x[t,:] >= min_x)
    
    s.vec_x = vec_x0_dom
    vec_t, mat_x = s.trajectory(100*teq, record_interval=100)
    
    # the candidate equilibrium
    vec_xf_dom = mat_x[-1,:]

    idx = has_fixated(mat_x, T=tfix)
    d_bc = dist_bray_curtis(vec_x0_dom, vec_xf_dom)
    d_alt = dist_stretched(vec_x0_dom, vec_xf_dom)

    vec_d_bc[n] = d_bc
    vec_d_alt[n] = d_alt
    vec_fix[n] = idx

print()


print(np.amin(vec_fix))
print(vec_fix)

vec_X = np.sum( dat.mat_x[:ns*ts,:],axis=1)
vec_tX = dat.vec_t[:ns*ts]

fig, axs = plt.subplots(2)

ax = axs[0]
ax.stackplot(vec_ts, dat.mat_x[:ns*ts,:].T, colors=colors)

#ax.plot(vec_ts, vec_d_alt)
#ax.plot(vec_ts, vec_fix)
#ax.plot(vec_tX, vec_X)

ax = axs[1]
ax.plot(vec_ts, vec_d_bc)

fig.savefig(subpath("simil3"))



gt.stop()