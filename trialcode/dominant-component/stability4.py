import os, sys
sys.path.append('./')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer
import argparse
import glv


gt = Timer("Script").start()

colors = glv.rand_cmap(dat.S, type='soft')

T = 100
traj = glv.obs.Observable.load("./data/traj_strong_ref.pkl")
traj.lam = 10**-8 #forgot to save this...

traj.mat_x = traj.mat_x[:T,:]
traj.vec_t = traj.vec_t[:T]

obs = glv.obs.ClosenessToEquilibrium(1,None,1000,200)
obs.from_trajectory(traj)


fig, axs = plt.subplots(2)

ax = axs[0]
ax.plot(traj.vec_t, obs.vec_c)
ax.plot(traj.vec_t, obs.vec_fix)

ax = axs[1]
ax.plot(traj.vec_t, obs.vec_ndom)
ax.plot(traj.vec_t, obs.vec_next)

fig.savefig(joinpath(currentdir,"closeness"))

gt.stop()
