import os, sys
sys.path.append('./glv-chaos/')
import glv
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(p): return joinpath(currentdir, p)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer
import argparse

gt = Timer("Script").start()

dat = glv.obs.Observable.load("./glv-chaos/ref-data/strong_chaos_500.pkl")


s = glv.sim_legacy.UnitGLVSystem(dat.S, dat.dt, dat.mu, dat.sig, gam=0., lam=0., mat_alpha = dat.mat_alpha )

theta = 0.01

T = 1000
n = 50

assert(n*T <= dat.vec_t.size)

N = dat.mat_x.shape[0]
S = dat.mat_x.shape[1]

mat_x = dat.mat_x[:n*T:T,:]

mat_x2 = np.zeros((n*T,S))

for t in range(mat_x.shape[0]):
    print(f"Progress {int(100*t/mat_x.shape[0])}%", end='\r')
    vec_x0 = mat_x[t,:] * (mat_x[t,:] > theta)
    s.vec_x = vec_x0
    _ , mat_x_add = s.trajectory(100*T, record_interval=100) 
    mat_x2[t*T:(t+1)*T,:] = mat_x_add
print()
    
fig, ax = plt.subplots()
colors = glv.rand_cmap(dat.S, type='soft')
ax.stackplot(dat.vec_t[:n*T], mat_x2[:n*T,:].T, colors=colors)

fig.savefig(subpath("stab"),dpi=200)

# vec_x0 = dat.mat_x[n*T,:] * (dat.mat_x[n*T,:] > theta)
# s.vec_x = vec_x0
# vec_t_new, mat_x_new = s.trajectory(100*10000, record_interval=100) 


# fig, ax = plt.subplots()
# colors = glv.rand_cmap(dat.S, type='soft')
# ax.stackplot(vec_t_new, mat_x_new.T, colors=colors)
# fig.savefig(subpath("stab2"),dpi=200)


gt.stop()