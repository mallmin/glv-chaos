'''
Dynamics with a structuerd matrix
'''

import os, sys
sys.path.append('./')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import matplotlib.pyplot as plt
import argparse
import glv
import myutils as utl

S = 500
K = 20

m = 0
std = 1

U = np.random.uniform(size=(S,K))
C = np.random.normal(m,std,size=(K,K))
ones = np.ones(K)

v1 = (U @ C) @ ones
v2 = (U @ C.T) @ ones

A = np.zeros((S,S))
for i in range(S):
    for j in range(S):
        A[i,j] = v1[i] - v2[j] 

mu = np.mean(A)
sig = np.std(A)
A = 0.5 - mu/sig + 0.3 / sig * A
mu = np.mean(A)
sig = np.std(A)

print(f"mean {mu} std {sig}")

fig, ax = plt.subplots()
ax.imshow(A)


s = glv.sim_legacy.UnitGLVSystem(S=S,mu=mu,sig=sig,mat_alpha=A)

traj = glv.obs.AbundanceSeries()

s.generate_observables(traj, 100*5000, record_interval=100)

fig, ax = plt.subplots()
ax.stackplot(traj.vec_t, traj.mat_x.T)

bins = np.logspace(-10,2,50)
#dist = glv.obs.AbundanceDistributions(bins)
#dist.from_trajectory(traj)

fig, ax = plt.subplots()
#ax.plot(dist.bins_mid, dist.vec_dist)
ax.hist(traj.mat_x[-1,:], bins=bins)
#ax.set_yscale('log')
ax.set_xscale('log')



plt.show()


