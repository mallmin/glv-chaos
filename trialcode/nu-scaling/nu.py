'''
Loads reference data for different S and lam and extracts the nu value and plots vs S or lam.
'''

import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
gparentdir = os.path.dirname(parentdir)
sys.path.append(parentdir)
sys.path.append(gparentdir)
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

import glv
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import default_rng
from timer import Timer
import argparse

gt = Timer("Script").start()


## S-scaling ######################################

vec_S = np.array([320, 640, 1280, 2560, 5120, 10240])

if False:
    bins = np.logspace(-10,2,50, base=10)
    vec_nu_S = np.zeros(vec_S.size)
    for i in range(vec_S.size):
        dat = glv.obs.UnitGLVSeries.load(f"./glv-chaos/ref-data/strong_chaos_{vec_S[i]}.pkl")
        abd = glv.obs.AbundanceDistributions(bins)
        abd.from_trajectory(dat)
        vec_nu_S[i] = abd.nu
        del dat
        del abd

    print(vec_S)
    print(vec_nu_S)
else:
    vec_nu_S = np.array([1.147824, 1.18975841, 1.2307742, 1.27156674, 1.3143631, 1.35728056])

fig, ax = plt.subplots()
ax.plot(vec_S, vec_nu_S - 1)
ax.scatter(vec_S, vec_nu_S - 1)
ax.set_xscale('log')
fig.savefig(joinpath(currentdir,"graph_nu_vs_S"))

## lam-scaling ######################################

lam_tags = ['lam6','500','lam10','lam12','lam14','lam16','lam20','lam24','lam28']#,'lam32']
vec_lam = np.array([1e-6,1e-8,1e-10,1e-12,1e-14,1e-16,1e-20,1e-24,1e-28])#,1e-32]) 

if True:
    bins = np.logspace(-10,2,50, base=10)
    vec_nu_lam = np.zeros(len(lam_tags))

    for i in range(len(lam_tags)):
        dat = glv.obs.UnitGLVSeries.load(f"./glv-chaos/ref-data/strong_chaos_{lam_tags[i]}.pkl")
        abd = glv.obs.AbundanceDistributions(bins, powerlaw_cutoff = [vec_lam[i]*1000, 1e-2] )
        abd.from_trajectory(dat)
        vec_nu_lam[i] = abd.nu
        del dat
        del abd
        print(f"{vec_lam[i]}   {vec_nu_lam[i]}")

    #print(vec_lam)
    #print(vec_nu_lam)
else:
    vec_nu_lam = np.array([1.19376476, 1.15047998, 1.11728494, 1.10289406, 1.08315323, 1.06617158, 1.04480842, 1.0341527,  1.02349139])

fig, ax = plt.subplots()
ax.plot(1/np.log10(vec_lam), vec_nu_lam - 1)
ax.scatter(1/np.log10(vec_lam), vec_nu_lam - 1)
#ax.plot(vec_lam, vec_nu_lam - 1)
#ax.scatter(vec_lam, vec_nu_lam - 1)
#ax.set_xscale('log')
#ax.set_yscale('log')
fig.savefig(joinpath(currentdir,"graph_nu_vs_lam"))


gt.stop()