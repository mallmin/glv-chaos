### SETUP #########################################################

# Paths

import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(".")
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

# Imports

import glv
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import default_rng
from myutils import Timer
import argparse
import pandas as pd
import myutils as utl

# Argument parsing

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--load", action="store_true")
parser.add_argument("-t", "--tag", type=str)
parser.add_argument("-d", "--data", type=str, default="./data/traj_strong_ref.pkl")
parser.add_argument("--target", type=str, default="gpu")


args = parser.parse_args()

# Timing

gt = Timer("Script").start()

### FUNCTIONS #####################################################

# Data manipulation

def load_tensor():
    '''
    Load the cross-correlation tensor from pickle
    '''

    filename = f"ccf_{args.tag}.npz"    
    d = np.load(joinpath(currentdir,filename))
    ten_ccf = d['ten_ccf']

    return ten_ccf

def make_tensor(traj, save=True):
    '''
    Compute the cross-correlation tensor, and pickle
    '''

    tm = Timer("Computing CCF").start()
    
    ten_ccf = glv.comp.crosscorr_fft(traj.mat_x, target=args.target, lags=1000, progress=True)
    ten_ccf[np.isnan(ten_ccf)] = 0
    
    if save:
        filename = f"ccf_{args.tag}.npz"
        np.savez(joinpath(currentdir,filename), ten_ccf=ten_ccf)
        
    tm.stop()

    return ten_ccf


def maximum_correlation(ten_ccf, vec_t):
    '''
    Give the max magnitude cross-correlation and time value    
    '''
    S = ten_ccf.shape[1]
    mat_maxidx = np.argmax(ten_ccf, axis=0)
    mat_cmax = np.zeros((S,S))
    mat_tmax = np.zeros((S,S))
    for i in range(S):
        for j in range(S):
            mat_cmax[i,j] = ten_ccf[mat_maxidx[i,j],i,j]
            mat_tmax[i,j] = vec_t[mat_maxidx[i,j]]
    
    return mat_cmax, mat_tmax

def pcorr(mat_x):
    mat_p = glv.obs.relative_abundance(mat_x)
    S = mat_p.shape[1]
    mat_c = np.zeros((S,S))

    for i in range(S):
        for j in range(i,S):
            mat_c[i,j] = np.mean( mat_p[:,i] * mat_p[:,j])

    return mat_c

# Plotting

def plot_cmax(mat, traj, ax):

    threshold = 0.1
    mat_thresh = ( np.abs(mat) > threshold ) * np.sign(mat)

    from matplotlib.colors import ListedColormap
    cmap = 'viridis'#ListedColormap(["darkorange", "gold", "lawngreen", "lightseagreen"])
    
    vec_frac = glv.obs.DominantComponent.dominance_time(traj.mat_x)
    
    #ranks = np.argsort(vec_frac)[::-1]
    ranks = np.argsort(np.sum(traj.mat_alpha,axis=1))[::-1]
    

    mat_thresh = mat_thresh[ranks][:,ranks]

    
    #np.fill_diagonal(mat_thresh, 0)
    im = ax.imshow(mat_thresh, cmap=cmap)
    plt.colorbar(im, orientation="vertical")

def plot_tmax(mat, ax):    
    np.fill_diagonal(mat, 0)
    ax.imshow(mat)
    ax.set_aspect(1)

def plot_ct(mat_c, mat_t, ax):
    ax.scatter(mat_t.flatten(), mat_c.flatten(),s=0.1)

def plot_pair_trajectory(mat_c, traj, ax):
    '''
    Trajectory of two most-correlated species.
    '''
    mat_c = mat_c.copy()
    np.fill_diagonal(mat_c,0)

    i, j = np.unravel_index(np.argmax(mat_c), mat_c.shape)
    print(i, j)
    T = None
    ax.plot(traj.vec_t[:T], (traj.mat_x[:T,i]), alpha=0.6, color='dodgerblue')
    ax.plot(traj.vec_t[:T], (traj.mat_x[:T,j]), alpha=0.6, color='deeppink')

    print(mat_c[i,j])
    print(f"alpha_ij = {traj.mat_alpha[i,j]}")
    print(f"alpha_ji = {traj.mat_alpha[j,i]}")

def plot_dist(mat, ax):

    bins=np.arange(0,50,1)

    for th in [0.1,0.2,0.3]:
        vec_n = np.sum(mat_cmax > th, axis=1) - 1
        print(f"{th} : {np.amax(vec_n)}")
        ax.hist(vec_n, bins=bins, alpha=0.5)
        print(f"{np.mean(vec_n)} +- {np.std(vec_n)}")


def plot_corr_w(mat_c, ten_ccf, traj, ax):
    mat_z = (traj.mat_alpha - traj.mu) / traj.sig
    mat_w = - (mat_z +  mat_z.T) / 2

    ti = np.triu_indices(mat_w.shape[0], k=1)

    vec_c2 = ten_ccf[0,:,:][ti]

    vec_w = mat_w[ti]
    vec_c = mat_c[ti]

    ax.scatter(vec_w, vec_c2, s=0.2, lw=0, alpha=0.6)
    ax.set_facecolor("white")

def plot_ct(mat_c, mat_t, traj, ax):
    mat_z = (traj.mat_alpha - traj.mu) / traj.sig
    ti = np.triu_indices(mat_z.shape[0], k=1)

    vec_c2 = ten_ccf[0,:,:][ti]
    vec_c = mat_c[ti]
    vec_t =mat_t[ti]

    ax.scatter(vec_t, vec_c, s=0.2, lw=0, alpha=0.6)
    ax.set_facecolor("white")


def plot_corr_w2(mat_c, ten_ccf, traj, ax):
    mat_z = (traj.mat_alpha - traj.mu) / traj.sig
    mat_w = - (mat_z +  mat_z.T) / 2

    ti = np.triu_indices(mat_w.shape[0], k=1)

    vec_c2 = ten_ccf[0,:,:][ti]

    vec_w = mat_w[ti]
    vec_c = mat_c[ti]

    ax.scatter(vec_w, vec_c2, s=0.2, lw=0, alpha=0.6)
    ax.set_facecolor("white")


def plot_wc(traj, ax):
    mat_z = (traj.mat_alpha - traj.mu) / traj.sig
    mat_w = - (mat_z +  mat_z.T) / 2
    #mat_c = pcorr(traj.mat_x)
    #np.savez(joinpath(currentdir,"mat_c.npz"), mat_c=mat_c)
    mat_c = np.load(joinpath(currentdir,"mat_c.npz"))['mat_c']


    ti = np.triu_indices(mat_w.shape[0], k=1)

    vec_w = mat_w[ti]
    vec_c = mat_c[ti]

    #ax.scatter(vec_w, vec_c, s=0.2, lw=0, alpha=0.6)

    cmin = np.amin(vec_c)
    cmax= np.amax(vec_c)
    c_range = np.linspace(cmin,cmax,100)
    print("doing g")

    idx_wplus = np.argwhere(vec_w >= 0)

    vec_cplus = vec_c[idx_wplus]

    vec_g = np.zeros(c_range.size)
    for i,c in enumerate(c_range):
        n_cpos = np.count_nonzero( vec_cplus > c)
        n_ctot = np.count_nonzero( vec_c > c)
        if n_ctot > 0:
            vec_g[i] = n_cpos / n_ctot
        
    #vec_g = np.array([np.count_nonzero( vec_c[idx_wplus] > c ) / np.count_nonzero( vec_c > c) for c in c_range])

    ax.plot(c_range, vec_g)


def plot_shortcorr(traj, ax, T=1000):
    '''
    Correlations over a short time window
    '''
    ten_ccf = glv.comp.crosscorr_fft(traj.mat_x[:T,:], target=args.target, lags=10, progress=True)
    mat_c = ten_ccf[0,:,:]

    vec_frac = glv.obs.DominantComponent.dominance_time(traj.mat_x[:T,:])
    ranks = np.argsort(vec_frac)[::-1]
    mat_c = mat_c[ranks][:,ranks]

    im = ax.imshow(mat_c)
    plt.colorbar(im, orientation="vertical")


### MAIN SCRIPT #####################################################

# Load / make data


traj =  glv.obs.UnitGLVSeries.load(args.data)

if args.load:
    ten_ccf = load_tensor()
else:
    ten_ccf = make_tensor(traj)

mat_cmax, mat_tmax = maximum_correlation(ten_ccf, traj.vec_t)


# print( ( np.sum( (mat_cmax > 0.15) * (mat_tmax < 100) ) - 500 ) /2 )


# Plot

fig, ax = plt.subplots()
utl.adjust_fig_size(fig, 0.4, 2)

#plot_cmax(mat_cmax, traj, ax)
#plot_tmax(mat_tmax, ax)
#plot_ct(mat_cmax, mat_tmax, ax)
#plot_pair_trajectory(mat_cmax, traj, ax)
#plot_dist(mat_cmax,ax)
#plot_ct(mat_cmax, mat_tmax, traj, ax)
plot_wc(traj, ax)

#fig.savefig(joinpath(currentdir,f"pair_{args.tag}.png"), dpi=600, bbox_inches='tight', pad_inches=0, transparent=True)
#fig.savefig(joinpath(currentdir,f"mat_{args.tag}.png"), dpi=600, bbox_inches='tight', pad_inches=0, transparent=True)
#fig.savefig(joinpath(currentdir,f"dist_{args.tag}.png"), dpi=600, bbox_inches='tight', pad_inches=0, transparent=True)

fig.savefig(joinpath(currentdir,f"wbias_{args.tag}.png"), dpi=600, bbox_inches='tight', pad_inches=0, transparent=False)


gt.stop()