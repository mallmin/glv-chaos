# glv-chaos

This project concerns primarily the strong-interaction regime of disordered Lotka-Volterra model with immigration, with a focus on chaotic dynamics. 

## File structure

Note! Simulation code assumes execution from the glv-chaos

```
glv/                    # custom python package for glv dynamics - placed at root level for easy import

simcode/                # code to generate simulation data in data/

data/                   # 'raw' simulation data, and some summary statistics of these, upon which figures are based

figurecode/             # source for creating and aligning figures 

trialcode/              # various "experiments" on glv dynamics & analysis - not part of paper

manuscript/             # source files for typsetting manuscript; contains copies of all necessary figures  

notes/                  # various research notes etc.

presentations/        # data & figures produced for posters/presentations      

```
