''' Common code snippets for copy/paste'''

### IMPORTS ##########################

import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import numpy as np
import matplotlib.pyplot as plt
from timer import Timer

import os, sys
sys.path.append('./glv-chaos/')
import glv
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timer import Timer

### ARGPARSE #########################
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--load", action = "store_true")
args = parser.parse_args()

### PLOTTING ########################

ax, fig = plt.subplots()

plt.rc('font', size=16) 

fig.set_size_inches(8, 8)

ax.set_aspect('equal', adjustable='box')

# Colorbar size when axes are equal

from mpl_toolkits.axes_grid1 import make_axes_locatable

def colorbar(mappable):
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    return fig.colorbar(mappable, cax=cax)


### RNG #############################

from numpy.random import default_rng


### FILE HANDLING ###################

def output_path(output_dir_or_file, filename_default):
    '''Takes a path and returns it if it is the name of a file in an existing directory, 
    or returns appends a default filename if the path is an existsing directory'''

    odof = output_dir_or_file
    if os.path.isdir(odof):
        # existant directory; append default file name
        output_file_path = os.path.join(odof, filename_default)
    else:
        # not an existant directory        
        [dir, file] = args.output.rsplit('/', 1)
        if os.path.isdir(dir):
            # is path to a (possibly nonexistsnet) file in an existing directory 
            output_file_path = os.path.join(dir, file)
        else:            
            raise OSError(f"Output directory '{dir}' not found")
    
    return output_file_path
