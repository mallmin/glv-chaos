\documentclass[12pt]{article}

\usepackage{amsmath,amssymb}
\usepackage{enumerate}
\usepackage{titlesec}
\usepackage[margin=28mm, top=20mm]{geometry}
\usepackage[colorlinks,linkcolor=blue,citecolor=blue]{hyperref}
\usepackage{cleveref}% Load AFTER hyperref!
\usepackage{appendix}
\usepackage{mathrsfs}  
\usepackage{graphicx}
\usepackage{placeins}
\usepackage{subfig}
\usepackage[labelfont=bf]{caption}
\captionsetup[figure]{format=hang}


%\llangle, \rrangle
\makeatletter
\newsavebox{\@brx}
\newcommand{\llangle}[1][]{\savebox{\@brx}{\(\m@th{#1\langle}\)}%
	\mathopen{\copy\@brx\kern-0.5\wd\@brx\usebox{\@brx}}}
\newcommand{\rrangle}[1][]{\savebox{\@brx}{\(\m@th{#1\rangle}\)}%
	\mathclose{\copy\@brx\kern-0.5\wd\@brx\usebox{\@brx}}}
\makeatother

\renewcommand{\vec}[1]{\boldsymbol{#1}}
\newcommand{\vx}{\vec{x}}
\newcommand{\vJ}{\vec{J}}
\newcommand{\del}{\partial}
\newcommand{\ddel}[2]{\frac{\del#1}{\del#2}}
\newcommand{\avg}[1]{\langle #1 \rangle}
\newcommand{\aavg}[1]{\llangle #1 \rrangle}
\newcommand{\Var}[1]{\text{Var}[#1]}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\deriv}[1]{\frac{\diff{}}{\diff{#1}}}
\newcommand{\dd}[2]{\frac{\diff{#1}}{\diff{#2}}}
\newcommand{\std}[1]{\text{std}(#1)}

\newcommand{\E}[1]{\text{E}\left[#1\right]}

\newcommand{\red}[1]{{\color{red}#1}}

%\titleformat*{\section}{\Large\bfseries\sffamily}
%\titleformat*{\subsection}{\large\bfseries\sffamily}
%\titleformat*{\subsubsection}{\large\sffamily}

\titleformat*{\section}{\large\centering\bfseries\sffamily}
\titleformat*{\subsection}{\centering\bfseries\sffamily}
\titleformat*{\subsubsection}{\centering\sffamily}


\title{\bfseries\sffamily Notes on dLV with strong interactions and small immigration}
\author{\it Emil Mallmin}



\begin{document}
	\maketitle
	
	\begin{center}
		\begin{minipage}{0.8\textwidth}	
			These are notes to summarize the progress on this research project. The graphs are preliminary renderings made at different times in the recent past, and therefore somewhat inconsistent in quality and style.
			
		\end{minipage}
	\end{center}
	
	
\section*{Project goals}

	\begin{itemize}
		\item Understand qualitatively the behaviour of dLV with strong interactions and small immigration that prevents extinction (mostly achieved by Matthieu Baron (MB))
		\item Produce an empirical phase diagram of the strong-interaction regime --- understand how it connects to the phase diagram of the weak interaction-regime
		\item Find a focal-species model for the strong-interaction chaotic phase --- how related to DMFT?
		\item Connect results with the ecological literature
	\end{itemize}		
	
	

\section*{Model \& notation}

Disordered Lotka-Volterra with immigration:	
\begin{equation}\label{eq:gLV}
	\dot{x}_i(t) = r_i x_i(t) \left( 1 - x_i(t) - \sum_{j(\neq i)} \alpha_{ij} x_j(t)   \right) + \lambda,\quad i=1,\ldots,S,
\end{equation}
Disorder parameters
\begin{equation}
	\mathbb{E}[A_{ij}] = \mu,\quad \text{Var}[A_{ij}] = \sigma^2, \quad \text{Corr}(A_{ij},A_{ji}) = \gamma.
\end{equation}
The \textit{weak-interaction regime} is defined by letting 
\begin{equation}
	\mu = \tilde{\mu} /S,\quad \sigma^2 = \tilde{\sigma}^2 / S,
\end{equation}
with $\tilde{\mu},\tilde{\sigma} = O(1)$ as $S\to\infty$, whereas in the \textit{strong-interaction regime} $\mu,\sigma=O(1)$.

Will limit ourselves to $r_i = 1$ for all $i$ and $\gamma = 0$. In weak regime the higher moments of the distribution of $\alpha_{ij}$ don't matter, but in the strong regime they should in principle matter quantitatively (although hopefully not qualitatively)---we will assume Gaussian distribution.


\section*{Phases}

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{pd}
	\caption{Phase diagram, $S=500, \lambda=10^{-8},\gamma=0$}
	\label{fig:pd}
\end{figure}

We draw one random Gaussian matrix $Z_0$ with mean 0 and variance $1$. We then rescale it to $\alpha(\mu,\sigma) = \mu + \sigma Z_0$ for each point in the $(\mu,\sigma)$--plane. For each phase-point we run one ``long'' simulation ($T\sim 50'000$ time units with 100 discrete steps of $dt=0.01$ per time unit) from a random initial condition, and test whether the trajectory was divergent, found a fixed-point or limit cycle, or kept fluctuating (which turns out always to be chaos, although ultimately it may be transient): \autoref{fig:pd}. A number of other scalar metrics are also saved for each simulation. Each plot is $\sim 10'000$ simulations and must be run on a cluster. 

Some things to note:
\begin{itemize}
	\item The ``multiple attractor phase'' in the weak regime is basically turned into a strictly chaotic phase when there is immigration
	\item The strong regime has a relatively clear-cut chaotic phase!
	\item There is a multiple attractor phase in the strong regime, that may have some discernable internal structure
	\item  The fixed-point phase past $\mu=1$ is competitive exclusion multistability (as MB showed)
\end{itemize}

\vspace{0.5cm}
Things to do:
\begin{itemize}
	\item Run for $\mu$ up to $1.5$ to better see phase-lines
	\item Zoomed-in diagram around symmetry point, $(\mu,\sigma)=(1,0)$
\end{itemize}

\section*{Description of chaotic phase}

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{traj}
	\caption{Difference in trajectories of relative abundance between chaos in the weak vs.\ strong regimes. Species are ordered by there average rank over the time interval. Parameters: $S=500, \lambda=10^{-8},\gamma=0$, $\mu=0.5,\sigma=0.3$ (strong), $\tilde\mu=5.8, \tilde\sigma=2.4$ (weak).}
	\label{fig:traj}
\end{figure}

There is an important qualitative difference between chaos in the weak and strong regimes (\autoref{fig:traj}): in the weak regime, the chaotic phase seems to involve fluctuations in abundances around a fixed point (or maybe cluster of similar fixed points), so that some species are generally more abundant than others. In the strong regime, species instead seem to be statistically identical (to within some tolerance) if viewed over long enough times. 

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{chaos}
	\caption{A closer look at the behaviour in the chaotic phase ($\mu=0.5,\sigma=0.3,S=500,\lambda=10^{-8}$). (A) Stacked abundances show that the community is at any time dominated by a handful of species, but which species are dominant changes on some characteristic timescale. (B) The distribution of abundances is approximately a truncated power-law truncated at $\lambda$ and $1$. Thin grey lines show that each species' distribution of abundances over time (abundnace fluctuation distribution, AFD) is similar. Histogram of distribution of the abundances of all species at one random point in time (snapshot species abundance distribution, SAD) is the same as the average AFD---a sort of ``ergodicity'' property. (C) The time-series of a single-species over long time on a linear abundance scales looks like on-off intermittency.}
	\label{fig:chaos}
\end{figure}



In the strong regime, each species appears to undergo on-off intermittency such that the community is at any time dominated by only a few species (\autoref{fig:chaos}). Results as MB's. %However, from the relatively small fluctuations in the total abundance, one can intuit that an effective description of the system as $S$ independent stochastic processes (DMFT) would not work, since the fluctuations in total abundance of summed intermittent process be larger. Therefore correlations are expected to be relevant, whereas in the weak regime they vanish with large $S$.

The power-law exponent of the AFD in the chaotic phase is displayed in \autoref{fig:nuglv} together with a map of the maximal Lyapunov exponent (MLE). Generally, the MLE is small but non-zero outside the fixed-point simulations, but diverges close the the divergent phase. For persistently chaotic simulation points the AFD power-law exponent $-\nu$ is between $-1$ and $-1.45$, with these extremes corresponding to the weak regime, and close to the symmetry point $(\mu,\sigma)=(1,0)$. 

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{mle-nu}
	\caption{Left: Maximal Lyapunov exponent $\Lambda_0$ for all non-divergent simulations. Dark grey means $\Lambda_0 <0$, and light grey $\Lambda_0 > 0.1$. Right: Variation of AFD power-law exponent $\nu$ in the chaotic phase ($S=500,\lambda=10^{-8}$) [Note! When this fig was produced I had defined $\nu$ as $-\nu$; and the two figs are not the exact same dataset, but same parameters]}
	\label{fig:nuglv}
\end{figure}

In \autoref{fig:scaling} we see how key metrics depend on the number of species. Intermittent behaviour with $O(1)$ dominant species holds for the range of  $S$ studied, but with larger $S$ each species has longer times of rarity between its periods of being dominant.


\begin{figure}
	\centering
	\includegraphics[scale=0.5]{scaling}
	\caption{Scaling of some key metrics with $S$ for $\mu=0.5,\sigma=0.3,\lambda=10^{-8}$. (A) The AFD exponent varies a little with $S$ (as MB showed). (B) The cumulative relative abundance of all species up to rank $r$. Shows that dominant set remains $O(1)$ even with $S$ very large. To understand $S\to\infty$ one should decrease $\lambda$ with $S$. Remains to check it then an $S$-independent distribution is found for lower ranks then. (C) Distribution of durations that a species remains $<0.01$ absolute abundance. Approximately exponential, so the coarse-grained ``on-off'' dynamics is basically a two-state Markov jump process. (D) A measure of species variability, basically the average KS-distance of a species AFD up to time $t$ with the species-average AFD up to time $t$. Shows that species differences decrease with time until it plateaus at a level that decreases with $S$.   }
	\label{fig:scaling}
\end{figure}

\vspace{0.5cm}
Things to do:
\begin{itemize}
	\item Produce insets for \autoref{fig:scaling} showing how slopes (A,B), number of top-ranked species that together make $99\%$ (C), and sv($\infty$) (D), depend on $S$.
\end{itemize}


\section*{Focal-species model}

\subsection*{Definition and solution}
The following stochastic logistic growth model produces on-off intermittency, that we shall try to match with the dLV:
\begin{equation}\label{eq:red}
	\dot{x}(t) =  x(t)(g(t) - x(t)) + \lambda,
\end{equation}
where the `growth factor' $g(t)$ is a coloured Gaussian noise (or Ornstein-Uhlenbeck process),
\begin{equation}
	\avg{g(t)} = - k, \quad \avg{g(t)g(t')} - \avg{g}^2 = u^2 e^{-|t-t'|/\tau}.
\end{equation}

The steady-state distribution can be solved approximately using the unified coloured noise approximation:
\begin{equation}\label{eq:x-tilde-P*}
	P^*(x) = \frac{1}{\mathscr{N}} e^{- [q_+(\lambda/x) + q_-(x)] } x^{-\nu}  \left( \tau^{-1} + x +  \frac{\lambda}{x}\right),
\end{equation}
expressed using the quadratic functions
\begin{equation}
	q_\pm(y) = \frac{\left(  y + (\tau^{-1} \mp k) \right)^2}{2 u^2} 
\end{equation}
and the exponent
\begin{equation}\label{eq:nu-formula}
	\nu = 1 + \frac{k}{\tau u^2}.
\end{equation}
By the right choice of $k,u,\tau$ (and $\lambda$ the same as dLV) we hope to match the focal-species dynamics reasonably well.


\subsection*{Matching the dLV}
To motivate the focal-species model consider in the dLV (strong chaotic phase)
\begin{equation}
	g_i(t) := 1 - \sum_i \alpha_{ij} x_j(t). 
\end{equation}
With $\alpha_{ij} \sim \mathcal{N}(\mu,\sigma^2)$, and supposing we can treat each $\alpha_{ij}$ and $x_j(t)$ as essentially uncorrelated, we would expect
\begin{equation}
g_i(t) \sim \mathcal{N}\left(1 - \mu \sum_{j(\neq i)} x_j(t),\ \sigma^2 \sum_{j(\neq i)} x_j^2(t) \right),
\end{equation}
based on the properties of linear sums of Gaussian variables. This observation motivates the introduction of
\begin{equation}
a(t) := 1 - \mu \sum_i x_i(t),\quad
b(t) := \sigma \sqrt{\sum_i x_i^2(t)} 
\end{equation}
and 
\begin{equation}
\eta_i(t) := \frac{g_i(t) - a(t)}{b(t)}.
\end{equation}
We expect $\eta_i(t)$ to have a unit-Gaussian stationary distribution. This is verified by simulation \autoref{fig:eta-normal}. We then look into its auto-correlation $\phi_{ii}(t)$ and cross-correlations $\phi_{ij}(t)$ (assuming $\eta$ is a stationary process; computed using Fourier transforms). The results in \autoref{fig:eta-cf} shows that the autocorrelation function is approximately identical across species and can be approximated as a decaying exponential. At any fixed time lag, the distribution of cross-correlation coefficients across all species pairs is a Gaussian, so that only a small subset of species pairs have significantly (anti)correlated noise; and these decrease with time-lag on average.

\begin{figure}
	\centering
	\subfloat[Histogram of $\eta_i(t)$ over all simulation time-points $t$, shown for nine different species $i$.]{\includegraphics[scale=0.45]{graph_timeavg_eta}}\quad
	\subfloat[Histogram of $\eta_i(t_k)$ over all species $i$ for for nine different time points  $t_k$ evenly spaced across the entire simulation.]{\includegraphics[scale=0.45]{graph_speciesavg_eta}}\\
	\subfloat[Histogram of $\eta_i(t)$ over all species $i$ and times $t$.]{\includegraphics[scale=0.6]{graph_avg_eta}}
	\caption{In each graph, a histogram of values for $\eta_i(t)$ is plotted and compared with the unit normal distribution represented by the thick line. For $\mu=0.5,\sigma=0.3,S=500,\lambda=10^{-8}$}\label{fig:eta-normal}	
\end{figure}

\begin{figure}
	\centering
	\subfloat[ \textit{Upper panel:} ACF $\phi_{ii}(t)$ plotted in grey for all species, with a few also in colour. \textit{Lower panel:} ACF averaged over all species and compared with $\exp{[-t/\tau_{\text{fit}}]}$ with $\tau_{\text{fit}}$ resulting from least-squares minimization.\label{fig:eta-acf}]{%
		\includegraphics[scale=0.45]{graph_eta_acf}%
	}%
	\quad%
	\subfloat[For the different fixed time lags $t$ indicated, we plot the distribution of the cross-correlation function (CCF) $\phi_{ij}(t)$ across all pair of species $i > j$ (coloured graphs) and the best-fit Gaussian (balck, dashed). \textit{Inset:} empirical time-evolution of the standard deviation. \label{fig:eta-ccf}]{%
		\includegraphics[scale=0.45]{graph_eta_crosscorr.png}%
	}%
	\caption{Descriptions of the autocorrelation function (ACF) $\phi_{ii}(t)$ and the cross-correlation function $\phi_{ij}(t)$ ($i >j $) for system parameters $(S, \mu, \sigma, \lambda) = (500, 0.5, 0.3, 10^{-8})$ with simulation time step $dt = 0.01$ for a total time $T=500'000$ (i.e. $5\times10^7$ time steps).}\label{fig:eta-cf}
\end{figure}

In conclusion, it seems reasonable to model $\eta_i(t)$, for any given $i$, as a unit-Gaussian process with exponential autocorrelation decay. Assuming Markovity for simplicity, the only choice is then that $\eta(t)$ is the OUP
\begin{equation}\label{eq:OUP}
	\frac{\diff{}}{\diff{t}}{\eta}(t) = - \frac{1}{\tau} {\eta}(t) + \sqrt{\frac{2}{\tau}} \xi (t),\quad \xi \   \text{standard white noise}.
\end{equation}
If we also assume $a(t)$ and $b(t)$ have relatively moderate fluctuations around their time-averages, it seems reasonable to model $g_i$ as
\begin{equation}
	g(t) = \overline{a} + \overline{b}\eta(t).
\end{equation}
Then we get the model \eqref{eq:red} with
\begin{gather}
	k = - \overline{a},\\
	u = \overline{b}
\end{gather}
and $\tau$ from the exponential fit performed as in \eqref{fig:eta-acf}.

It would be nice of course if $\overline{a}$, $\overline{b}$, $\tau$---which we will call the \textit{compound parameters}--- could be predicted without having to extract them from a full simulation of dLV, similar to the self-consistency in the DMFT approach valid for the weak regime. I will argue why self-consistent DMFT cannot be extrapolated from the weak chaos to the strong chaos case, but that there still is some simplifying relation between the compound parameters that may be useful.

\subsection*{Performance of the focal-species model}
As shown in \autoref{fig:comp}, the reduced model \eqref{eq:red} does capture the statistics of focal species in the strong-chaos dLV, given that  $-k, u, \tau$ are taken as the ``true'' values $\overline{a}$, $\overline{b}$, $\tau$.

There is a systematic deviation for high and low abundances, likely due to the fact that in order to reach these extreme values we need $a(t)$, $b(t)$ and $\eta_i(t)$ to fluctuate ``in the same direction'', but in the reduced model we have ignored fluctuations in $a,b$.   



\begin{figure}
	\centering
	\includegraphics[scale=0.5]{comp}
	\caption{Comparison of exact dLV to reduced focal-species model with ``true'' parameters $\overline{a},\overline{b},\tau$. (A,C) show particular simulation at $S=500, \mu={0.5},\sigma=0.3,\lambda=10^{-8}$. (B) shows how well the reduced model captures the AFD power-law exponent across the range of $(\mu,\sigma)$ that produced persistent chaos. [Note! When C was produced I had defined $\nu$ as $-\nu$]}
	\label{fig:comp}
\end{figure}


\vspace{0.5cm}
Things to do:
\begin{itemize}
	\item Also include how well \eqref{eq:nu-formula} predicts true $\nu$ --- some older plots I have showed that the UCNA is not quantitatively that accurate for the exponent in this model.
\end{itemize}

\subsection*{Analysis of compound parameters}

Firstly, we can check how the compound parameters vary over the $\mu,\sigma$ plane: \autoref{fig:at}. It appears $\overline{a} < 0$ generally (which we anticipated by defining the parameter $k>0$ in the reduced model). The decorrelation time $\tau$ grows very long as we approach the phase line to the unique-fixed-point phase---this makes sense given the DMFT autocorrelation function converges to a positive constant in the weak regime. Interestingly, there the compound parameters seem related by simple laws: \autoref{fig:taylorlike}. Approximately, $\overline{b} \propto - \overline{a}$ and $\tau \propto - 1/ \overline{a}$, with the proportionality constant probably depending on $\lambda, S$. It remains to be tested whether these laws become sharper with large $S$. While I'm not sure how these simple laws are generated, from \eqref{eq:nu-formula} one might expect some relation given that $\nu$ ought to be constrained to some range of $O(1)$. 

\begin{figure}
	\centering
	\includegraphics[scale=0.4]{at}
	\caption{Variation of $\overline{a}$ and $\tau$ over phase plane. (Left has $\overline{a} < -1$ in grey; Right has $\tau>200$ in grey.)}
	\label{fig:at}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{taylorlike}
	\caption{``Taylor-like'' laws between compound parameters. Each point represent one result for $(\overline{a},\overline{b},\tau)$ for a given $(\mu,\sigma)$. Most of the variation seems to be due to $\sigma$, with $\mu$ contributing more to spread around the trend.}
	\label{fig:taylorlike}
\end{figure}

The fact that $k = -\overline{a} > 0$ is crucial for the reduced model to generate intermittency. (And so is the fact that the noise is colored rather than white.) We find below that $\overline{a}<0$ in the strong chaotic regime is only possible due to long-lived correlation between species:

An approximate formula for the time-averaged total abundance $\overline {X}$ is\footnote{Derivable by direct integration of $\deriv{t} \log X(t)$ from $0\to T$, divide by $T$ and let $T\to\infty$. Then assume $X$ practically independent from $D$ and $\rho$.}
\begin{equation}\label{eq:Xbar}
	\overline{X} \approx \frac{1}{\mu + (1-\mu)\overline{D} - \sigma \rho}.
\end{equation}
Here $D(t) = \sum_i p_i^2(t)$, with $p_i = x_i / X$ relative abundance, is the Simpson diversity index. We define 
\begin{equation}
	\rho = -\sum_{ij} z_{ij} \overline{p_i p_j},
\end{equation}
with $\alpha_{ij} = \mu + \sigma z_{ij}$, as an `interaction-weighted' species-correlation. If $i,j$ are positively correlated and interact beneficially on average, $(z_{ij}+z_{ji})/2 < 0$, they make a positive contribution to $\rho$. Same if they are anti-correlated and interact deleteriously. If correlations are small or ``spurious'' the disorder average should make $\rho = 0$.

We find that $\overline{a} = 1 - \mu \overline{X} < 0$ iff
\begin{equation}
	\rho > \frac{1 - \mu}{\sigma} \overline{D}.
\end{equation}
Since $\overline{D} \gtrsim 0.1$ in the strong chaotic phase because of dominance of a few, and $\mu < 1$, $\rho$ must be significantly positive, so correlations are important! This statement does not depend on $S$ as long as there is dominance of a few, which we found earlier extends to high $S$.



\vspace{0.5cm}
Things to do:
\begin{itemize}
	\item See if we can extract $\tau$ as the decorrelation time of $\vec{x}(t)$ (abundance vector) instead of $\eta_i(t)$.
\end{itemize}

\subsection*{Comparison with DMFT}

The focal-species equation from DMFT for $\gamma=0$ is
\begin{equation}\label{eq:eff-weak}
	\dot{x}(t) = x(t)(1 - x - \tilde{\mu} m(t) + \tilde{\sigma} \zeta(t) ) + \lambda	
\end{equation}
where 
\begin{equation}
	\avg{\zeta(t)} = 0,\quad \avg{\zeta(t)\zeta(t')} = C(t,t').
\end{equation}
and  $m, C$ are deterministic functions chosen self-consistently as
\begin{equation}
	m(t) = \avg{x(t)},\qquad \avg{x(t)x(t')} = C(t,t').
\end{equation}
Supposing we are in the chaotic part of the weak regime, we treat $x(t)$ as a stationary process, and $m$ will be constant and $C(t,t')=C(|t-t'|)$.

What happens if we let $\tilde{\mu} = S \mu$, etc., to just extrapolate into the strong regime? The issue is that $S\avg{x}$ in the term $\tilde{\mu}m = \mu S \avg{x}$ comes from replacing $X(t) = \sum_i x_i(t)$ with its time-average (might be ok), and replacing that time-average with the expected time-average of a sum of $S$ independent processes, $S\avg{x}$. This works in the weak regime when $S\to\infty$ since then both fluctuations of the average and correlations between species vanish. In contrast, as we have seen, independence in the limit is not true for strong-regime chaos, so the replacement is not valid.  

\vspace{0.5cm}
Things to do:
\begin{itemize}
	\item Run DMFT extrapolated into the strong regime, to get shape of $C(\tau)$ (particularly behaviour as $\tau\to\infty$). A preliminary test, assuming for simplicity $\tilde{\sigma}\zeta \to \sigma\sqrt{S \avg{x^2}}\eta$, did not converge (and stopped being intermittent) when started from the ``true'' values from dLV simulation.
\end{itemize}

\section*{Final remarks}

\begin{itemize}
	\item We understand the behaviour of the strong chaotic phase quite well at this point.
	\item The intermittent model is simple enough to be compared with data, which can be the basis of a future project
	\item Could one derive the laws connecting the compound parameters?
	\item In the end I will re-generate all production graphs with simulations not assuming $\gamma=0, r_i = 1, \alpha_{ij}\sim$ Gaussian, to see if there are significant differences. The theoretical results may be difficult to generalize, though. 
	\item Interestingly, the plankton SAD exponent is empirically $\approx 1.5$, which in comparison with chaotic dLV would put it close to the neutrality-point $(\mu,\sigma)=(1,0)$, but on the chaotic side. Maybe there is some significance to this, as it represents the ``edge of chaos'' as well as proximity to competitive exclusion, which one might expect if plankton are competing for very similar niches. 
\end{itemize}


%\bibliographystyle{../../../mybibstyle}	
%\bibliography{../../../MasterRef}
	
	
	
	
	
\end{document}



