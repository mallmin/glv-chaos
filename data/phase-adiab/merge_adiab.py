
import os, sys
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(".")
import argparse
import re
from myutils import Timer
import pandas as pd


parser = argparse.ArgumentParser()
parser.add_argument("-o", "--output-dir", type = str, default="data/phase-adiab", dest="output_dir")
parser.add_argument("-e", "--err", action="store_true", dest="err")
parser.add_argument("-l", "--log", action="store_true", dest="log")
parser.add_argument("-d", "--dat", action="store_true", dest="dat")
parser.add_argument("-r", "--erase", action="store_true", dest="erase")
args = parser.parse_args()

# Check compatible folder structure

output_dir = args.output_dir
if not os.path.isdir(output_dir):
    raise OSError(f"Output directory '{output_dir}' does not exist.")

data_dir = joinpath(output_dir, "csv")
err_dir = joinpath(output_dir, "err")
log_dir = joinpath(output_dir, "log")


# Merge data files

if args.dat:

    tm = Timer("Merge csv data").start()
    
    data_file_pattern = re.compile("dat_\d+\.csv")
    #merge_file = open(joinpath(output_dir,"data.csv"), "w")
    
    dfs = []

    for filename in os.listdir(data_dir):
        file_path = joinpath(data_dir, filename)
    
        if os.path.isfile(file_path) and data_file_pattern.match(filename):
            
            df = pd.read_csv(file_path)
            dfs.append(df)

            if args.erase:
                os.remove(file_path)
    
    df_merged = pd.concat(dfs)
    df_merged.to_csv(joinpath(output_dir,"data.csv"))

    #merge_file.close()

    tm.stop()

# Merge error files

if args.err:

    tm = Timer("Merge err").start()

    merge_file = open(joinpath(output_dir,"err.txt"), 'w')

    nonempty_err_ids = []
    for filename in os.listdir(err_dir):
        file_path = os.path.join(err_dir, filename)
        if os.path.getsize(file_path) > 0:
            nonempty_err_ids.append(int(filename.split('.')[0]))

    nonempty_err_ids.sort()

    merge_file.write(f"Error statistics: {len(nonempty_err_ids)}/{len(os.listdir(err_dir))}\n")
    merge_file.write(f"Ids with errors: {','.join([str(i) for i in nonempty_err_ids])}\n\n")

    for id in nonempty_err_ids:
        file_path = joinpath(err_dir, f"{id}.err")
        merge_file.write(f"{id}\n")
        with open(file_path,"r") as file:
            for line in file:
                merge_file.write(line)
        merge_file.write("\n")

    merge_file.close()
   
    if args.erase:
        for filename in os.listdir(err_dir):
            file_path = os.path.join(err_dir, filename)
            os.remove(file_path)

    tm.stop()