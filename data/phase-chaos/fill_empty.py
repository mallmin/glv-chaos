'''
Merges csv, err, log data for simulations in chaotic phase
'''

import os, sys
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(".")
import argparse
import re
from myutils import *
import pandas as pd
import numpy as np


param_file = "./simcode/phase-chaos/params_chaos.txt"


err = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,39,40,42,43,44,45,46,47,48,60,61,62,64,67,109,287,472,1480,1626,2199,2223,2247,2306,2318,2320,2393,2516,2534,2576,2583,2587,2614,2649,2675,2706,2750,2766,2777,2832,2842]

parser = argparse.ArgumentParser()
parser.add_argument("-S", type = int, default = 500)
parser.add_argument("--mu", type = np.float64,  default = 0.5)
parser.add_argument("--sig", type = np.float64,  default = 0.3)
parser.add_argument("--lam", type = np.float64, default = 10**-8)
parser.add_argument("--gam", type = np.float64,  default = 0.0)
parser.add_argument("--alpha-seed", type=int, default=None)
parser.add_argument("--init-seed", type=int, default=None)
parser.add_argument("--dt", type = np.float64, default = 0.01)
parser.add_argument("--scheme", type=str, default="default")
parser.add_argument("--steps-transient", type = int, default = 100*10000) 
parser.add_argument("--steps-main", type = int, default = 100*100000) # for data 
parser.add_argument("--steps-lim-test", type = int, default = 100*1000) # enough to ascertain fixed-point 
parser.add_argument("--steps-focal", type = int, default = 100*100000) #enough to ascertain fixed-point 
parser.add_argument("--trials", type = int, default = 5) 
parser.add_argument("--skip", action="store_true")
parser.add_argument("--profile", action = "store_true")
parser.add_argument("--plot", action = "store_true")
parser.add_argument("--output-dir", type=str, default="./data/phase-chaos")
parser.add_argument("--sim-id", type = int, default=None)
parser.add_argument("--job-id", type = int, default=None)


with open(param_file) as f:
    for i, line in enumerate(f):
        args = parser.parse_args(line[:-1].split(" "))
        if (i in err) or (args.skip):
            d = open(joinpath("./data/phase-chaos/csv",f"dat_{i}.csv"), 'w')
            d.write("mu,sig,chaos,k,u,tau_n,tau_x,X,Neff,rho,nu,nu_eff\n") #mu,sig,chaos,k,u,tau_n,tau_x,X,Neff,rho,nu,nu_eff
            d.write(f"{args.mu},{args.sig},0,,,,,,,,,")
            d.close()
        