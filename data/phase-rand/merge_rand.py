
import os, sys
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(".")
import argparse
import re
from myutils import Timer
import pandas as pd


parser = argparse.ArgumentParser()
parser.add_argument("-o", "--output-dir", type = str, default="data/phase-rand", dest="output_dir")
parser.add_argument("-e", "--err", action="store_true", dest="err")
parser.add_argument("-l", "--log", action="store_true", dest="log")
parser.add_argument("-c", "--csv", action="store_true", dest="csv")
parser.add_argument("-d", "--diag", action="store_true", dest="d")
parser.add_argument("-r", "--erase", action="store_true", dest="erase")
args = parser.parse_args()

gt = Timer("Script").start()

# Check compatible folder structure

output_dir = args.output_dir
if not os.path.isdir(output_dir):
    raise OSError(f"Output directory '{output_dir}' does not exist.")

data_dir = joinpath(output_dir, "csv")
err_dir = joinpath(output_dir, "err")
log_dir = joinpath(output_dir, "log")


# Merge data files

if args.d: #diagnose
    data_file_pattern = re.compile("dat_\d+\.csv")
    #merge_file = open(joinpath(output_dir,"data.csv"), "w")
    
    for filename in os.listdir(data_dir):
        file_path = joinpath(data_dir, filename)
    
        l_empty = []
        l_short = []

        if os.path.isfile(file_path) and data_file_pattern.match(filename):
            
            i =  int(filename.split("_")[1].split(".")[0])

            if i != 6415:
                df = pd.read_csv(file_path)
                N = df["N"].to_numpy()
                n_fp = df["n_fp"].to_numpy()
                n_chaos = df["n_chaos"].to_numpy()
                n_cycle = df["n_cycle"].to_numpy()
                n_div = df["n_div"].to_numpy()
                if N != n_fp + n_chaos + n_cycle + n_div:
                    print(i)


            # if i in [33,198,224,235,289,491,577,629,687,1005,1240,1345,1525,1526,1527,1528,1529,1530,1531,1532,1533,1534,1541,1542,1543,1544,1545,1546,1547,1548,1858,1944,1951,2131,2148,2227,2556,2598,2897,2906,2963,2964,2968,3041,3128,3312,3509,3855,4284,4286,4712,4727,4732,4764,4765,4808,4906,4934,5018,5049,5131,5203,5242,5698,5811,6006,6121,6274,6406,6407,6408,6409,6412,6413,6415,6466,6476,6729,6926,6942,7064,7086,7093,7239,7651,8255,8427,8432,8932,8936,8944,8953,8959,8995,9798,9891,9948,11147,12376]:
            #     print(i)
            #     with open(file_path,"r") as file:
            #         for line in file:
            #             print(line,end="")
            #         print()
                
                # if os.path.getsize(file_path) == 0:
                #     print(f"{i}: empty")
                # else:
                #     df = pd.read_csv(file_path)
                #     print(f"{i}: {df.iloc[0,:]}")

            # if os.path.getsize(file_path) > 0:
            #     df = pd.read_csv(file_path)
            #     print(df['N'].to_numpy()[0])
            #     if df['N'].to_numpy()[0] < 10:
            #         l_short.append(i)
            # else:
            #     l_empty.append(i)
    print(f"Empty: {l_empty}")
    print(f"Short: {l_short}")

if args.csv:

    tm = Timer("Merge csv data").start()
    
    data_file_pattern = re.compile("dat_\d+\.csv")
    #merge_file = open(joinpath(output_dir,"data.csv"), "w")
    
    dfs = []

    for filename in os.listdir(data_dir):
        file_path = joinpath(data_dir, filename)
    
        if os.path.isfile(file_path) and data_file_pattern.match(filename):
            
            df = pd.read_csv(file_path)
            dfs.append(df)

            if args.erase:
                os.remove(file_path)
    
    df_merged = pd.concat(dfs)
    df_merged.to_csv(joinpath(output_dir,"data.csv"))

    #merge_file.close()

    tm.stop()

# Merge error files

if args.err:

    tm = Timer("Merge err").start()

    merge_file = open(joinpath(output_dir,"err.txt"), 'w')

    nonempty_err_ids = []
    for filename in os.listdir(err_dir):
        file_path = os.path.join(err_dir, filename)
        if os.path.getsize(file_path) > 0:
            nonempty_err_ids.append(int(filename.split('.')[0]))

    nonempty_err_ids.sort()

    merge_file.write(f"Error statistics: {len(nonempty_err_ids)}/{len(os.listdir(err_dir))}\n")
    merge_file.write(f"Ids with errors: {','.join([str(i) for i in nonempty_err_ids])}\n\n")

    for id in nonempty_err_ids:
        file_path = joinpath(err_dir, f"{id}.err")
        merge_file.write(f"{id}\n")
        with open(file_path,"r") as file:
            for line in file:
                merge_file.write(line)
        merge_file.write("\n")

    merge_file.close()
   
    if args.erase:
        for filename in os.listdir(err_dir):
            file_path = os.path.join(err_dir, filename)
            os.remove(file_path)

    tm.stop()

gt.stop()