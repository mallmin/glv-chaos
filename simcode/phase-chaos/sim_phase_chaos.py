'''
Script to gather statistics for the chaotic phase
'''

# Paths
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(".")
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

import glv
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import default_rng
import argparse
from myutils import Timer
import pandas as pd
import warnings


def parse_args(arg_list):
    '''
    Parses the command line arguments.

    Returns
    -------
    args
        object containing arguments
    '''
    parser = argparse.ArgumentParser()

    # Model parameters
    parser.add_argument("-S", type = int, default = 500)
    parser.add_argument("--mu", type = np.float64,  default = 0.5)
    parser.add_argument("--sig", type = np.float64,  default = 0.3)
    parser.add_argument("--lam", type = np.float64, default = 10**-8)
    parser.add_argument("--gam", type = np.float64,  default = 0.0)

    # Random seeds
    parser.add_argument("--alpha-seed", type=int, default=None)
    parser.add_argument("--init-seed", type=int, default=None)

    # Simulation parameters
    parser.add_argument("--dt", type = np.float64, default = 0.01)
    parser.add_argument("--scheme", type=str, default="default")
    parser.add_argument("--steps-transient", type = int, default = 100*10000) 
    parser.add_argument("--steps-main", type = int, default = 100*100000) # for data 
    parser.add_argument("--steps-lim-test", type = int, default = 100*1000) # enough to ascertain fixed-point 
    parser.add_argument("--steps-focal", type = int, default = 100*100000) #enough to ascertain fixed-point 

    # Simulation meta parameters
    parser.add_argument("--trials", type = int, default = 5) 
    parser.add_argument("--skip", action="store_true")

    # Diagnostics
    parser.add_argument("--profile", action = "store_true")
    parser.add_argument("--plot", action = "store_true")

    # Output
    parser.add_argument("--output-dir", type=str, default="./data/phase-chaos")

    # Simulation IDs
    parser.add_argument("--sim-id", type = int, default=None)
    parser.add_argument("--job-id", type = int, default=None)

    args = parser.parse_args(arg_list)

    # Generate default IDs
    if args.sim_id is None:
        try:
            args.sim_id =  int(os.getenv('SLURM_ARRAY_TASK_ID'))
        except:
            args.sim_id = 0
    if args.job_id is None:
        try:
            args.job_id = int(os.getenv('SLURM_ARRAY_JOB_ID'))
        except:
            args.job_id = 0

    return args


class ChaosSimulation():
    '''
    Methods
    -------
    from_args
    run
    save_csv
    '''

    @classmethod
    def from_args(cls, args):
        sim = cls()
        
        sim.sim_id =  args.sim_id
        sim.job_id = args.job_id
        
        sim.steps_transient = args.steps_transient
        sim.steps_main = args.steps_main
        sim.steps_lim_test = args.steps_lim_test
        sim.steps_focal = args.steps_focal

        sim._s = glv.sim.UnitGLVSystem(S=args.S, mu=args.mu, sig=args.sig, gam=args.gam, alpha_seed=args.alpha_seed, scheme=args.scheme)
        sim._s.initialize_uniform(low=args.lam, high= 2 / args.S, seed = args.init_seed)

        sim.output_dir = args.output_dir

        sim.plot = args.plot
        sim.stats = {'mu' : args.mu, 'sig' : args.sig, 'chaos' : 0} # all parameters / statistics to be saved

        return sim

    def run(self):

        # Run transient, and check for divergence

        
        tm = Timer("Transient").start()

        nondivergent = self._s.discard_transient(self.steps_transient)
        tm.stop()
        if not nondivergent:
            print("\tDiverged!")
            return False

        # Do limit test

        tm = Timer("First limit test").start()
        traj_limtest = glv.obs.UnitGLVSeries()
        self._s.generate_observables(traj_limtest, self.steps_lim_test, record_interval = 100)
        lim_idx = traj_limtest.lim_idx
        self._s.reset_time() #reset time! 
        tm.stop()
        if lim_idx != 1:
            print("\tNot chaos!")
            return False # not chaos after long transient

        # Gather long trajectory data for statistics
        tm = Timer("Main sim").start()

        traj_main = glv.obs.UnitGLVSeries()
        self._s.generate_observables(traj_main, self.steps_main, record_interval = 100)
        T_lim_test = int(self.steps_lim_test / 100)
        lim_idx = traj_main.limit_index(T_lim_test)
        tm.stop()
        if lim_idx != 1:
            print("\tNot chaos!")
            return False # end of long trajectory is not chaotic
        
        #traj_main.save("./data/traj_main.pkl")

        # Calculate statistics
        tm = Timer("Calc statistics").start()

        self.stats['chaos'] = 1

        eta = glv.obs.EffectiveNoise()
        eta.from_trajectory(traj_main)

        self.stats['k'] = eta.k
        self.stats['u'] = eta.u
        self.stats['tau_n'] = eta.tau 
        self.stats['tau_x'] = traj_main.tau

        self.stats['X'] = np.mean(traj_main.vec_X)
        self.stats['Neff'] = np.mean(traj_main.vec_Neff)
        self.stats['rho'] = np.mean(traj_main.vec_rho)
        
        bins = np.logspace(-10, 1, 100)

        afd = glv.obs.AbundanceDistributions(bins)
        afd.from_trajectory(traj_main)
        self.stats['nu'] = afd.nu

        tm.stop()

        # Simulate effective model
               
        tm = Timer("Eff model").start()              
        r = glv.sim.IntermittencyModel(eta.k, eta.u, eta.tau, self._s.lam)
        r.initialize_uniform()
        r.discard_transient(self.steps_lim_test) # no need to be so long
        traj_eff = glv.obs.IntermittencyModelSeries()
        r.generate_observables(traj_eff, self.steps_focal, record_interval=100)

        afd_eff = glv.obs.AbundanceDistributions(bins)
        afd_eff.from_trajectory(traj_eff)
        
        self.stats['nu_eff'] = afd_eff.nu

        tm.stop()

        if self.plot:
            pass

        return True

    def save_csv(self):
        csv_path = joinpath(self.output_dir, "csv", f"dat_{self.sim_id}.csv")
        df = pd.DataFrame.from_dict({key : [val] for (key,val) in self.stats.items()})
        df.to_csv(csv_path, index=False, mode='w+')


if __name__ == '__main__':
    args = parse_args(sys.argv[1:])
    gt = Timer(f"Whole script [id={args.sim_id}]", absolute=True).start()

    if args.profile:
        import tracemalloc
        tracemalloc.start()

    if not args.skip:
        done = False
        i = 0
        while (not done) and (i < args.trials):
            tm = Timer(f"Trial {i}").start()
            cs = ChaosSimulation.from_args(args)
            done = cs.run()
            tm.stop()
            i += 1
        if not done:
            raise RuntimeError(f"Did not find complete chaotic trajectory with {args.trials} tries")
    else:
        cs = ChaosSimulation.from_args(args)


    cs.save_csv()
       
    if args.profile:
        current_bytes, peak_bytes = tracemalloc.get_traced_memory()
        tracemalloc.stop()
        print(f"Peak memory usage: {peak_bytes/10**9:.4f}GB")
        
    gt.stop()
 