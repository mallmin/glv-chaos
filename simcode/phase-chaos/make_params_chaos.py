
import os, sys
sys.path.append('./')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import argparse
import glv
from matplotlib import colors
import numpy.ma as ma
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

###############################################################################
# Simulation arguments
###############################################################################

output_dir = joinpath('./data/phase-chaos')
S = 500
lam = 10**-8

steps_transient = 100*10000 
steps_main = 100*100000
steps_lim_test = 100*1000 
steps_focal = 100*1000000


###############################################################################
# Making of the argument file
###############################################################################



# Get, X,Y mesh from adiabatic 
df = pd.read_csv("./data/phase-rand/data.csv")
df['cp'] = df['n_chaos'] / df['N']
X,Y,Z = glv.phase.meshgridXYZ(df, 'mu', 'sig', 'cp')

# Get Z mesh from hand-drawn image
img = plt.imread(joinpath(currentdir,'chaos_region.png'))
Z = img[::-1,:,0] # The chaos region!

with open(joinpath(currentdir, f"params_chaos.txt"),"w") as f:
    
    # Put the chaotic ones first
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            mu = X[i,j]
            sig = Y[i,j]
            c = Z[i,j]
            if c > 0:
                args_sim = f"-S {S} --mu {mu} --sig {sig} --lam {lam}"
                args_steps_dir = f"--steps-transient {steps_transient} --steps-lim-test {steps_lim_test} --steps-main {steps_main} --steps-focal {steps_focal} --output-dir {output_dir}"
                args_tot =  args_sim + " " + args_steps_dir + (" --skip" if not c else "") + "\n"
                f.write(args_tot)
    
    # Nonchaotc last
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            mu = X[i,j]
            sig = Y[i,j]
            c = Z[i,j]
            if c == 0.0:
                f.write(f"-S {S} --mu {mu} --sig {sig} --lam {lam} --output-dir {output_dir} --skip\n")


