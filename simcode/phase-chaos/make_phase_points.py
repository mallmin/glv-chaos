'''
Script to define all points (mu,sig) that will be part of the chaotic phase.

The region is 'drawn' by hand in an png - one pixel per (mu,sig) -, with visual reference to the 
adiabatic and chaos-probability phase diagram

'''

import os, sys
sys.path.append('./')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import argparse
import glv
from matplotlib import colors
import numpy.ma as ma
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


def get_mesh_adiab(plot=False):
    df = pd.read_csv("./data/phase-adiab/data_pre_overhaul.csv")
    X,Y,Z = glv.phase.meshgridXYZ(df, 'mu', 'sig', 'lim_idx')
   
    if plot:
        fig, ax = plt.subplots()
        ax.pcolormesh(X,Y,Z, cmap='viridis', alpha=0.5)
        fig.savefig(joinpath(currentdir,f"phase_adiab.png"), dpi=600, bbox_inches='tight',pad_inches=0,transparent=True)

    return X,Y,Z

def get_mesh_rand(p=0.7, plot=False):
    df = pd.read_csv("./data/phase-rand/data.csv")
    df['cp'] = df['n_chaos'] / df['N']
    X,Y,Z = glv.phase.meshgridXYZ(df, 'mu', 'sig', 'cp')
    Z = (Z > p)

    if plot:
        fig, ax = plt.subplots()
        ax.pcolormesh(X,Y,Z, cmap='viridis')
        fig.savefig(joinpath(currentdir,f"phase_rand_{p}.png"), dpi=600, bbox_inches='tight',pad_inches=0,transparent=True)
        # Version to save and modify in paint
        plt.imsave(joinpath(currentdir,'pixels.png'), Z[::-1,:])
           
    return X,Y,Z


X1, Y1, Z1 = get_mesh_rand()
X2, Y2, Z2 = get_mesh_adiab()



Z2[Z2 > 1] = 2

img = plt.imread(joinpath(currentdir,'chaos_region.png'))
Z = img[::-1,:,0] # The chaos region!

fig, ax = plt.subplots()
ax.pcolormesh(X1,Y1,Z, cmap='viridis', alpha=0.5)
ax.pcolormesh(X2,Y2,Z2, cmap='viridis', alpha=0.5)
fig.savefig(joinpath(currentdir,f"overlap_adiab.png"), dpi=600, bbox_inches='tight',pad_inches=0,transparent=True)


# Now we save the pairs of mu, delta 

list_mu_chaos = []
list_sig_chaos = []
for i in range(Z.shape[0]):
    for j in range(Z.shape[1]):
        mu = X1[i,j]
        sig = Y1[i,j]
        if Z[i,j] > 0:
            list_mu_chaos.append(mu)
            list_sig_chaos.append(sig)

# add non-chaotic point for range's sake
list_mu_chaos.append(1.0)
list_sig_chaos.append(0.0)
vec_c = np.ones(len(list_mu_chaos))
vec_c[-1] = 0

df_new = pd.DataFrame({'mu' : list_mu_chaos, 'sig' : list_sig_chaos, 'chaos' : vec_c})

X3, Y3, Z3 = glv.phase.meshgridXYZ(df_new, 'mu', 'sig', 'chaos')

fig, ax = plt.subplots()
ax.pcolormesh(X3,Y3,Z3, cmap='viridis')
#plt.show()

# Now we make a new datatable


Z_chaos = ma.filled(Z3, 0)

list_mu = []
list_sig = []
list_c = []
for i in range(Z.shape[0]):
    for j in range(Z.shape[1]):
        mu = X1[i,j]
        sig = Y1[i,j]
        list_mu.append(mu)
        list_sig.append(sig)
        if Z[i,j] > 0:
            list_c.append(1)
        else:
            list_c.append(0)
        
df_full = pd.DataFrame({'mu' : list_mu, 'sig' : list_sig, 'chaos' : list_c})
df_full.to_csv(joinpath(currentdir,"phase_chaotic.csv"),index=False)
