#!/bin/bash

#SBATCH -p amd  
#SBATCH -J chaos
#SBATCH -N 1 
#SBATCH -n 1
#SBATCH --mem-per-cpu 5800
#SBATCH -t 72:00:00
#SBATCH --array 1-2848
#SBATCH -o ./data/phase-chaos/log/%a.log
#SBATCH -e ./data/phase-chaos/err/%a.err

cd $HOME/glv-chaos/
python3 simcode/phase-chaos/sim_phase_chaos.py `sed " $SLURM_ARRAY_TASK_ID!d" $HOME/glv-chaos/simcode/phase-chaos/params_chaos.txt`