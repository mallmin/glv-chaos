'''
This is the simulation code for the data underlying Fig. 5

At fixed sig, we simulate adiabatically from hogh to low mu, and then back,
and record the abundances obtained after transient at each value.
'''

# Paths
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(".")
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

import glv
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import default_rng
from timer import Timer
import argparse
from scipy.signal import argrelextrema, argrelmax, argrelmin
import myutils as utl
from matplotlib.gridspec import GridSpec

parser = argparse.ArgumentParser()
parser.add_argument("-r", action = "store_true")
args = parser.parse_args()

S = 500
LAM = 1e-8
SIG = 0.3
MU_FIRST = 0.6
MU_LAST =  1.4
MU_DELTA = 0.01
RANGE_MU = np.around(np.linspace(MU_FIRST, MU_LAST, round( ( MU_LAST - MU_FIRST) / MU_DELTA ) + 1), 10)
ALPHA_SEED = 12345
INIT_SEED = 54321


class AdiabaticLine(utl.Container):

    def __init__(self, S=S, range_mu=RANGE_MU, sig=SIG, lam=LAM, alpha_seed=ALPHA_SEED, init_seed=INIT_SEED):
        self.range_mu = range_mu
        self.s = glv.sim_legacy.UnitGLVSystem(S=S, mu=0, sig=sig, mat_alpha=None, lam=lam)
        self.s.initialize_uniform(low=lam, high= 2 / S, seed = init_seed)
        self.mat_z = glv.sim_legacy.gaussian_random_matrix(self.s.S, 0., 1., seed=alpha_seed) 
        self.traj = glv.obs.AbundanceSeries()
        
        self.Xs = []
        self.mat_x = np.zeros((self.range_mu.size, self.s.S))
        self.lim_idxs = []

        self.idxof = { mu : i for (i,mu) in enumerate(range_mu) }

         # (mu, lim_idx, Xvals)

    def run(self, first=True):
        tm = utl.Timer("Start sweep").start()
        
        #first = True
        for i,mu in enumerate(self.range_mu):                     
            (lim_idx, X, vec_x) = self.run_point(mu, first)
            print(f"mu {mu}: {lim_idx}; {X.size}")
            first = False
            self.lim_idxs.append(lim_idx)
            self.mat_x[i,:] = vec_x
            self.Xs.append(X)

            if lim_idx == -1:
                break
        del self.traj
        tm.stop()


        ''' To make a bar at each time point'''


        
    
    def run_point(self, mu, first=False):
        #slightly perturb
        #self.s.vec_x = self.s.vec_x + 10* self.s.lam
        # update the interaction matrix
        self.s.mat_alpha = mu + self.s.sig * self.mat_z
        np.fill_diagonal(self.s.mat_alpha, 0)
        self.s.mu = mu
        steps = 100*50000 if first else 100*1000
        self.s.discard_transient(100*2000)
        self.s.generate_observables(self.traj, steps, record_interval = 100)
        #lim_idx = glv.comp.limit_index(self.traj.mat_x)
        lim_idx = self.traj.lim_idx

        if lim_idx == -1: 
            return (-1, np.array(), np.full(self.s.S, np.NaN))
        elif lim_idx == 0:
            X = np.sum(self.s.vec_x)
            return (0,np.array([X]), self.s.vec_x)
        else:
            vec_X = np.sum(self.traj.mat_x, axis=1)
            idx_maxima = argrelmax(vec_X)[0]
            idx_minima = argrelmin(vec_X)[0]
            idx_extrema = np.sort(np.concatenate((idx_maxima, idx_minima)))
            vec_extrema = vec_X[(idx_extrema,)]
            return(lim_idx, vec_extrema, self.s.vec_x)

    def plot_cascade(self, ax):
        
        mus = []
        Xs = []
        cs = []

        for i, vec_X in enumerate(self.Xs):
            for X in vec_X:
                mus.append(self.range_mu[i])
                Xs.append(X)
                cs.append(self.lim_idxs[i])
        
        ax.scatter(mus,Xs, s=0.2, c='k')
        if self.range_mu[0] > self.range_mu[-1]:
            ax.set_xlim(ax.get_xlim()[::-1])

    def plot_abundances(self, ax, order=False):
        colors = glv.rand_cmap(self.s.S)

        n = self.range_mu.size 
        mat_x_plus = np.zeros((2 * n, self.s.S))
        vec_mu_plus = np.zeros(2*n)
        delta_mu = np.diff(self.range_mu)[0]
        eps = 0.01 * delta_mu

        for i in range(n):
            mat_x_plus[2*i, :] = self.mat_x[i,:]
            mat_x_plus[2*i+1, :] = self.mat_x[i,:]
            vec_mu_plus[2*i] = self.range_mu[i] - delta_mu/2 + eps
            vec_mu_plus[2*i+1] = self.range_mu[i] + delta_mu/2 - eps
            
        ax.stackplot(vec_mu_plus, mat_x_plus.T, colors=colors)
        if (not order) and (self.range_mu[0] > self.range_mu[-1]):
            ax.set_xlim(ax.get_xlim()[::-1])

    def plot_diversity(self, ax, order=False):
        vec_D2 = glv.obs.Diversity.EffSize(self.mat_x)
        ax.plot(vec_D2)


# If run for new data
if args.r:
    ad_path = joinpath(currentdir,f"ad_{args.t}.pkl")
    ad_rev_path = joinpath(currentdir,f"ad_rev_{args.t}.pkl")

    ad = AdiabaticLine()
    ad.run()
    ad.save(ad_path)
    ad_rev = AdiabaticLine(range_mu=ad.range_mu[::-1])
    ad_rev.s.vec_x = ad.s.vec_x
    ad_rev.run(False)
    ad_rev.save(ad_rev_path)

