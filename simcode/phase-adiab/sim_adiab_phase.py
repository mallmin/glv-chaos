import sys
from adiab import *


# traj = utl.Container.load("./data/test/traj_adiab.pkl")

# print(traj.mat_x)

# fig, ax = plt.subplots()

# range_mu = [mu for (mu,sig) in traj.list_mu_sig]
# ax.stackplot(np.array(range_mu), traj.mat_x.T)

# fig.savefig("./data/test/traj.png")

# quit()
arg_list = sys.argv[1:]

# Setup
args = parse_args(arg_list)
gt = utl.Timer(f"Whole script [id={args.sim_id}]", absolute=True).start()
if args.profile:
    import tracemalloc
    tracemalloc.start()

# Main code
sim = AdiabaticSimulation.from_args(args)
sim.run()
sim.save_csv()
sim.save_trajectory()

# Finalize 
if args.profile:
    current_bytes, peak_bytes = tracemalloc.get_traced_memory()
    tracemalloc.stop()
    print(f"Peak memory usage: {peak_bytes/10**9:.4f}GB")
gt.stop()

