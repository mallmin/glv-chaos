'''
Making the params for adiabatic phase diagram
'''

import numpy as np
import os, sys
from os.path  import join as joinpath
import argparse
currentdir = os.path.dirname(os.path.realpath(__file__))

S = 500
lam = 1e-8

alpha_seed = 1234
init_seed = 2345

params_dir = currentdir
output_dir = joinpath('./data/phase-adiab')

mu_first = 1.4
mu_last = - 0.1
delta_mu = -0.01

sig_first = 0
sig_last = 1.4
delta_sig = 0.01

mode = 'h'

short_transient = 2000*100
long_transient = 100000*100
short_main = 1000*100
long_main = 1000*100


with open(joinpath(params_dir, f"params_adiabatic_{mode}.txt"),"w") as f:
    
    if mode == 'h':
        sig_range = np.around(np.linspace(sig_first, sig_last, round((sig_last - sig_first)/delta_sig ) + 1), 4)
        for sig in sig_range:
            f.write(f"-S {S} --mu-range {mu_first} {mu_last} {delta_mu} --sig {sig} --lam {lam} --alpha-seed {alpha_seed} --init-seed {init_seed} --steps-transient-short {short_transient} --steps-transient-long {long_transient} --steps-main-short {short_main} --steps-main-long {long_main} --output-dir {output_dir}\n")
    elif mode == 'v':
        mu_range = np.around(np.linspace(mu_first, mu_last, round((mu_last - mu_first)/delta_mu ) + 1), 4)
        for mu in mu_range:
            f.write(f"-S {S} --sig-range {sig_first} {sig_last} {delta_sig} --mu={mu} --lam {lam} --alpha-seed {alpha_seed} --init-seed {init_seed} --steps-transient-short {short_transient} --steps-transient-long {long_transient} --steps-main-short {short_main} --steps-main-long {long_main} --output-dir {output_dir}\n")