'''
Script to adiabatically simulate one horizontal or vertical line of the phase diagram.
Records limit type, and some additional statistics for each point along the line, and saves to csv.
'''

# Paths
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(".")
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

import glv
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import default_rng
import argparse
import myutils as utl
import pandas as pd
import warnings



def parse_args(arg_list):
    '''
    Parses the command line arguments.

    Returns
    -------
    args
        object containing arguments
    '''
    parser = argparse.ArgumentParser()

    # Model parameters
    parser.add_argument("-S", type = int, default = 500)
    parser.add_argument("--mu-range", type = np.float64, nargs=3, default = None) #mu-range and sig-range are mutually exclusive
    parser.add_argument("--sig-range", type = np.float64, nargs=3, default = None)
    parser.add_argument("--mu", type = np.float64,  default = None)
    parser.add_argument("--sig", type = np.float64,  default = None)
    parser.add_argument("--lam", type = np.float64, default = 10**-8)
    parser.add_argument("--gam", type = np.float64,  default = 0.0)

    # Random seeds
    parser.add_argument("--alpha-seed", type=int, default=None)
    parser.add_argument("--init-seed", type=int, default=None)

    # Simulation parameters
    parser.add_argument("--dt", type = np.float64, default = 0.01)
    parser.add_argument("--scheme", type=str, default="log")
    parser.add_argument("--steps-transient-long", type = int, default = 100*10000) #for swithcing of limit type
    parser.add_argument("--steps-transient-short", type = int, default = 100*1000) #for when same limit type
    parser.add_argument("--steps-main-short", type = int, default = 100*1000) #enough to ascertain fixed-point 
    parser.add_argument("--steps-main-long", type = int, default = 100*2000) #data for non-FP
    parser.add_argument("--max-diverge", type = int, default = 100)

    parser.add_argument("--record-trajectory", action = "store_true")

    # Diagnostics
    parser.add_argument("--profile", action = "store_true")
    parser.add_argument("--plot", action = "store_true")

    # Output
    parser.add_argument("--output-dir", type=str, default="./data/phase-adiab")

    # Simulation IDs
    parser.add_argument("--sim-id", type = int, default=None)
    parser.add_argument("--job-id", type = int, default=None)

    args = parser.parse_args(arg_list)

    # Generate default IDs
    if args.sim_id is None:
        try:
            args.sim_id =  int(os.getenv('SLURM_ARRAY_TASK_ID'))
        except:
            args.sim_id = 0
    if args.job_id is None:
        try:
            args.job_id = int(os.getenv('SLURM_ARRAY_JOB_ID'))
        except:
            args.job_id = 0

    return args


class AdiabaticSeries(utl.Container):
    '''
    For recording the trajectory of abundances just before each interaction matrix change
    '''
    def __init__(self, T, S, mat_z, list_mu_sig, lam):
        self.mat_x = np.zeros((T,S))
        self.list_mu_sig = list_mu_sig
        self.mat_z = mat_z
        self.lam = lam
        self._n = 0

    def add_next(self, vec_x):
        self.mat_x[self._n, :] = vec_x
        self._n += 1    


class AdiabaticSimulation():
    '''
    Organizes parameters, execution, and recording of result for adiabatic dLV simulation
    along a horizontal or vertical line in the mu,sig-plane.

    Methods
    -------
    from_args
    run
    save_csv
    '''

    @classmethod
    def from_args(cls, args):
        '''
        Create object from parsed command line arguments
        '''
        sim = cls()
        
        sim.meta = {
            'sim_id' : args.sim_id,
            'job_id' : args.job_id
        }

        sim.steps_transient_long = args.steps_transient_long
        sim.steps_transient_short = args.steps_transient_short
        sim.steps_main_long = args.steps_main_long
        sim.steps_main_short = args.steps_main_short        
        sim.max_div = args.max_diverge 
        sim.S = args.S

        # detect from arguments if adabatic sweep is horizontal or vertical
        if (args.mu_range is not None) and (args.sig_range is None) and (args.sig is not None):
            sim.mode = 'horizontal'
            sim.sig = args.sig
            mu_first =  args.mu_range[0]
            mu_last = args.mu_range[1]
            n_mu = round( ( mu_last - mu_first) / args.mu_range[2] )
            sim.mu_range = np.around(np.linspace(mu_first, mu_last, n_mu + 1),4)
            sim.list_mu_sig = [(mu, sim.sig) for mu in sim.mu_range]
            
        elif (args.mu_range is None) and (args.sig_range is not None) and (args.mu is not None):
            sim.mode = 'vertical'
            sim.mu = args.mu
            sig_first =  args.sig_range[0]
            sig_last = args.sig_range[1]
            n_sig = round( ( sig_last - sig_first) / args.sig_range[2] )
            sim.sig_range = np.around(np.linspace(sig_first, sig_last, n_sig + 1),4)
            sim.list_mu_sig = [(sim.mu, sig) for sig in sim.sig_range]
        else:
            raise ValueError("Not a horizontal or vertical sweep!") 

        sim.scheme = args.scheme
        sim.output_dir = args.output_dir

        sim.mat_z = glv.sim_legacy.gaussian_random_matrix(args.S, 0., 1., seed=args.alpha_seed) 

        sim._s = glv.sim_legacy.UnitGLVSystem(S=args.S, mu=0, sig=0, gam=args.gam, mat_alpha=sim.mat_z, scheme=sim.scheme)
        sim._s.initialize_uniform(low=args.lam, high= 2 / args.S, seed = args.init_seed)

        # together with _s hold the state the last simulation point ended on        
        sim.traj = glv.obs.UnitGLVSeries()
        sim.vec_x_last_nondivergent = sim._s.vec_x
        sim.prev_lim_idx = 0
        sim.number_diverged = 0
        
        sim.results = [] #a list of a dictonary of named results for each simulation point
        
        
        {} # (mu,sig) : (lim_idx,Neff)

        if args.record_trajectory:
            S = sim._s.S
            T = len(sim.list_mu_sig)
            sim.traj_adiab = AdiabaticSeries(T, S, sim.mat_z, sim.list_mu_sig, sim._s.lam)
        else:
            sim.traj_adiab = None

        return sim

    def run(self):
        '''
        Run adiabatic simulation of the full line, store results
        '''
        first = True # long transient for first point first point
        for (mu,sig) in self.list_mu_sig:
            res = self._run_next_point(mu, sig, first=first)
            self.results.append(res)
            first = False

    def _run_next_point(self, mu, sig, first=False, plot=False):
        '''
        Returns the result of the next point (mu,sig), starting from the simulation
        state of the previous point
        '''

        res = self._data_row(mu=mu, sig=sig) #results returned

        # If many consecutive divergences, assume rest diverge too
        if self.number_diverged > self.max_div:
            lim_idx = -1
            res['lim_idx'] = -1
        else:                
            # Next we simulate, without knowing yet whether the new point will have the same
            # limit type as the previous. To begin with we assume it will, therefore we use: 
            # * short transient (unless it's the very first simulation point) because attractor
            #   should moves just a little
            # * short data gathering if previous was FP (because we only need to be sure it's an FP),
            #   otherwise long (because we need more data for good time-averages)
                
            steps_transient = self.steps_transient_long if first else self.steps_transient_short 
            steps_main = self.steps_main_short if self.prev_lim_idx == 0 else self.steps_main_long
            
            if steps_transient == self.steps_transient_long:
                print("LONG TRANSIENT! (First) ")
            if steps_main == self.steps_main_long:
                print("LONG MAIN! (Prev not FP) ")

            lim_idx = self._forward_simulation(mu, sig, steps_transient, steps_main)

            # must check if we diverged           
            if lim_idx == -1:
                self.number_diverged += 1
                res['lim_idx'] = -1
            else:
                # Now we can check if we have switched from a fixed point to chaos/cycle.
                # If so, it could be that we're in the multiple-attractor phase and will 
                # eventually find a different FP, therefore we need to resimulate with a
                # long transient 

                chaos_or_cycle_after_fp = (lim_idx not in [-1,0] ) and ( self.prev_lim_idx == 0 )

                if chaos_or_cycle_after_fp:
                    
                    print("LONG TRANSIENT & MAIN! (CCAFP)")
                    lim_idx = self._forward_simulation(mu, sig, self.steps_transient_long, self.steps_main_long)
                    
                    # again, check divergence                
                    if lim_idx == -1:
                        self.number_diverged += 1
                        res['lim_idx'] = -1
                
                if lim_idx != -1:
                    # If we arrive here, then we have not diverged, and we are confident 
                    # in that limit type is stable, and we have enough data for statistics

                    self.number_diverged = 0 #reset!

                    res['lim_idx'] = lim_idx
                    
                    vec_Neff = self.traj.vec_Neff
                    res['Neff'] = np.mean(vec_Neff)
                    res['dNeff'] = np.std(vec_Neff)

                    vec_rho = self.traj.vec_rho
                    res['rho'] = np.mean(vec_rho)
                    res['drho'] = np.std(vec_rho)
                    
                    vec_X = self.traj.vec_X
                    res['X'] = np.mean(vec_X)
                    res['dX'] = np.std(vec_X)

                    self.vec_x_last_nondivergent = self._s.vec_x 
                
        # Add abundances to adiabatic trajectory, if it's being stored
        if self.traj_adiab is not None:
            if lim_idx != -1:
                vec_x_end = self._s.vec_x
            else:
                vec_x_end = np.empty(self._s.S)
                vec_x_end[:] = np.nan
            self.traj_adiab.add_next(vec_x_end)

        # plot also if cycle, to look at whether lim cycle or heteroclinic
        if plot or lim_idx > 1: 
            fig, ax = plt.subplots()
            colors = glv.rand_cmap(self._s.S)
            ax.stackplot(self.traj.vec_t, self.traj.mat_x.T, colors=colors)
            fig.savefig(joinpath(self.output_dir, "graphs",f"stack_{self.meta['sim_id']}"))

        self.prev_lim_idx = res['lim_idx']
        
        return res
            

    def _forward_simulation(self, mu, sig, steps_transient, steps_recorded, perturb=False):
        '''
        Runs the simulation forward, with the (mu,sig)-matrix, by an unrecorded transient, and then a
        recorded trajectory

        Returns
        -------
        lim_idx
            the limit index based on the recorded trajectory portion; or divergence if it occurrs in the transient   
        '''
        
        # if current state is divergent, change to previous nondivergent state
        if self.prev_lim_idx == -1:
            self._s.vec_x = self.vec_x_last_nondivergent 

        # can slightly perturb (nonrandomly)
        if perturb:
            self._s.vec_x = self._s.vec_x + 10* self._s.lam

        # update the interaction matrix
        self._s.mat_alpha = mu + sig * self.mat_z
        np.fill_diagonal(self._s.mat_alpha, 0)
        self._s.mu = mu
        self._s.sig = sig

        # do the transient
        nondivergent = self._s.discard_transient(steps_transient)

        if not nondivergent:
            return -1

        # run recorded simulation
        self._s.generate_observables(self.traj, steps_recorded, record_interval = 100)
        #lim_idx = glv.comp.limit_index(self.traj.mat_x) #deprecated 
        lim_idx = self.traj.lim_idx

        return lim_idx

    def _data_row(self, mu=None, sig=None):
        return {'mu' : mu, 'sig' : sig} | {field : None for field in ['lim_idx','Neff','dNeff', 'rho', 'drho', 'X', 'dX']}
    
    def save_csv(self):
        '''
        Save data for the line to csv
        '''

        field_labels = list(self._data_row().keys())
        df = pd.DataFrame.from_dict({label : [ res[label] for res in self.results] for label in field_labels})
        csv_path = joinpath(self.output_dir, "csv", f"dat_{self.meta['sim_id']}.csv")
        df.to_csv(csv_path, index=False, mode='w+')


    def save_trajectory(self):
        '''
        Save trajectory of abundances, if it's been stored
        '''
        if self.traj_adiab is not None:
            path = joinpath(self.output_dir, "traj_adiab.pkl")
            self.traj_adiab.save(path)
        else:
            warnings.warn("Cannot save: no abundance data was recorded")


if __name__ == "__main__":
    
    arg_list = sys.argv[1:]

    # Setup
    args = parse_args(arg_list)
    gt = utl.Timer(f"Whole script [id={args.sim_id}]", absolute=True).start()
    if args.profile:
        import tracemalloc
        tracemalloc.start()

    # Main code
    sim = AdiabaticSimulation.from_args(args)
    sim.run()
    sim.save_csv()
    if args.record_trajectory:
        sim.save_trajectory()

    # Finalize 
    if args.profile:
        current_bytes, peak_bytes = tracemalloc.get_traced_memory()
        tracemalloc.stop()
        print(f"Peak memory usage: {peak_bytes/10**9:.4f}GB")
    gt.stop()
