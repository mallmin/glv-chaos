#!/bin/bash

#SBATCH -p amd  
#SBATCH -J adiab
#SBATCH -N 1 
#SBATCH -n 1
#SBATCH --mem-per-cpu 1000
#SBATCH -t 48:00:00
#SBATCH --array 1-141
#SBATCH -o ./data/phase-adiab/log/%a.log
#SBATCH -e ./data/phase-adiab/err/%a.err

cd $HOME/glv-chaos/
python3 simcode/phase-adiab/adiab.py `sed " $SLURM_ARRAY_TASK_ID!d" $HOME/glv-chaos/simcode/phase-adiab/params_adiabatic_h.txt`