#!/bin/bash

#SBATCH -p gpu
#SBATCH -J S=1280
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem-per-cpu 2074
#SBATCH -t 48:00:00
#SBATCH -o ./simcode/traj/log/%a.out
#SBATCH -e ./simcode/traj/log/%a.err

python3 ./simcode/traj/traj.py -S 1280 --mu 0.5 --sig 0.3 --lam 1e-08 -T 100000 -o data/traj-S-range/traj_S=1280.pkl