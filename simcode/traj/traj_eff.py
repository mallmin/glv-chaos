'''
Simulates and saves a trajectory of the stochastic intermittency model
'''

import os, sys
sys.path.append('.')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import myutils as utl
import argparse
import glv

gt = utl.Timer("Script").start()

parser = argparse.ArgumentParser()

parser.add_argument("--lam", type = np.float64,  default = 10**-8)
parser.add_argument("-k", type = np.float64,  default = 0.)
parser.add_argument("-u", type = np.float64,  default = 0.)
parser.add_argument("--tau", type = np.float64,  default = 0.)

# Time parameters
parser.add_argument("--dt", type=np.float64, default=0.01)
parser.add_argument("--transient-steps", type=int, default=100*1000)
parser.add_argument("-T", type=int, default=10000, help="Time to run; overrides --steps") #
parser.add_argument("--steps", type=int, default=100*1000)

parser.add_argument("--output-dir", type=str, default="./data/")
parser.add_argument("--name", type=str, default="traj_eff_ref")


args = parser.parse_args()


r = glv.sim_legacy.IntermittencyModel(args.k, args.u, args.tau, args.lam)
r.initialize_uniform()
r.discard_transient(args.transient_steps)
traj = glv.obs.IntermittencyModelSeries()
r.generate_observables(traj, args.steps, record_interval=100)
traj.save(joinpath(args.output_dir, args.name + ".pkl"))


gt.stop()


