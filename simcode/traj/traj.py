'''
Generate and save trajectory data for a disorder Lotka-Volterra model
with random Gaussian interactions, constant migration, and unit carrying
capacities and growth rates.

'''

import os, sys
sys.path.append('.')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import myutils as utl
import argparse
import glv


tr = glv.Tracker().start("Trajectory simulation")

##############################################################
# PARSE ARGUMENTS
##############################################################

parser = argparse.ArgumentParser()

parser.add_argument("-S", type=int, default=500)
parser.add_argument("--mu",  type = np.float64, default = 0.5)
parser.add_argument("--sig", type = np.float64, default = 0.3)
parser.add_argument("--lam", type = np.float64,  default = 1e-8)
parser.add_argument("--gam", type = np.float64,  default = 0.)

parser.add_argument("--alpha-seed", type = int,  default = 12345)


# Alternatively give scaled parameters for weak regime
parser.add_argument("--mut",  type = np.float64, default = None)
parser.add_argument("--sigt", type = np.float64, default = None)
parser.add_argument("--lamt", type = np.float64,  default = None)

# Time parameters
parser.add_argument("-d","--transient", type=int, default=1000)
parser.add_argument("-T", type=int, default=1000)

parser.add_argument("--scheme", type = str,  default = "default")
parser.add_argument("--label", type=str, default="", help="string saved with data")

# output
parser.add_argument("-o","--output", type=str, default="./data/traj.pkl")


args = parser.parse_args()

if args.mut is not None: args.mu = args.mut / args.S
if args.sigt is not None: args.sig = args.sigt / np.sqrt(args.S)
if args.lamt is not None: args.lam = args.lamt / args.S

##############################################################
# SETUP & RUN SIMULATION
##############################################################

s = glv.DisorderedLVSystem(
    S = args.S,
    mu = args.mu,
    sig = args.sig,
    gam = args.gam,
    lam = args.lam,
    scheme = args.scheme
    )

s.initialize_uniform()

traj = s.trajectory(steps=100*args.T, transient=100*args.transient, record_interval=100)

traj.save(args.output)

tr.end()


