'''
Generate .sh files for producing trajectories on on ADA
'''

import os, sys
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(".")
def subpath(*args): return joinpath(currentdir, *args)



sbatch_default = {
    'partition' : 'gpu',
    'memory' : 1000,
    'time' : '24:00:00'
}


args_default = {
    '-S' : 500,
    '--mu' : 0.5,
    '--sig' : 0.3,
    '--lam' : 1e-8,
    '-T' : 100000
}

jobs = []

# Reference simulation

args = args_default.copy()
args.update({'-o' : "data/traj_ref.pkl"})
jobs.append(("ref", sbatch_default, args))


# simulation of different lambdas

for lam in [1e-8,1e-12,1e-16,1e-20,1e-24,1e-28,1e-32]:
    args = args_default.copy()
    args.update({'--lam' : lam, "-o" : f"data/traj-lam-range/traj_lam={lam}.pkl"})
    jobs.append((f"lam={lam}",sbatch_default, args))
       

# simulation of different S

for S in [320, 640, 1280, 2560, 5120, 10240]:
    args = args_default.copy()
    args.update({'-S' : S, "-o" : f"data/traj-S-range/traj_S={S}.pkl"})

    T = args_default['-T']
    sbatch = sbatch_default.copy()
    sbatch.update({'memory' : int(2 * 8e-6 * (T*S + S*S) ), 'time' : '48:00:00'})

    jobs.append((f"S={S}",sbatch, args)
        
    )

# Now make the files


for job in jobs:
    tag = job[0]
    sbatch = job[1]
    args_str = ' '.join([str(k)+' '+str(v) for k,v in job[2].items()])
    with open(subpath(f"job_{tag}.sh"), 'w') as f:
        f.write(f"#!/bin/bash\n\n")
        f.write(f"#SBATCH -p {sbatch['partition']}\n")
        f.write(f"#SBATCH -J {tag}\n")
        f.write("#SBATCH -N 1\n")
        f.write("#SBATCH -n 1\n")
        f.write(f"#SBATCH --mem-per-cpu {sbatch['memory']}\n")
        f.write(f"#SBATCH -t {sbatch['time']}\n")
        f.write("#SBATCH -o ./simcode/traj/log/%a.out\n")
        f.write("#SBATCH -e ./simcode/traj/log/%a.err\n\n")
        f.write(f"python3 ./simcode/traj/traj.py {args_str}")
