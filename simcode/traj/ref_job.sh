#!/bin/bash

#SBATCH -p gpu  
#SBATCH -J traj
#SBATCH -N 1 
#SBATCH -n 1
#SBATCH --mem-per-cpu 3000 
#SBATCH -t 1:00:00
#SBATCH -o ./simcode/traj/log/%a.out
#SBATCH -e ./simcode/trah/log/%a.err
 
python3 ./simcode/traj/traj.py -S 500 --scheme log --lam 1e-48 --label 1e-48 -T 100000