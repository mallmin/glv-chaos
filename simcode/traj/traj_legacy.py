'''
Generate and save full trajectory data.
'''

import os, sys
sys.path.append('.')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import myutils as utl
import argparse
import glv


gt = utl.Timer("Script").start()

parser = argparse.ArgumentParser()

parser.add_argument("-S", type=int, default=500)
parser.add_argument("--mu",  type = np.float64, default = 0.5)
parser.add_argument("--sig", type = np.float64, default = 0.3)
parser.add_argument("--lam", type = np.float64,  default = 10**-8)
parser.add_argument("--gam", type = np.float64,  default = 0.)

# Alternatively give scales parameters
parser.add_argument("--mut",  type = np.float64, default = None)
parser.add_argument("--sigt", type = np.float64, default = None)
parser.add_argument("--lamt", type = np.float64,  default = None)

# Time parameters
parser.add_argument("--dt", type=np.float64, default=0.01)
parser.add_argument("--steps-transient", type=int, default=100*1000)
parser.add_argument("--steps", type=int, default=100*1000)


parser.add_argument("--scheme", type = str,  default = "default")
parser.add_argument("--label", type=str, help="string saved with data")

# output
parser.add_argument("-o", type=str, default="./data/traj.pkl")


args = parser.parse_args()

if not (args.mut is None): args.mu = args.mut / args.S
if not (args.sigt is None): args.sig = args.sigt / np.sqrt(args.S)
if not (args.lamt is None): args.lam = args.lamt / args.S
# if not 

# steps = int(steps_per_time 

s = glv.sim_legacy.UnitGLVSystem(args.S, mu=args.mu, sig=args.sig, gam=args.gam, lam=args.lam, dt=args.dt, scheme=args.scheme)
s.initialize_uniform()
s.discard_transient(args.steps_transient)

traj = glv.obs.UnitGLVSeries()
s.generate_observables(traj, args.steps, record_interval=100)
#traj.save(joinpath(currentdir, "./data/traj-strong-chaos-lam", args.label + ".pkl"))
traj.save(args.o)

gt.stop()


