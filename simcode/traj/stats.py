'''
Script to output some simple statistics of a trajectory
'''


import os, sys
sys.path.append('.')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import myutils as utl
import argparse
import glv

gt = utl.Timer("Script").start()

parser = argparse.ArgumentParser()
parser.add_argument("--traj", type=str, default="./data/traj_strong_ref.pkl")
parser.add_argument("-o", "--output-file", type=str)

args = parser.parse_args()

traj = glv.load_observable(args.traj)

with open(joinpath(args.output_file), "w") as f:
    
    # Parameters for the noise
    eta = glv.focal.EffectiveNoise()
    eta.from_trajectory(traj)
 
    sad = glv.distribution.AbundanceDistributions(np.logspace(-10, 1, 100))
    sad.from_trajectory(traj)
    
    u_approx = traj.sig * np.mean(traj.vec_X) / np.sqrt( np.mean(traj.vec_Neff))

    print(args.traj, file=f)
    print(f"k = {eta.k}", file=f)
    print(f"u = {eta.u}", file=f)
    print(f"u' = {u_approx}", file=f)
    print(f"tau = {eta.tau}", file=f)
    #print(f"r2 = {traj.r2}", file=f)
    print(f"Seff = {np.mean(traj.vec_Neff)}", file=f)
    print(f"nu = {sad.nu}", file=f)
    dom = glv.dominance.DominantComponent()
    dom.from_trajectory(traj)
    
    print(f"p_thresh={np.mean(dom.vec_pmin)}")
    # percent biomass
    def component_size(p):
        (T,S) = traj.mat_x.shape
        vec_n = np.zeros(T)
        for t in range(T):
            vec_p = np.sort(traj.mat_x[t,:] / np.sum(traj.mat_x[t,:]))[::-1]
            vec_n[t] = S - np.sum(np.cumsum(vec_p) >= p)
        return np.mean(vec_n)
    
    T = traj.mat_x.shape[0]
    vec_p = np.zeros(T)
    for t in range(T):
        vec_x = np.sort(traj.mat_x[t,:])[::-1]
        vec_p[t] = np.sum(vec_x[0:int(dom.vec_ndom[t])]) / np.sum(traj.mat_x[t,:])
    
    print(f"%dom = {np.mean(vec_p)}", file=f)
    print(f"n99% = {component_size(0.99)}", file=f)

gt.stop()

