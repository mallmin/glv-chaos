#!/bin/bash

#SBATCH -p gpu
#SBATCH -J lam=1e-24
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem-per-cpu 1000
#SBATCH -t 24:00:00
#SBATCH -o ./simcode/traj/log/%a.out
#SBATCH -e ./simcode/traj/log/%a.err

python3 ./simcode/traj/traj.py -S 500 --mu 0.5 --sig 0.3 --lam 1e-24 -T 100000 -o data/traj-lam-range/traj_lam=1e-24.pkl