'''
When I changed module and object names, my old stored trajectories cannot be loaded with 
the new code. This is to update the format of the stored trajectory to fit the new code
'''

import os, sys
sys.path.append('./')
sys.path.append('./figurecode/')

from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import argparse
import glv

tr = glv.Tracker().start("Converting")

parser = argparse.ArgumentParser()
parser.add_argument("--old", type=str)
parser.add_argument("--new", type=str)
args = parser.parse_args()

traj_old = glv.obs.Observable.load(args.old)

print(traj_old.scheme)

traj_new = glv.DisorderedLVSeries()

traj_new.__dict__.update(traj_old.__dict__)
traj_new.vec_K = np.ones(traj_new.S)
traj_new.vec_r = np.ones(traj_new.S)

traj_new.save(args.new)

tr.end()