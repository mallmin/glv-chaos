
'''
Script to calculate probability of chaos

Adapted for use on a SLURM-managed cluster.
'''

# Paths
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
sys.path.append(".")
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

import glv
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import default_rng
from myutils import Timer
import argparse
import pandas as pd


def parse_args():
    '''
    Parses the command line arguments.

    Returns
    -------
    args
        object containing arguments
    '''
    parser = argparse.ArgumentParser()

    # Model parameters
    parser.add_argument("-S", type = int, default = 500)
    parser.add_argument("--mu", type = np.float64, default = 0.5)
    parser.add_argument("--sig", type = np.float64, default = 0.3)
    parser.add_argument("--lam", type = np.float64, default = 10**-8)
    parser.add_argument("--gam", type = np.float64,  default = 0.0)

    # Random seeds
    # parser.add_argument("--alpha-seed", type=int, default=None)
    # parser.add_argument("--init-seed", type=int, default=None)

    # Simulation parameters
    parser.add_argument("-N", type = int, default = 3)
    parser.add_argument("--dt", type = np.float64, default = 0.01)
    parser.add_argument("--scheme-transient", type=str, default="log")
    parser.add_argument("--scheme", type=str, default="logistic")
    parser.add_argument("--steps-transient", type = int, default = 100*2000)
    parser.add_argument("--steps-lim-test", type = int, default = 100*1000)

    # Simulation flow
    parser.add_argument("--profile", action = "store_true")
    parser.add_argument("--plot", action = "store_true")

    parser.add_argument("--output-dir", type=str, default="./data/phase")

    # Simulation IDs
    parser.add_argument("--sim-id", type = int, default=None)
    parser.add_argument("--job-id", type = int, default=None)

    args = parser.parse_args()

    # Generate default IDs
    if args.sim_id is None:
        try:
            args.sim_id =  int(os.getenv('SLURM_ARRAY_TASK_ID'))
        except:
            args.sim_id = 0
    if args.job_id is None:
        try:
            args.job_id = int(os.getenv('SLURM_ARRAY_JOB_ID'))
        except:
            args.job_id = 0

    return args


gt = Timer("Script", absolute=True).start()

args = parse_args()

N = args.N
n_chaos = 0
n_cycle = 0
n_fp = 0
n_div = 0


for n in range(args.N):
    s = glv.sim_legacy.UnitGLVSystem(S=args.S, mu=args.mu, sig=args.sig, gam=args.gam, alpha_seed=None, scheme=args.scheme_transient)
    s.initialize_uniform(low=args.lam, high= 2 / args.S, seed = None)
    nondivergent = s.discard_transient(args.steps_transient)
    s.set_scheme(args.scheme)
    
    if not nondivergent:
        n_div += 1
    else:
        traj = glv.obs.AbundanceSeries()
        s.generate_observables(traj, args.steps_lim_test, record_interval=100)
        #lim_idx = glv.comp.limit_index(traj.mat_x)
        lim_idx = traj.lim_idx
        if lim_idx == -1:
            n_div += 1
        elif lim_idx == 0:
            n_fp += 1
        elif lim_idx == 1:
            n_chaos += 1
        elif lim_idx > 1:
            n_cycle += 1

csv_path = joinpath(args.output_dir, "csv", f"dat_{args.sim_id}.csv")

if os.path.isfile(csv_path): #update
    df = pd.read_csv(csv_path)
    N += df['N'].to_numpy()[0]
    n_div += df['n_div'].to_numpy()[0]
    n_fp += df['n_fp'].to_numpy()[0]
    n_chaos += df['n_chaos'].to_numpy()[0]
    n_cycle += df['n_cycle'].to_numpy()[0]

with open(csv_path,"w") as f:
    csv_header = ','.join(['mu','sig','N','n_div','n_fp','n_chaos','n_cycle'])  
    f.write(csv_header)
    f.write('\n')
    f.write(f"{s.mu},{s.sig},{N},{n_div},{n_fp},{n_chaos},{n_cycle}")
    

gt.stop()



