#!/bin/bash

#SBATCH -p amd  
#SBATCH -J rand
#SBATCH -N 1 
#SBATCH -n 1
#SBATCH --mem-per-cpu 350
#SBATCH -t 24:00:00
#SBATCH --array 1-21291
#SBATCH -o ./data/phase-rand/log/%a.log
#SBATCH -e ./data/phase-rand/err/%a.err

cd $HOME/glv-chaos/
python3 simcode/phase-rand/sim_rand.py `sed " $SLURM_ARRAY_TASK_ID!d" $HOME/glv-chaos/simcode/phase-rand/params_rand.txt`