import numpy as np
import os, sys
from os.path  import join as joinpath
import argparse
currentdir = os.path.dirname(os.path.realpath(__file__))


S = 500
lam = 1e-8

alpha_seed = 1234
init_seed = 2345

params_dir = currentdir
output_dir = joinpath('./data/phase-rand')


mu_first = - 0.1
mu_last = 1.4
delta_mu = 0.01

sig_first = 0
sig_last = 1.4
delta_sig = 0.01

sig_range = np.around(np.linspace(sig_first, sig_last, round((sig_last - sig_first)/delta_sig ) + 1), 4)
mu_range = np.around(np.linspace(mu_first, mu_last, round((mu_last - mu_first)/delta_mu ) + 1), 4)

N = 10

with open(joinpath(params_dir, f"params_rand.txt"),"w") as f:
    for sig in sig_range:
        for mu in mu_range:
            f.write(f"-S {S} --mu {mu} --sig {sig} --lam {lam} -N {N} --steps-transient 1000000 --steps-lim-test 100000 --output-dir {output_dir}\n")
