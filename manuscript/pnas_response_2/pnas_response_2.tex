\documentclass[]{monograph}
\usepackage{parskip}
\usepackage[normalem]{ulem}


\newcommand{\emil}[1]{{\color{WildStrawberry}{#1}}}
\newcommand{\response}[1]{{\color{NavyBlue}{#1}}}
\newcommand{\review}[1]{{\it #1}}

\addbibresource{../PNAS_revised_2/refs.bib}



\title{Response Letter for the second revision of ``Chaotic turnover of rare and abundant species in a strongly interacting model community''}

\author{Tracking nr 2023-12822}

\begin{document}
\maketitle
We thank the reviewers for evaluating our revised manuscript. We are pleased that they have appreciated our efforts to improve the framing of our results and agree that the manuscript has been substantially improved. 

In response to the invitation by Reviewer 2 to relate our work with publications by Barbara Drossel and co-workers, as represented by Refs.\ \cite{Allhoff2015,Rogge2019,Hamm2021}, we have added in the Discussion a paragraph with a perspective on evolution and compositional turnover, wherein these references (among several others added) fit. Rather than providing an extensive model-to-model comparison, we highlight a few conceptual points of interest. This allows us to respect the journal page limit without trimming away material that has already been vetted and approved by the reviewers. We provide more details in our below response to Reviewer 2.


We hope that the Reviewers and the Editor will agree that the amendments we have made reflect a good compromise in addressing the numerous implications and associations while orienting the discussion around our main themes of ecological chaos, rare biosphere patterns, and plankton ecology.

With best regards, 

Emil Mallmin, Arne Traulsen and Silvia De Monte

\section*{Report from Reviewer 1}
\textit{The authors have incorporated my suggestion into their manuscript and I have no further suggestions of substance to offer. Their additional ecology-minded revisions are also much appreciated and serve the paper well. I look forward to seeing the final product in print.}

\response{We are very pleased that Reviewer 1 made their support even stronger than after the first revision.}

\section*{Report from Reviewer 2}

\review{
I believe that the extensive revisions to the "framing" (Introduction and Discussion) will make the paper much more accessible and significant for empirically focused ecologists, and my positive evaluation of the substantive content in my original review (which I will not repeat here) has not changed.
}

\response{We thank the Reviewer for the overall positive assessment of the current version of the paper, that adds to the flattering evaluation on the substance of our work from the previous review round.}

\review{
I am not in the camp that believes papers should be fine-tuned to supposed optimality before they are published, so I will make only one comment for the authors to address (if the Editor agrees that this ought to happen). Having now a better idea of the biological issues that the paper aims to address, it seems to me that there should be recognition and discussion of the parallel literature on "evolving food webs" where species richness is replenished through mutation/speciation rather than (or in addition to) immigration, and in many cases feeding relationships are determined by trait values (the large eating the small, for example) rather than by from-above phenomenological interaction coefficients. I can't claim to know this literature well -- my highly selective knowledge is driven by who makes a habit of posting new manuscripts to the parts of the arXiv I subscribe to. Within my limited awareness, the key group seems to be Barbara Drossel and her evolving circle of collaborators, and here are a few of the most relevant recent papers: 
 
Michaela Hamm, Barbara Drossel. 2021. The concerted emergence of well-known spatial and temporal ecological patterns in an evolutionary food web model in space. Scientific Reports 11, 4632. 
 
T Rogge, D Jones, B Drossel, KT Allhoff. 2019. Interplay of spatial dynamics and local adaptation shapes species lifetime distributions and species-area relationships Theoretical Ecology 12, 437-451. 
 
KT Allhoff, D Ritterskamp, BC Rall, B Drossel, C Guill. 2015. Evolutionary food web model based on body masses gives realistic networks with permanent species turnover. Scientific reports 5, 10955 
 
There are quite a few more, starting in the early 2000s, but these three recent papers seem like the ones most closely related to the present manuscript. 
 
I think that Mallmin et al. should discuss how their paper relates to this parallel literature, at two levels: how do the models relate to each other in terms of assumptions and dynamics, and how do the community-level predictions relate? Are these two different modeling frameworks able to explain the same phenomena (at the same level of detail and precision), or are there differences in what they predict and how well those predictions align with reality? Are there opportunities for making further progress by bringing together these two separate lineages of theory?

}

\response{The Reviewer points us to a series of works that we had indeed overlooked by focusing on classes of simpler and analytically tractable models and on ecological time scales. There is certainly a considerable literature on eco-evolutionary models, and the question of how our work is connected to this literature is interesting. We have therefore added the following paragraph (line 1076): 

``Besides explicit incorporation of structured interactions, an extension of our model with particular biological relevance would be to allow interactions to evolve, notably as they are reshaped by the appearance of novel species---a different scenario than our immigration term captures. Persistent turnover can then manifest on long timescales even if the ecological dynamics is---contrary to our case---at equilibrium. Such turnover has been shown in numerical models of evolving food webs structured by body size \cite{Allhoff2015,Hamm2021} and when adaptive dynamics occurs in high-dimensional trait spaces \cite{Doebeli2014}. An open question is what evolutionary process may produce interactions that underpin chaotic turnover on ecological timescales. The observation that evolution sustains higher diversity under boom-bust than equilibrium ecology \cite{Doebeli2021}, together with the propensity of diversity to cause instability, suggests a possible role for eco-evolutionary feedbacks.''

We have refrained from including a detailed model-to-model comparison in the manuscript, e.g. on the success in predicting different community patterns, but give here the following remarks about certain key differences: Beside \cite{Allhoff2015,Rogge2019,Hamm2021} focussing on evolutionary instead of ecological dynamics and timescales, in \cite{Hamm2021}, for instance, there is only a factor 2.3 difference in abundance between the most and least abundant species, at odds with the empirical `rare biosphere' pattern that we specifically mean to capture. The authors argue that this is due to the low dimension of the underlying trait space based on body size as a `master trait'. The food web model captures instead several other empirical findings, such as species life time distribution, that we do not consider, in part because we have eschewed explicit spatial structure to focus on within-patch ecological dynamics.

We have also concentrated several statements about the ecological interpretation of the disordered approach already present in the previous manuscript version into its own paragraph (line 1015):

``While the assumption of disordered interactions may appear \textit{ad hoc}, predictions for the onset of instability by the dLV model qualitatively match experiments in synthetic bacterial communities \cite{Hu2022}. In a plankton context, we take the dLV to be a minimal yet relevant phenomenological representation of the relationships between species (or `operational taxonomic units' from sequencing)  of marine protists of a similar size class: the protistan interactome is largely uncharted \cite{Bjorbaekmo2019}, the ubiquity of mixoplankton blurs consumer--resource distinctions \cite{Millette2023}, and the effects of a diversity of zooplankton and viruses can manifest as apparent competition between species. ''

We believe this serves to justify why, apart from parsimony, the disordered approach may be more suitable than morphological traits for the types of communities we are interested in, in particular plankton.

For a better segue of topics, we have then swapped the order of the following paragraphs, such that the comparison with empirical SADs in plankton comes before the paragraph on plankton chaos at different levels of aggregation.

A few wordings were changed throughout the manuscript to try and shorten the text to create space for the new paragraph on evolution and turnover (see the version `diff' file.)

We hope that the Reviewer is satisfied with our efforts to address their questions.
}





\printbibliography
\end{document}