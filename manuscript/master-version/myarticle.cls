\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{myarticle}[2023/07/18 MyArticle LaTeX class]

% Pass any unspecified options to article class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass[]{article}

\RequirePackage{stix2}
\RequirePackage[colorlinks,linkcolor=blue,citecolor=blue]{hyperref}
\RequirePackage[margin=16mm, top=12mm,includehead,headheight=8mm]{geometry}
\RequirePackage{authblk}

\newcommand{\affiliation}[2][]{\affil[#1]{{\small #2}}}
\newcommand{\myrule}{\par\noindent\rule{\textwidth}{0.4pt}}



\renewcommand{\maketitle}{%
    \thispagestyle{empty}	
	\vspace{-2cm}
	{\noindent\bfseries\large \@title}
	\myrule
	\vspace{2mm}	
	\@author

    \vspace{2mm}
	\noindent\@date	
    \myrule
	\vspace{10mm}
}