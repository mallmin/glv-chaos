\documentclass[11pt]{article}

\usepackage{amsmath,amssymb}
\usepackage{enumerate}
\usepackage{titlesec}
\usepackage[margin=16mm, top=12mm]{geometry}
\usepackage[colorlinks,linkcolor=blue,citecolor=blue]{hyperref}
\usepackage{cleveref}% Load AFTER hyperref!
\usepackage{appendix}
\usepackage{mathrsfs}  
\usepackage{graphicx}
\usepackage{placeins}
%\usepackage{subfig}
\usepackage[labelfont=bf]{caption}
\usepackage[dvipsnames]{xcolor}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{datetime}
\newdateformat{stddate}{\monthname[\THEMONTH] \THEDAY, \THEYEAR}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	FOR REFERENCING THE MAIN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Referencing supplementary material
\usepackage{xr}

\externaldocument[main:]{./chaotic_glv_draft}
\newcommand{\mref}[1]{\begin{NoHyper}\ref{main:#1}\end{NoHyper}}
\newcommand{\maineqref}[1]{Main Article Eq.~\mref{#1}}
\newcommand{\mainfigref}[1]{Main Article Figure \mref{#1}}
\newcommand{\main}{Main Article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	STYLING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\captionsetup[figure]{format=hang}

\usepackage[T1]{fontenc}
\usepackage{stix2}

\usepackage[format=plain]{caption}


\renewcommand{\vec}[1]{\boldsymbol{#1}}
\newcommand{\vx}{\vec{x}}
\newcommand{\vJ}{\vec{J}}
\newcommand{\del}{\partial}
\newcommand{\ddel}[2]{\frac{\del#1}{\del#2}}
\newcommand{\avg}[1]{\langle #1 \rangle}
\newcommand{\aavg}[1]{\llangle #1 \rrangle}
\newcommand{\Var}[1]{\text{Var}[#1]}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\deriv}[1]{\frac{\diff{}}{\diff{#1}}}
\newcommand{\dd}[2]{\frac{\diff{#1}}{\diff{#2}}}
\newcommand{\std}[1]{\text{std}(#1)}

\newcommand{\E}[1]{\text{E}\left[#1\right]}

\newcommand{\red}[1]{{\color{red}#1}}
\newenvironment{pink}{\color{WildStrawberry}}{}
\newcommand{\inpink}[1]{{\color{WildStrawberry}#1}}
%\titleformat*{\section}{\Large\bfseries\sffamily}
%\titleformat*{\subsection}{\large\bfseries\sffamily}
%\titleformat*{\subsubsection}{\large\sffamily}

\titleformat*{\section}{\bfseries} %\centering
\titleformat*{\subsection}{\small\bfseries\sffamily}

\newcommand{\myrule}{\par\noindent\rule{\textwidth}{0.4pt}}

\newcommand{\mycaption}[2]{\caption[#1]{\textbf{#1.} #2}}
\renewcommand\thefigure{S\arabic{figure}}
\renewcommand{\theequation}{S\arabic{equation}}
\renewcommand{\thesection}{S\arabic{section}}

% Abbreviations
\newcommand{\ie}{\emph{i.e.}}
\newcommand{\eg}{\emph{e.g.}}
\newcommand{\vs}{\emph{vs.}}
\newcommand{\etc}{\emph{etc.}}




\begin{document}
%	\begin{center}
%		{\large Supplementary material for}\vspace{2mm}
%		
%	{\bfseries\large Chaotic turnover of dominant species in a diverse strongly interacting community}		
%	\end{center}

\noindent{\bfseries\large Supplementary material}\vspace{2mm}

\noindent{\large Chaotic turnover of dominant species in a diverse strongly interacting community}
\myrule

\noindent Emil Mallmin\textsuperscript{1}$^*$, Arne Traulsen\textsuperscript{1}, Silvia De Monte\textsuperscript{1,2} \hfill Draft: \stddate\today
\vspace{3mm}

\noindent{\small\textsuperscript{1}Max Planck Institute for Evolutionary Biology, Plön, Germany
	
	\noindent\textsuperscript{2}Institut de Biologie de l'ENS (IBENS), D\'epartement de Biologie,\\\phantom{\textsuperscript{2}}Ecole normale sup\'erieure, CNRS, INSERM, Universit\'e PSL, 75005 Paris, France\\
	$^*$\texttt{mallmin@evolbio.mpg.de}	
}			
\myrule



\tableofcontents
\listoffigures



\myrule
	
\section{Supplementary Figures}

As in the main text, unless otherwise stated, reference parameters in simulations are $S=500, \mu=0.5, \sigma=0.3, \lambda=10^{-8}$.

\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-sensitivity/figS_sensitivity.png}
	\caption[Sensitive dependence on model parameters]{\textbf{Sensitive dependence on model parameters}. \textbf{A} Stacked abundance plot for the reference simulation, similar to \mainfigref{fig:turnover}A. In below panel we have subtly modified the simulation by \textbf{B} choosing a different integration scheme; \textbf{C} or adding a random $O(10^{-6})$ perturbation to the interaction coefficients; \textbf{D} or an $O(10^{-8})$ perturbation to the initial condition. \textbf{E} In each case, the Bray-Curtis similarity to the reference simulation deviates significantly from $1$ on a horizon of $\sim 200$ time units, and soon approaches zero as the respective dominant communities cease to overlap (bottom). } 
	\label{fig:sens}
\end{figure}


\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-lyapunov/figS_lyapunov.png}
	\caption[Convergence to positive maximum Lyapunov exponent (MLE)]{\textbf{Convergence to positive maximum Lyapunov exponent (MLE)}. \textbf{A} We calculate the maximal finite-time Lyapunov exponent (FTLE) over a just a few integration time steps ($n=2$) along a trajectory. \textbf{B} The cumulative average of the FTLE converges towards a limit that is the maximal Lyapunov exponent of $0.02$. Because it is decidedly positive, the trajectory is chaotic.} 
	\label{fig:lya}
\end{figure}


\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-tempsim/figS_tempsim.png}
	\mycaption{Decay of community similarity with time}{\textbf{A} The temporal similarity matrix $\mathcal{T}$ has elements given by the Bray-Curtis similarity between the abundance vectors any two time points, $\mathcal{T}(t,t') = \text{BC}(\vec{x}(t), \vec{x}(t'))$. Because only the diagonal is far from zero, and the similarity index is mostly determined by the overlap of dominant species, we conclude that the dominant component is not closely repeated (unless, perhaps, after an exceedingly long time). The aberration around $t\approx t' \approx 8000$ reflect a time when a dominant community took particularly long to displace by an invader, which from the point of view of the deformed heteroclinic dynamics laid out in the \main. \textbf{B} For a few well-separated time points $t$ (one graph each), we show how $\mathcal{T{t,t'}}$ BC decays over time $t'$ on a timescale of 200 time units (top panel), and how it fluctuates over a longer time scale of 5000. } 
	\label{fig:tempsim}
\end{figure}

 
\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-effsize/figS_effsize.png}
	\mycaption{Scaling of effective community size with richness}{The time-average of the effective community size, $\overline{S}_\text{eff}$, (\maineqref{eq:Neff}) increases slowly (but super-logarithmically) with the overall richness $S$. That is, even if we add a \textit{thousands} of new species to the community, the dominant component would just have a species or two more at a time than before.} 
	\label{fig:Seff}
\end{figure}


\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-robustness/figS_robustness.png}
	\mycaption{Robustness of turnover dynamics under model variations}{ We leave a systematic investigation of the chaotic dynamics of disordered Lotka-Volterra beyond the simplifying assumption we have made for future research. However, here we illustrate that our assumptions are not necessary for the chaotic dynamics.  \textbf{A} Non-uniform growth rates: we sample $r_i \sim U(0,1)$. \textbf{B} Non-uniform carrying capacities: $K_i \sim \text{LogNorm}(0,0.1)$. \textbf{C} Sparse interaction: each interaction has a $0.1$ chance to be non-zero. \textbf{D} Symmetric bias: $\gamma=0.2$ correlation between diagonally opposed interaction coefficients \textbf{E} Predator-prey bias: $\gamma=-0.3$ correlation between diagonally opposed interaction coefficients. }
	\label{fig:robustness}
\end{figure}

%
%\begin{itemize}
%	\item $\gamma = -0.8$
%	\item $\gamma=0.8$
%	\item $r_i \sim U[0,1]$
%	\item $\alpha_{ij} \sim U(0,1)$
%	\item Carrying capacities $U(0,1)$
%	\item sparse 
%\end{itemize}



\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-corr/figS_corr.png}
	\mycaption{Pairwise correlations in species abundances}{For each of $(S-1)^2$ inter-species interactions, we have plotted the correlation coefficient $C_{ij} = \phi_{ij}(0)$ against the rescaled interaction coefficient $z_{ij} = (\alpha_{ii} - \mu)/\sigma$ (grey). ($\phi_{ij}$ is the cross-correlation function.) Then for each species, we have highlighted its maximum correlation in blue, and its maximum anti-correlation in pink; as well as, in a lighter hue of blue (pink), the respective maximum (minimum) of $\phi_{ij}(t)$ over the lag-time $t$ in a window $200$ time units. The inset shows that the vast majority of species pairs have a very low level of correlation that may be spurious. Nonetheless, the fact that high pairwise correlations are associated with a more-facilitative interaction ($z_{ij}$ < 0) and higher anti-correlation with more-detrimental interaction ($z_{ij}$ > 0), points toward these low numbers still being meaningful. }\label{fig:corr}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-nuscaling/figS_nuscaling.png}
	\mycaption{Scaling of AFD power-law exponent with $S$ and $\lambda$}{From simulation we have extracted the slope of the power-law section fo the abundance fluctuation distribution (AFD) \textbf{A} For varying $S$, we find empirically $\nu = \nu_0 + c \log S$, where $\nu_0 = \nu_0(\mu,\sigma,\lambda), c = c(\mu,\sigma,\lambda)$. \textbf{B} For varying $\lambda$, it appears $\nu = 1 - d / \log \lambda$, where $d = d(\mu,\sigma,S)$. The values of $\lambda$ are 10 to the power of negative 8, 12, 16, 20, 24, 28, 32, and 128 in order to extrapolate towards zero immigration. The dashed line connects $\nu=1$ with the value at $\lambda=10^{-8}$.}
	\label{fig:nuscaling}
\end{figure}


%\section{Adiabatic phase diagram}\label{sec:adiab}

\begin{figure}
	\includegraphics[]{../../figurecode/figS-adiabatic/figS_adiabatic.png}
	\mycaption{Phase diagram form adiabatic simulations}{  \textbf{A} We draw a single realization $\zeta$ of a standard Gaussian random matrix, which is then continuously rescaled into the interaction matrix with elements $\alpha_{ij}(\mu,\sigma) = \mu + \sigma \zeta_{ij}$. We run a simulation starting starting with $\mu=\mu_0$, let the dynamics converge toward an attractor (\eg\ fixed point), then change $\mu$ by some small $\delta \mu$, and let the simulation converge again from its previous state, and so on, to obtain a sequence of attractors visited as $\mu$ is varied. Varying $\mu$ `adiabatically' in this way, \ie\ slowly and continuously, allows the system to track an attractors as it moves with $\mu$ in the phase space, until there is a bifurcation. Here we have followed this procedure for each line of $\mu$ from 1.4 $\to -0.1$, for each $\sigma$ separately. The colour quality reflects the class of the attractor, and the colour gradation indicated the effective community size. This rendering reveals that in the multiple-attractor phase, one finds clear lines radiating from $(\mu,\sigma) = (1,0)$ that delineate sectors characterized by the number of high-abundance species coexisting at a fixed-point. (\autoref{sec:invasion} explains an attempt to derive this situation by a statistical invasion analysis.) In a non-adiabatic simulation approach, this structure is not apparent because one doe not reliably find fixed point, but just as well chaos or cycles. \textbf{B} Stacked abundances of the attractor found in an adiabatic sequence $\mu = 1.4 \to 0.6 \to 1.4$ (top panel, right to left; then bottom panel, left to right) at $\sigma=0.3$. One can see sudden jumps to new equilibria involving one more (or less) species. For low $\mu$ there is chaos, so the abundances observed at the attractor depend on the moment of record. The jump to chaos occurs suddenly in a transcritical bifurcation: by studying the transition at arbitrarily fine $\Delta\mu$ resolution, one find sudden loss of stability of the (not plotted at this resolution here). Characteristic of this transition, there is hysteresis, meaning that the value $\mu_c$ of the transition depends on the direction of approach. Curiously, it seem that a fixed point involving mostly the same species is recovered when the chaotic phase is exited; but we leave systematic investigation of the properties of the multiple-attractor phase for future work.} %\inpink{[update so that data comes from exactly same and seed as above]} } 
	\label{fig:adiab}
\end{figure}

%
%\begin{itemize}
%	\item Adiabatic approach, replicate of $(S=500,\lambda=10^-8)$ with different seed; $(S=500,\lambda=10^-20)$; $(S=1000,\lambda=10^-8)$; $(S=500,\lambda=10^-8, \gamma=\pm 0.8)$  
%\end{itemize}

\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-phasevar/figS_phasevar.png}
	\mycaption{Variation of community-level observables across phase diagram}{Here we show the time-averaged value of the community-level observables that enter \maineqref{eq:Xbar} (top row) and the relative fluctuations in the respective quantities (bottom row). The data comes from the adiabatic simulation detailed in the caption to \autoref{fig:adiab}. An arrow on the end of the colour bar implies the range has been capped for clarity.}
	\label{fig:commobs}
\end{figure}


\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-kuscaling/figS_kuscaling.png}
	\mycaption{For Dependence of $u, k$ on $S,\lambda$}{The empirical, approximate relationship $u \propto k$, found across the range of $\mu,\sigma$ in the chaotic phase,  has a proportionality constant that depends only relatively weakly on $S$ and $\lambda$.}
	\label{fig:kuscaling}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-rhoc/figS_rhoc.png}
	\mycaption{Collective correlation}{\textbf{A} The collective correlation in the chaotic phase. Within the bounds to the chaotic phase indicated in \autoref{fig:adiab}A, we run a long simulation for each parameter point with random initial condition and interaction matrix realization. Statistics have been recorded for persistently chaotic trajectories; non-chaotic trajectories were discarded, and the parameter point rerun to obtain a long chaotic trajectory, up to five times, else the point was omitted (chaos probability was shown in \mainfigref{fig:chpr}). \textbf{B} The critical value of the collective correlation as defined by \maineqref{eq:rho_c}. \textbf{C} The ratio $\overline{\rho}/\rho_c$ tends towards 1 at the boundary to the equilibrium phase. Note that $\overline{\rho}$ changes continuously across this boundary---see \autoref{fig:commobs}C. An arrow on the end of the colour bar implies the range has been capped for clarity.}
	\label{fig:critcorr}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-nuphase/figS_nuphase.png}
	\mycaption{Power-law exponent $\nu$ across chaotic phase}{\textbf{A} Variation of the AFD power-law exponent across the chaotic phase. \textbf{B} The relative error in the prediction of the deviation of the exponent from $1$, between the stochastic focal-species model and the full disordered Lotka-Volterra simulations. Data from the simulations described in \autoref{fig:critcorr}. An arrow on the end of the colour bar implies the range has been capped for clarity.}
	\label{fig:nuphase}
\end{figure}


\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-taux/figS_taux.png}
	\mycaption{Comparison of autocorrelation times}{We compare the autocorrelation time of the abundance vector $\vec{x}$ ($\tau_{\vec{x}}$) and the effective noise $\eta$ ($\tau_\eta$); these are the parameters of the exponential fit $e^{-t/\tau}$ applied to the respective autocorrelation functions. Across the chaotic phase, these to timescale are quantitatively close, for reasons explained in Main Article \mref{app:noise}. }
	\label{fig:taux}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[]{../../figurecode/figS-Xerr/figS_Xerr.png}
	\mycaption{Error in approximate formula for $\overline{X}$}{To assess the accuracy of \maineqref{eq:Xbar}, we have compared the relative difference in $\overline{X}$ from simulations (the adiabatic data; \autoref{fig:adiab}) with the output of the formula given the simulated values of $\overline{S}_\text{eff}$ and $\overline{\rho}$. The formula is generally accurate across all phases avoiding proximity to divergence. In particular, in regions of lower fluctuations (compare \autoref{fig:commobs}), especially at fixed points, it becomes near-exact ($<1\%$ error). An arrow on the end of the colour bar implies the range has been capped for clarity.}
	\label{fig:Xerr}
\end{figure}

\FloatBarrier

\section{Effective integration schemes for LV equations}\label{sec:scheme}

We consider the numerical integration of
\begin{equation}\label{eq:dotxi}
	\dot{x}_i(t) = r_i x_i(t) (g_i(t) - x_i(t))  ,\quad g_i(t) = 1 - \sum_{j(\neq i)}{ \alpha_{ij} x_j(t)}.
\end{equation}
The main concern is that abundances may become exponentially small, but eventually recover. If, by numerical error, abundances become zero or negative, that may cause a problem over long integration times. It is therefore advisable to use an integration scheme that guarantees the positivity of abundances. One approach is based on the exact identity
\begin{equation}
	x_i(t+ \Delta t ) = x_i(t) \exp\left( \int_{t}^{t+\Delta t} (g_i(t') - x_i(t')) dt'\right).
\end{equation}
Approximating the integral , and writing $x_i[n] = x_i(n\Delta t)$, \etc,
%\begin{equation}
%	x(t+ \Delta t ) \approx x(t) \exp\left( r(g(t) - x(t)) \Delta t\right)
%\end{equation}
\begin{equation}
	x_i[n + 1] = x_i[n] \exp\left( r_i(g_i[n] - x_i[n]) \Delta t\right),\quad g_i[n] = 1 -  \sum_{j(\neq i)}{ \alpha_{ij} x_j[n]}.
\end{equation}
Thanks to the positivity of the exponential, abundances can never become negative. Defining $\tilde{r}_i = \Delta t r_i$ and $\tilde{\alpha}_{ij} = \tilde{r}_i \alpha_{ij}$ ($i\neq j$), $\tilde{\alpha}_{ii} = \tilde{r}_i$, 
\begin{equation}
	x_i[n + 1] = x_i[n] \exp\left( \tilde{r}_i - \sum_{j} \tilde{\alpha}_{ij} x_j[n] \right).
\end{equation}
This \textit{logarithmic scheme} is equivalent to using forward-Euler to simulate the log-abundances. With $y_i[n] = \ln x_i[n]$:
\begin{equation}
	y_i[n+1] = y_i[n] +  \tilde{r}_i - \sum_{j} \tilde{\alpha}_{ij} \exp(y_j[n]).
\end{equation}
Another approach is based on the formal solution
\begin{equation}\label{eq:logistic-sol}
	x_i(t + \Delta t) = x_i(t) \times \frac{G_i(t+\Delta t | t)}{1 + r_i x_i(t) \int_{t}^{t + \Delta t} G_i(t'|t) dt'},\quad G_i(t'|t) = e^{r_i \int_{t}^{t'} g_i(s) ds }
\end{equation}
Approximating $G_i(t'|t) \approx \exp( r_i g_i(t) (t' - t))$ for $t' -t$ small, then performing the integral in the denominator of \eqref{eq:logistic-sol}, we obtain 
\begin{equation}
	x_i(t+\Delta t) \approx  \frac{g_i(t) x_i(t)}{ g_i(t) e^{- r_i g_i(t) \Delta t} + x_i(t) ( 1 - e^{- r_i g_i(t) \Delta t})},
\end{equation}
which is always positive: the denominator is negative iff $g<0$, in which case the numerator is too. Rearranging to minimize the number of arithmetic operations involved, we obtain the \textit{logistic scheme}
\begin{equation}
	x_i[n + 1] =  g_i[n] \times\frac{ x_i[n]}{ x_i[n] + (g_i[n] - x_i[n]) e^{- r_i g_i[n] \Delta t}},\quad g_i[n] = 1 -  \sum_{j(\neq i)}{ \alpha_{ij} x_j[n]}.
\end{equation}
To our knowledge, this scheme was first proposed by Jules Fraboul (personal communication).

To either scheme we can add a term $+ \lambda \Delta t$ for the immigration.



\section{Simplification of the disordered Lotka-Volterra model with mixing}\label{mix}
We consider an always well-mixed volume $V$ containing $S$ species with instantaneous absolute abundance $N_i(t)$, and constant nominal carrying capacities $K_i$ and growth rates $R_i$. The growth dynamics follows the standard Lotka-Volterra form. We add the effect of mixing with an external environment containing the same set of species but at abundances $N_i^\text{ext}(t)$: a fraction $\Lambda$ per unit time of the volume $V$ is exchanged with an equal volume from the external environment that gets instantaneously mixed in with the volume $V$. In total, the dynamics is
\begin{equation}\label{eq:gLV-2}
	\dot{N}_i(t) = R_i N_i(t) \left( 1 - \frac{N_i(t) + \sum_{j(\neq i)} \beta_{ij} N_j(t)}{K_i}   \right) + \Lambda (N^{\text{ext}}_i(t) -  N_i(t)) .
\end{equation}
Note that if we let $\Lambda \gg \max_i R_i$, we will force $N_i(t) \approx N_i^\text{ext}(t)$. Instead, we consider the slow-mixing scenario $\Lambda \ll \min R_i$, since our purpose in adding mixing is mainly to prevent extinction of rare species. We introduce rescaled parameters
\begin{align}
	r_i &= R_i - \Lambda \approx R_i, \\
	K_i' &= K_i \left(1 - \frac{\Lambda}{r_i} \right) \approx K_i, \\
	\alpha_{ij} &= \frac{\beta_{ij} K_j'}{K_i'}, \\
	\lambda_i(t) &= \Lambda \frac{N_i^{\text{ext}}(t) }{ K_i'}, \\
	x_i(t) &= \frac{N_i(t)}{K_i'},
\end{align}
so that
\begin{equation}\label{eq:gLV-simple}
	\dot{x}_i(t) = r_i x_i(t) \left( 1 - x_i(t) + \sum_{j(\neq i)} \alpha_{ij} x_j(t)   \right) + \lambda_i(t). 
\end{equation}
Since the mixing occurs slowly, we suppose that it is justified to replace $ N_i^\text{ext}$ with its time-average $\overline{N}_i^\text{ext}$. A parsimonious distribution for these abundances is that they result from effectively independent species (in particular in the chaotic phase; see main text) constrained by a roughly constant total community biomass $N^*$ independent of the number of species:
\begin{equation}
	\overline{N}_i^\text{ext} = N^* \frac{K_i}{\sum_j K_j}.
\end{equation}
Introducing $\tilde{\lambda} = \Lambda N^* / K^*$ with $K^* = S^{-1}\sum_j K_j$ we have
\begin{equation}
	\lambda_i(t) \equiv \lambda = \frac{\tilde{\lambda}}{S}.
\end{equation}
Upon the simplifying assumptions $r_i \equiv 1$ and Gaussian distribution of $\alpha_{ij}$ (resulting from a combination of some distribution of $K_i$ and $\beta_{ij}$; see \cite{Barbier2017x}), we obtain the model of the main article.


\section{Invasion analysis of few-species equilibria}\label{sec:invasion}
Let $\mathcal{R}(t), \mathcal{D}(t)$ be the sets of rare and dominant species. For a rare species $i\in \mathcal{R}(t)$ that successfully invades, while $\lambda \ll x_i \ll 1$, it's dynamics is 
\begin{equation}
	\dot{x}_i \approx x_i\left( 1 - \sum_{j \in\mathcal{D}} \alpha_{ij} x_j \right) := \gamma_i x_i.
\end{equation} 
The growth rate $\gamma_i$ must in this time interval be (mostly) positive. Thus, no rare species is expected to be able to invade the dominant component while
\begin{equation}\label{eq:ninv}
	\gamma_\text{max} :=	\max_{i\in \mathcal{R}} \gamma_i < 0.
\end{equation}

Suppose then that the $|\mathcal{D}| \approx \overline{S}_\text{eff}$ dominant species are in a few-species equilibrium that would be stable but for invasion (potentially). We approximate the abundances of dominant species as equal (we expect them at least to be of the same order) and recall that they make up the overwhelming share of the total abundance. Thus, $x_j \approx \overline{X} / \overline{S}_\text{eff}$ for $j\in\mathcal{D}$. We suppose further that $\{ \alpha_{ij} \}_{j\in\mathcal{D}}$ for a random $i\in\mathcal{R}$ can be treated as independent. Then the sum  $\sum_{j\in\mathcal{D}} \alpha_{ij} \sim \mathcal{N}(\overline{S}_\text{eff}\mu, \overline{S}_\text{eff}\sigma^2)$. It follows that, for random $i\in \mathcal{R}$,
\begin{equation}
	\gamma_i\sim \mathcal{N}\left(1 - \mu\overline{X},\ \frac{\sigma^2 \overline{X}^2}{\overline{S}_\text{eff}} \right).
\end{equation}
Using extreme value theory \cite{Vivo2015}, and the fact that $|\mathcal{R}|\approx S$, 
\begin{equation}
	\gamma_\text{max} \approx 1- \overline{X}\left( \mu  - \frac{\sigma}{\sqrt{\overline{S}_\text{eff}}} h(S) \right),
\end{equation}
where $h(S)$ is a random variable\footnote{The maximum $M$ of $N$ i.i.d.\ standard Gaussian RVs tends, as $N\to\infty$, to $M = a_N + \xi / a_N$, where $a_N = \sqrt{2 \ln N - \ln (4\pi \ln N)}$ and $\xi$ follows a standard Gumbel distribution, whose mean is the Euler-Mascheroni constant ($\approx 0.577$) and variance is $\pi^2/6$.} that scales approximately as $\sqrt{\ln S}$. In particular, $h(500)\approx 3.04 \pm 0.45$ and $h(10'000) \approx 3.85\pm 0.35$. Using \maineqref{eq:Xbar},
\begin{equation}
	\gamma_\text{max} \approx   \frac{1 - \mu - (\overline{S}_\text{eff} \overline{\rho} + \sqrt{\overline{S}_\text{eff}} h(S))\sigma}{1 + (\overline{S}_\text{eff}-1)\mu - \overline{S}_\text{eff} \sigma \overline{\rho}}.
\end{equation}
Condition \eqref{eq:ninv} for non-invadability then amounts to
\begin{equation}
	\sigma < \frac{\mu - 1}{\overline{S}_\text{eff}\overline{\rho} + \sqrt{\overline{S}_\text{eff}}h(S) }.
\end{equation}
This predict lines radiating from $(\mu,\sigma)=(1,0)$. However, with $\overline{\rho} \sim (\overline{S}_\text{eff})^{-1/2}$, the slope of the lines are less steep for sectors with more species in the equilibrium, contrary to observation.


\section{Solution of intermittency model under unified coloured noise approximation}\label{app:ucna}
The unified coloured noise approximation was put forth by Jung \& H\"anggi \cite{Jung1987} (see also Fox \cite{Fox1986a,Fox1986b}) to solve SDEs of the form
\begin{align}
	\dot{x}(t) &= F(x(t)) + G(x(t)) \eta(t), \label{eq:x-dot}
\end{align}
with $\eta(t)$ as a coloured Gaussian noise: $\avg{\eta}=0$, $\avg{\eta(t)\eta(t')} = \exp(-|t-t'|/\tau)$. By differentiating \eqref{eq:x-dot}, rearranging terms, and rescaling time into  $\hat{t} = \tau^{-1/2} t $ (with $\hat{x}(\hat{t}) = x(\tau^{1/2}\hat{t})$) one obtains the exact equation
\begin{equation}\label{eq:pre-adiab}
	\dd{^2\hat{x}}{\hat{t}^2}+ \frac{G'}{G} \left(\dd{\hat{x}}{\hat{t}} \right)^2 + H_\tau \dd{\hat{x}}{\hat{t}} = F + \sqrt{2}  \tau^{1/4} G \hat{\xi},
\end{equation}
where $\hat{\xi}$ is a standard Gaussian white noise and 
\begin{equation}
	H_\tau(x) =  \tau^{-1/2} - \tau^{1/2} \left( F'(x) - \frac{G'(x)}{G(x)} F(x) \right).
\end{equation}
As either $\tau \to \infty$ or $\tau  \to 0$ one finds $H_\tau \to \infty$, and then \eqref{eq:pre-adiab} can be replaced by the overdamped limit
\begin{equation}\label{eq:post-adiab}
	\dd{\hat{x}}{\hat{t}} = \frac{F}{H_\tau} + \sqrt{2} \tau^{1/4}\frac{G}{H_\tau} \hat{\xi}.
\end{equation}
It is hoped that this approximation is accurate for intermediate $\tau$ as well.

The overdamped equation \eqref{eq:post-adiab} can be solved for its steady state $P^*(x)$ by conventional techniques [Gardiner], e.g.\ under Stratonovich interpretation of the noise: the associated stationary probability current is then
\begin{equation}
	J^* = \frac{F}{H_\tau}P^* - \tau^{1/2} \frac{G}{H_\tau} \deriv{x} \frac{G}{H_\tau}  P^*.
\end{equation}
In one dimension, $J^*(x)$ must be constant. Since $x$ is a non-negative abundance in our case, we must impose a boundary at $x=0$ through which probability cannot flow. Therefore $J^* \equiv 0$. The solution for $P^*$ is then
\begin{equation}\label{eq:P*-app}
	P^*(x) \propto \exp \left\{ \int^x v(x') \diff{x'}\right\},	
\end{equation}
with
\begin{equation}
	v = (\tau^{-1/2} H_\tau)\frac{F}{G^2} + \left( \ln \frac{H_\tau}{G}\right)'.
\end{equation}

In the stochastic intermittency model, we have 
\begin{align}
	F(x) &= -x(k+x) + \lambda,\\
	G(x) &= ux,\\
	H_\tau(x) &= \tau^{-1/2} + \tau^{1/2}(x + \lambda x^{-1}).
\end{align}
With these functions, the integral in \eqref{eq:P*-app} can be performed exactly, yielding the result of the main article:
\begin{equation}\label{eq:x-tilde-P*}
	P^*(x) = \frac{1}{\mathscr{N}} e^{- [q_+(x) + q_-(\lambda/x)] } x^{-\nu}  \left( \tau^{-1} + x +  \frac{\lambda}{x}\right),
\end{equation}
\begin{equation}
	q_\pm(y) = \frac{\left(  y + (\tau^{-1} \pm k) \right)^2}{2 u^2},
\end{equation}
\begin{equation}\label{eq:nu-formula}
	\nu = 1 + \frac{k}{\tau u^2}.
\end{equation}
The normalization constant $\mathscr{N}$, however, we do not have in closed form; instead we calculate it numerically, after the change-of-variables $y = \ln x$, as 
\begin{equation}
	\mathscr{N} = \int_{-\infty}^\infty \diff{y}\,e^{y(1 - \nu) -  q_+(e^y) - q_-(\lambda e^{-y})  }  \left( \tau^{-1} + e^y +  \lambda e^{-y}\right). 
\end{equation}



\bibliographystyle{citestyle}	
\bibliography{refs_emil}
\addcontentsline{toc}{section}{\refname}

\end{document}