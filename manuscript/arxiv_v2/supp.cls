\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{supp}[2023/07/18 Supplementary Materials LaTeX class]

% -------  Inherit the standrad article class

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass[]{article}
\pagenumbering{arabic}  


% ------- Referencing main article
\RequirePackage{xr-hyper}

\newcommand{\mref}[1]{\begin{NoHyper}\ref{main:#1}\end{NoHyper}}
\newcommand{\maineqref}[1]{Main Text Eq.~(\mref{#1})}
\newcommand{\mainfigref}[1]{Main Text Figure \mref{#1}}
\newcommand{\main}{Main Text}

\newcommand\maindoc[1]{\externaldocument[main:]{#1}}

% Needed to avoid error?
\newcommand{\supp}{Supplementary}
\newcommand{\suppmat}{\supp{} Material}
\newcommand{\suppfig}{\supp{} Figure}
\newcommand{\suppsec}{\supp{} Note}
\newcommand{\sref}[1]{\begin{NoHyper}\ref{supp:#1}\end{NoHyper}}
\newcommand{\suppfigref}[1]{\suppfig{} \sref{#1}}
\newcommand{\suppsecref}[1]{\suppsec{} \sref{#1}}

% ------- Some essential packages

\RequirePackage{amsmath,amssymb}

% hyperlinks 
\RequirePackage[colorlinks,linkcolor=blue,citecolor=blue]{hyperref}

\RequirePackage{cleveref}% Load AFTER hyperref!

\RequirePackage{mathrsfs}  
\RequirePackage{graphicx}
\RequirePackage{placeins}
\RequirePackage{subfig}
\RequirePackage[format=plain]{caption}
\RequirePackage{multicol}
\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{dblfloatfix}


% ------- Simple layout settings -------------------------------

% nicer font
\RequirePackage{stix2}

% section formatting
\RequirePackage{titlesec,titletoc}
\titleformat*{\section}{\bfseries}
\titleformat*{\subsection}{\small\bfseries}
\def\subsectionautorefname{Section}


\RequirePackage{datetime}
\newdateformat{stddate}{\monthname[\THEMONTH] \THEDAY \THEYEAR}

% set margins
\RequirePackage[margin=16mm, top=12mm,includehead,headheight=8mm]{geometry}

% figure caption style
\RequirePackage[labelfont=bf,font=footnotesize]{caption}

% Equation references as Eq. (1)
\RequirePackage{letltxmacro}
\LetLtxMacro{\originaleqref}{\eqref}
\renewcommand{\eqref}{Eq.~\originaleqref}

% bold-style vectors
\renewcommand{\vec}[1]{\boldsymbol{#1}}

% Add S to labels
\renewcommand\thefigure{S\arabic{figure}}
\renewcommand{\theequation}{S\arabic{equation}}
\renewcommand{\thesection}{S\arabic{section}}



% -------- Title, abstract, header ---------------------------------

% title already defined
% short title for header, default to full title
\newcommand\shorttitle[1]{\renewcommand\@shorttitle{#1}}
\newcommand\@shorttitle{\@title}

% for author/affiliation 
\RequirePackage{authblk}

\newcommand{\affiliation}[2][]{\affil[#1]{{\small #2}}}

\newcommand{\myrule}{\par\noindent\rule{\textwidth}{0.4pt}}

% version variable
\newcommand\version[1]{\renewcommand\@version{#1}}
\newcommand\@version{}


%header upper right
%\newcommand\headerupperright[1]{\renewcommand\@headerupperright{#1}}
%\newcommand\@headerupperright{Version \@version, \@date}

\RequirePackage{fancyhdr}

% Define layout of title, abstract, header blocks
\renewcommand{\maketitle}{%
    % Title and author horizontal section
    \thispagestyle{empty}         
        \vspace{-2cm}
        {\noindent\bfseries\large Supplementary material }

        {\noindent\large \@title}
        
        \myrule
        \vspace{2mm}	
        \@author

        \vspace{2mm}
        \noindent \@version	
        \myrule
        \vspace{10mm}
    
    \pagestyle{fancy}
    %\fancyhead[R]{\thepage} %why does it not work?
    \fancyhead[L]{SI: \@shorttitle\hfill \thepage}
}






% ------- Appendix ------------------------------
\RequirePackage{appendix}

\let\orgappendix\appendix

\renewcommand{\appendix}{%
\orgappendix % Switch to appendix mode
	\section*{Appendices} % Define new unnumbered section for appendices
	\addcontentsline{toc}{section}{Appendices} % Add section to table of contents
	\renewcommand{\thesubsection}{\Alph{subsection}} % Set subsection numbering to alphabets
	%\makeatletter
	\renewcommand{\p@subsection}{Appendix }
	%\makeatother
}


% ------- Acknowledgements --------------------
\newcommand{\acknowledgements}{%
    \section*{Acknowledgements}
    \addcontentsline{toc}{section}{Acknowledgments}
}

% ------- Bibliography -----------------------------

%\RequirePackage{cite}

\RequirePackage[
    backend=bibtex,
    %style=numeric-comp,
    style=phys,
    biblabel=brackets,
    pageranges=false,
    maxnames=6,
    isbn=false
]{biblatex}

% remove ugly "in"
\renewbibmacro{in:}{}

\DeclareFieldFormat[article]{title}{#1}
