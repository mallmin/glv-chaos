\documentclass[]{monograph}
\usepackage{parskip}
\usepackage[normalem]{ulem}


\newcommand{\emil}[1]{{\color{WildStrawberry}{#1}}}
\newcommand{\response}[1]{{\color{NavyBlue}{#1}}}
\newcommand{\review}[1]{{\it #1}}


\title{Response Letter}
%\title{Response Letter\\[1em]\small ``Chaotic turnover of rare and abundant species in a strongly interacting model community''\\Tracking nr 2023-12822}
%\author{Emil Mallmin, Silvia De Monte, Arne Traulsen}
\author{``Chaotic turnover of rare and abundant species in a strongly interacting model community'' Tracking nr 2023-12822}

\begin{document}
\maketitle

We thank the two Reviewers and the Editor for their time and thoughtful comments.

We are pleased that the Reviewers found the paper \textit{``very well-written''} and \textit{``accomplished and interesting''}, respectively. To alleviate any doubts that our work will be relevant to the broad audience of PNAS, we have followed the recommendation of the Editor to improve the framing of our results to emphasize their biological relevance and generality. 

We present first our section-by-section revisions to improve the framing, and then address the specific points raised by the reviewers. We have also supplied a version of the manuscript were differences to the original submission have been highlighting in colour.
\begin{itemize}
\item \textbf{Introduction}. Completely rewritten with the goal of presenting a clearer narrative. We start from empirical evidence for ecological chaos (in particular in the plankton), and of the 'rare biosphere'. These features are hard to capture by traditional low-dimensional ecological models, and call for the use of complex models for high-richness communities. We explicitly state that the goal of this work is to relate two complementary perspectives: that of the fluctuating abundance time series of individual species, and that of local community-level statistics, such as the instantaneous distribution of abundances across species and the overall strength of interactions. This goal sets our study apart from previous theoretical work on related models, and presents a framing that should appeal to both empirically and theortically oriented readers.

\item\textbf{Model}. Minor changes to plain text to avoid repeating conceptual points now stated in Introduction, and adding a note related to the time scale comment (see below).

\item\textbf{Results}. Unchanged apart from a few typos and phrasings.

\item\textbf{Discussion}. Rewritten to match the emphasis set by the revised Introduction and present clearly stated conclusions: ``first, persistent chaos arises generically, and can drive fast and extensive turnover of rare and abundant species; second, a statistical equivalence between species emerges such that a single focal species' fluctuation statistics predicts the largely invariant power-law abundance distributions; third, deviations from this equivalence are associated with species differences in frequency of occurence''. The following Discussion then elaborates on these points in a general ecological context---e.g. how robust is the chaotic phase to added biological realism?---and more specifically in a plankton context.

\item\textbf{Abstract}. Rewritten to match the updated framing.
\end{itemize}

We would like to point out that much of the insightful work carried out on disordered ecosystems is published in physics journals, in a technical language that we believe is mostly inaccessible to non-physicists. Our efforts, redoubled in the revision, to give ecological motivations and interpretations for the theoretical constructs used, while at the same time making an original scientific contribution, puts our work in a position to resonate with a broad PNAS audience.


Below we include the full review reports in italic, interspersed with our responses in colour.


\vfill
Best Regards,

Emil Mallmin, Arne Traulsen, Silvia De Monte 

\newpage

\section*{Report from Reviewer 1}

\review{This manuscript by Mallmin, Traulsen and De Monte reports on a comprehensive analysis of the dynamic coexistence of many species in Lotka-Volterra models with disordered interactions. Most prior work has considered these models in the weak interaction realm where species' self-limitation effects dominate or are similar in strength to the strongest interspecific effects and coexistence is sought in terms of fixed-point (equilibrial) stability. The present manuscript demonstrates how the coexistence of hundreds of species is possible in the strong interaction regime within which community dynamics are non-equilibrial and chaotic. Notable among the manuscript's other main contributions is the demonstration of how the frequency distribution of the typical (average) individual species' abundances over time (their "abundance fluctuation distribution) mirrors the community's species abundance distribution at a snapshot in time, insightfully showing by approximation how the slope of these may be related to the parameters of a stochastic focal-species model.}

\response{We thank the reviewer for having read the paper carefully and accurately summarizing our results.}

\review{With the caveat that empirical work in microbial and planktonic systems is outside my focal specialty and that many of the mathematical tools and approximation techniques are beyond my quantitative expertise, I consider the manuscript to be very well-written and thorough. Indeed, the authors anticipated most of the questions that came up for me and addressed them in subsequent sections or the supplementary materials. In particular, the empiricist in me was comforted by the authors showing that -- despite the chaotic nature of single-species and community-wide dynamics -- dynamics were such that there were, in general, persistently dominant and persistently rare taxa observed observed, an aspect of that would give me significant pause if it had not been observed given its occurrence in natural communities (both micro- and macroscopic). Likewise, the theoretician in me was similarly pleased to see the role of the immigration rate addressed throughout the manuscript (notably Figs. 2 and S7) seeing as its inclusion represents an important assumption that not only distinguishes the authors' work from much prior work but also has what would seem to be obviously strong repercussions in promoting species' persistence.

Somewhat sheepishly (for this is a rare occurrence for me), I must admit that I have no substantive criticism or suggestions to offer the authors. Certainly there are numerous questions and directions to explore (some initially explored in the supplementary materials), but I did not find the manuscript lacking for these. Even the occurrence of typos is so low as to not be worth mentioning.}

\response{We are very pleased that the reviewer appreciates our exposition, and especially that we managed to anticipate and address their questions to the extent that no substantive changes were called for.}

\review{Perhaps the only request I would make is to ask that the time-scale of the dynamics be described in more empirically-accessible terms. That is, for example, rather than (or in addition to) describing the turnover of abundant and rare species as occurring over a period of time comparable to the time it would take an isolated species to attain its carrying capacity (lines 225-230), it would be useful to re-express this in terms of how many (scaled) generations this time period represents (i.e. relating it back to r = K = 1 or their generalization in S3 ).}

\response{Species double their abundance every $(\ln 2)/r$ time units ($=0.7$ for $r=1$) in the absence of interactions, as we now state when the parameter is introduced. Since $r$ is proportional to division rate minus death rate, the number of generations is less than one per doubling, by some amount that the model is agnostic to. To preserve the generality of the model we therefore prefer to express our time scales in time units (rather than generations or days), but have added pointers to the order of magnitude of generations that are represented by some time scales quoted in the Results text.}

\newpage

\section*{Report from Reviewer 2}

\review{ Mallmin et al. present a thorough study of a many-species Lotka-Volterra model with random species interaction coefﬁcients. What distinguishes this paper, from many others that ﬁt the
same general description, is that the interaction coefﬁcients are not assumed to scale inversely
with species richness, the so-called weak-interaction regime that has been studied extensively using
methods from statistical physics. Instead, motivated by consideration of phytoplankton communities, the between-species interaction coefﬁcients are chosen independently from Gaussian distributions with mean $\mu > 0$ and variance $\sigma^2$ large enough that some negative values are likely to
occur in a species-rich community. Biologically, this means that most interactions are competitive
(phytoplankton competing for the same pool of limiting nutrients, and for light), but there also
are some facilitative interactions (cross-feeding, in which metabolites released by one species are
food for another).

The authors show that with these simple assumptions and suitable choices of $\mu$ and $\sigma$, the
model can reproduce several major features of phytoplankton communities: a power-law tail in
the rank-abundance plot for the community at any one time (so that the community consists
of relatively few common species, and many rare species), and “boom and bust” dynamics of
most species, so that there is constant turnover in the identity of the common species. They ﬁnd
that the dynamics of any one species can be represented by a single-species model in which the
net effect of other species on the focal species’ per-capita population growth rate is modelled
as an Ornstein-Uhlenbeck process with appropriate statistics. Looking broadly across $\mu$ and $\sigma$
values, the authors ﬁnd (by simulation) the parameter ranges giving different possible dynamics, including competitive exclusion of all but one species, stable coexistence of a few species, stable
coexistence of many species, and the chaotic turnover reminiscent of phytoplankton community
dynamics.}

\response{We thank the reviewer for attentively reading the paper and summarizing it well. }

\review{The paper is technically impressive. This work is far enough from what I do myself that I
can’t identify any technical breakthroughs, and I can’t testify to their absence, but it is at least a
clever and thoughtful application of the existing “toolkit” for this kind of model. And I enjoyed
reading it. The writing is admirably clear. The paper is well organized and very well signposted,
so that you always know where you are in the paper’s development and the principal messages
are clearly highlighted. Good decisions were made about the division of material between main
text and Supplement, and I appreciated the authors’ willingness to spell out technical details so
that somebody outside the “club” who work on very similar models can follow the arguments.
The Discussion is honest about the strengths and weaknesses of the paper, in particular about
how reasonable (or not) it is to regard the model as being a plausible model for phytoplankton
communities.}

\response{We are very pleased that the reviewer has found the paper well presented and that they enjoyed reading it. Since we have put particular effort into ``popularizing'' rather advanced methods from statistical physics, it is especially gratifying that the reviewer showed appreciation in this regard.}

\review{ So if I were reviewing this paper for a theoretical biology or theoretical ecology journal, I
would write with unqualiﬁed enthusiasm. For PNAS, however, it seems to me that a thorough study of a model is not the right framing. The framing should be, what novel information or
conclusions does the model bring to our understanding of many species coexistence in phyto-
plankton communities? For example, for a “thorough study” paper the broad picture shown
in Figure 5 is the most important conclusion, but for a “what have we learned about phytoplankton communities?” paper, the only important feature is that the chaotic turnover regime is fairly broad rather than a knife-edge phenomenon. On the other hand, given that other models
have produced chaotic turnover dynamics and power-law rank-abundance distributions, as the
authors acknowledge in the Introduction, for a general biological (or scientiﬁc) audience it is
important to present evidence that this model does a better job of explaining the data than others
already on the table — until that happens, it’s not clear why this particular model has a claim
for the attention of a general audience.}


\response{Yes, we provide a thorough study of a model, but its construction, the features of its dynamics and statistics that we focus on, and the interpretation of our results, are all framed by ecological concepts---especially so in the revised manuscript, where we have put more emphasis than before on the empirical evidence for ecological chaos and for rare biosphere patterns (especially in the plankon). 

We discuss various other models (e.g.\ with weak interactions, metacommunities, or neutral assumptions); none has the ability to---or was not analysed and discussed in such a way as to---directly link the two themes in the way that we have, where our derivation of a focal species model really sets our work apart. 

Despite the reviewer's comment about the framing in our original submission, it is clear from other paragraphs that they have apprehended our main message for ecology: that rare biosphere patterns and (robust) chaotic turnover can be conceptually and quantitatively linked through the combination of strong interactions and weak immigration.

By emphasizing more the conceptual over the technical aspects of the work (in and of itself and in relation to other model) in the revised Abstract, Introduction, and Discussion, we hope the appeal to a broader audience will be more apparent.
}

\review{ Please don’t get me wrong. This paper makes a signiﬁcant contribution, and I would be more
than thrilled to have one of my PhD students or postdocs produce an equally accomplished and
interesting paper. The weak-interaction regime may be appealing on grounds of tractability, but
there is overwhelming evidence that communities are a mix of strong and weak interactions.
This paper directs a community of theoreticians’ attention back to the strong interactions regime,
showing that major qualitative features of plankton communities can be obtained in the strong
interactions regime, possibly with better ﬁdelity to real communities (e.g., power law exponents
bigger than 1). But at this point, I think that this is mostly a message for theoreticians. In some
important ways the model is not plausible as a model for phytoplankton communities. Mechanistically, it lacks trophic structure, as the authors acknowledge. And phenomenologically, the
model cannot explain the frequent observation of greater regularities at the functional group level
than at the species level (e.g., Mutschinda et al.\ 2016, \emph{Functional Ecology} 2016, doi: 10.1111/13652435.12641), because interaction parameters are not chosen to create functional groups. This paper points towards possible future models outside the weak-interactions regime that are mechanistically plausible, and also best the competition in quantitative confrontations with empirical data. But we’re not there yet. Perhaps the authors can quickly take the next step and make this
into a paper with a stronger claim to general audience appeal, and if not, it may be better suited
to other publication venues.}

\response{ With regards to introducing strucuture, we do believe it is a particularly interesting direction, and have added a paragraph on fluctuations on multiple taxonomic levels of aggregation in the revised Discussion. We highlight the empirical findings that the signal for chaos weakens at higher levels of aggregation (Rogers 2023), and that succession patterns show more dynamical regularity and predictability at the level of functional groups (Mutshinda 2016). In fact, even with our unstructured model we capture the feature that fluctuations are less severe at the aggregated level.

The rich behaviour of the model despite its minimal assumptions is part of its merit in our opinion. Understanding it in detail provides a baseline against which the importance of added features of biological realism can be addressed in future work. Ultimately, we believe none of our simiplifying assumptions have sacrificed too much realism for our insights to be valuable. 
}


\end{document}