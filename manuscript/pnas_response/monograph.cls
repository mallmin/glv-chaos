\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{monograph}[2023/07/18 Monograph LaTeX class]

% -------  Inherit the standrad article class

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass[]{article}


% ------- Simple layout settings -------------------------------

% nicer font
\RequirePackage{stix2}

% colors
\usepackage[dvipsnames]{xcolor}

% section formatting
\RequirePackage{titlesec,titletoc}
\titleformat*{\section}{\bfseries} %\centering
\titleformat*{\subsection}{\small\bfseries}

% hyperlinks 
\RequirePackage[colorlinks,linkcolor=blue,citecolor=blue]{hyperref}

% set margins
\usepackage[margin=35mm, top=22mm]{geometry}

% figure caption style
\RequirePackage[labelfont=bf,font=footnotesize]{caption}

% Add todo notes
\usepackage{todonotes}

% -------- Title ---------------------------------

% for author/affiliation 
\RequirePackage{authblk}

\newcommand{\affiliation}[2][]{\affil[#1]{{\small #2}}}

\newcommand{\myrule}{\par\noindent\rule{\textwidth}{0.4pt}}

% Define layout of title section
\renewcommand{\maketitle}{%
    \thispagestyle{empty}
    \begin{center}
        \noindent{\bfseries\Large \@title}\vspace{8mm}
        
        \@author
        
        \@date    
    \end{center}
}


% ------- Bibliography -----------------------------

\RequirePackage[
    backend=bibtex,
    style=authoryear,
    maxbibnames=4
]{biblatex}

% remove ugly "in"
\renewbibmacro{in:}{}

% make title link to doi & remove quotation marks
\ExecuteBibliographyOptions{doi=false}
\newbibmacro{string+doi}[1]{%
  \iffieldundef{doi}{#1}{\href{http://dx.doi.org/\thefield{doi}}{#1}}}
\DeclareFieldFormat{title}{\usebibmacro{string+doi}{\mkbibemph{#1}}}
\DeclareFieldFormat[article]{title}{\usebibmacro{string+doi}{#1}} %{\mkbibquote{#1}}}
