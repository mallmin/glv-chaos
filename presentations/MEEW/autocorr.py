# Paths
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
grandparentdir = os.path.dirname(parentdir)
sys.path.append(parentdir)
sys.path.append(grandparentdir)
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

import numpy as np
import matplotlib.pyplot as plt
import glv
import argparse
from matplotlib import colors
from timer import Timer

gt = Timer("Script").start()

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--load", action="store_true")
args = parser.parse_args()



S = 500
dat = glv.obs.UnitGLVSeries.load(f"./glv-chaos/ref-data/strong_chaos_{S}.pkl")

pm = glv.obs.CompoundParameters()
mat_acf = pm.from_trajectory(dat)

tau = pm.tau_eta
T = 250
mat_acf = mat_acf[:T,:]
vec_t = dat.vec_t[:T]
vec_acf = np.mean(mat_acf, axis=1)
vec_std = np.std(mat_acf, axis=1)
vec_exp = np.exp(- vec_t / tau)

plt.rc('font',size=16)

fig, ax = plt.subplots()

ax.fill_between(vec_t, vec_acf + vec_std, vec_acf - vec_std, color="lightskyblue")
ax.plot(vec_t, vec_acf, color="midnightblue")
ax.plot(vec_t, vec_exp, color='k', linestyle="dashed")

fig.savefig(subpath(f"graph_acf_{S}"),dpi=300)

gt.stop()