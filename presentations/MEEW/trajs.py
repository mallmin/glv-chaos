
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
gparentdir = os.path.dirname(parentdir)
sys.path.append(parentdir)
sys.path.append(gparentdir)
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

import glv
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import default_rng
from timer import Timer
import argparse

gt = Timer("Script").start()

S = 500
mu = 6/S
sig = 2.2 / np.sqrt(S)

s = glv.sim_legacy.UnitGLVSystem(S=S,mu=mu,sig=sig)

traj = glv.obs.UnitGLVSeries()

s.initialize_uniform()
s.generate_observables(traj, 100*200, record_interval=100)

colors = glv.rand_cmap(S, type='soft') 
fig, ax = plt.subplots()
for i in range(S):
    ax.plot(traj.vec_t, traj.mat_x[:,i], color=colors[i])
fig.savefig(joinpath(currentdir,"graph_weak_chaos"),dpi=300)

gt.stop()