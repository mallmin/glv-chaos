# Paths
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
grandparentdir = os.path.dirname(parentdir)
sys.path.append(parentdir)
sys.path.append(grandparentdir)
from os.path  import join as joinpath
def subpath(p): return joinpath(currentdir, p)

import numpy as np
import matplotlib.pyplot as plt
import glv
import argparse
from matplotlib import colors
from timer import Timer

gt = Timer("Script").start()

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--load", action="store_true")
args = parser.parse_args()



S = 500
dat = glv.obs.UnitGLVSeries.load(f"./glv-chaos/ref-data/strong_chaos_{S}.pkl")

pm = glv.obs.CompoundParameters()
pm.from_trajectory(dat)
bins = np.logspace(-10,1,50)
ad = glv.obs.AbundanceDistributions(bins)
ad.from_trajectory(dat)
bins_mid = ad.bins_mid
vec_sad = ad.vec_dist

if args.load:
    datr = glv.obs.AbundanceSeries.load(joinpath(currentdir,"r.pkl"))
else:
    r = glv.sim_legacy.IntermittencyModel(-pm.a, pm.b, pm.tau_eta, 10**-8)
    datr = glv.obs.AbundanceSeries()
    r.generate_observables(datr, 100*100000, record_interval=100)
    datr.save(joinpath(currentdir,"r.pkl"))

adr = glv.obs.AbundanceDistributions(bins)
adr.from_trajectory(datr)
vec_sadr = adr.vec_dist

T = 5000
n = 2
vec_t = dat.vec_t[n*T:(n+1)*T] - dat.vec_t[n*T] 
vec_x = dat.mat_x[n*T:(n+1)*T,0]
vec_xr = datr.mat_x[n*T:(n+1)*T,0]

### PLOTTING

plt.rc("font",size=16)

ymax = 1.2 * np.maximum(np.amax(vec_x),np.amax(vec_xr))
ymin = 0.8 * np.minimum(np.amin(vec_x),np.amin(vec_xr))

# trajectory - SINGLE
fig, ax = plt.subplots()
fig.set_size_inches((12,4))
ax.plot(vec_t, vec_x, color='midnightblue',lw=1)
ax.set_yscale('log')
ax.set_ylim([ymin,ymax])
fig.savefig(subpath("graph_traj_one"), dpi=300)

# trajectory - BOTH
fig, ax = plt.subplots()
fig.set_size_inches((12,4))
ax.plot(vec_t, vec_x, color='midnightblue',lw=1)
ax.plot(vec_t, vec_xr, color='darkorange',lw=1)
ax.set_yscale('log')
ax.set_ylim([ymin,ymax])
fig.savefig(subpath("graph_traj_two"), dpi=300)

# sad - SINGLE
fig, ax = plt.subplots()
fig.set_size_inches((6,4))
ax.plot(bins_mid, vec_sad, color='midnightblue', lw=2)
ax.set_xscale('log')
ax.set_yscale('log')
fig.savefig(subpath("graph_sad_one"), dpi=300)

# sad - BOTH
fig, ax = plt.subplots()
fig.set_size_inches((6,4))
ax.plot(bins_mid, vec_sad, color='midnightblue',lw=2)
ax.plot(bins_mid, vec_sadr, color='darkorange',lw=2)
ax.set_xscale('log')
ax.set_yscale('log')
fig.savefig(subpath("graph_sad_two"), dpi=300)




gt.stop()