import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import glv
import numpy as np
import matplotlib.pyplot as plt
from timer import Timer
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--load", action = "store_true")
args = parser.parse_args()


gt = Timer("Script").start()

sp = "./glv-chaos/pres-trieste/"

lam = 10**-8
mu = 0.5
sig = 0.3
S = 500

if args.load:
    s = glv.sim.UnitGLVSystem.load(os.path.join(sp, "traj.npz"))
else:
    s = glv.sim.UnitGLVSystem(S=S, mu=mu, sig=sig, lam=lam, alpha_seed=12345)
    s.initialize_uniform(seed=1)
    s.trajectory(100*1000, store=False, check_nan = True, scheme='log')
    s.reset_time()
    s.trajectory(100*50000, store=True, record_interval=100, check_nan=True)
    s.save(os.path.join(sp, "traj.npz"))


plt.rc('font', size=16) 

bins = np.logspace(-9, 1, 50)
bins_mid = np.exp( (np.log(bins)[0:-1] + np.log(bins)[1:])/2 )
vec_x = s.mat_x.flatten()
hist_all, _ = np.histogram(vec_x, bins=bins, density=True)

fig, ax = plt.subplots()
fig.set_size_inches(7, 6)

for i in range(s.mat_x.shape[1]):
    hist, _ = np.histogram(s.mat_x[:,i], bins=bins, density=True)
    ax.plot(bins_mid, hist, color="grey", lw=0.2, zorder=-2, alpha=0.2)
ax.plot(bins_mid, hist_all, color='k')

#snap

ax.hist(s.mat_x[-2000,:],bins=bins,density=True, color="lightgrey", edgecolor="grey", zorder=-4)



ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel("Abundance", fontsize=16)
ax.set_ylabel("Frequency", fontsize=16)

fig.savefig(os.path.join(currentdir, "afd"))

gt.stop()