

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import default_rng


mat = - default_rng(123).uniform(size=10*10).reshape(10,10)
np.fill_diagonal(mat,-0.1)

sub_mat = mat[[0,3,5,7,8,9]][:,[0,3,5,7,8,9]]



plt.imshow(mat)
ax = plt.gca()
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)

plt.savefig("./glv-chaos/experiments/mat")

plt.imshow(sub_mat)
ax = plt.gca()
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)

plt.savefig("./glv-chaos/experiments/sub_mat")

mat = - default_rng(123).standard_normal((10,10))
np.fill_diagonal(mat,-1)

plt.imshow(mat,cmap='plasma')
ax = plt.gca()
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)

plt.savefig("./glv-chaos/experiments/mat_2")
