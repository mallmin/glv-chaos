import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import glv
import numpy as np
import matplotlib.pyplot as plt
from timer import Timer
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--load", action = "store_true")
args = parser.parse_args()


gt = Timer("Script").start()

sp = "./glv-chaos/pres-trieste/"

s = glv.sim.UnitGLVSystem.load(os.path.join(sp, "traj.npz"))

a = -0.2517185084248513
b = 0.2581021947073892
tau = 33.49625743233702

r = glv.sim.ReducedFocalSpeciesModel(a,b,tau,10**-8)

if args.load:
    npz = np.load(os.path.join(sp,"red.npz"))
    vec_x = npz['vec_x']
    vec_t = npz['vec_t']
else:
    vec_x, vec_t = r.trajectory(100*50000, record_interval=100)
    np.savez(os.path.join(sp, "red.npz"), vec_x=vec_x, vec_t=vec_t)


plt.rc('font', size=16) 



stop = 5000

fig, ax = plt.subplots()
fig.set_size_inches(16, 4.8)
ax.plot(vec_t[:stop], s.mat_x[:stop,0], label="GLV")
ax.plot(vec_t[:stop], vec_x[:stop], label="Reduced model")
ax.set_yscale("log")
ax.legend()
ax.set_xlabel("time")
ax.set_ylabel("abundance")
fig.savefig(os.path.join(sp,"comp"),dpi=200)


stop = None

fig, ax = plt.subplots()
ax.plot(vec_t[:stop], s.mat_x[:stop,0])
ax.plot(vec_t[:stop], vec_x[:stop])
fig.savefig(os.path.join(sp,"comp_log"))

gt.stop()

