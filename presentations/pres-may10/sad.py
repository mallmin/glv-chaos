import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import glv
import numpy as np
import matplotlib.pyplot as plt
from timer import Timer
from numpy.random import default_rng
from matplotlib.gridspec import GridSpec
import argparse

gt = Timer("Script").start()
path = "./glv-chaos/pres-may10/"


# s = glv.sim.UnitGLVSystem(S=500, mu=0.5, sig=0.3, lam=10**-8, scheme='logistic')
# s.initialize_stationary()

# s.trajectory(100*50000, record_interval=100, store=True)
# s.save(f"{path}traj.npz")

s = glv.sim.UnitGLVSystem.load(f"{path}traj.npz")


bins = np.logspace(-9, 1, 50)
bins_mid = np.exp( (np.log(bins)[0:-1] + np.log(bins)[1:])/2 )
vec_x_all = s.mat_x.flatten()

hist_all, _ = np.histogram(vec_x_all, bins=bins, density=True)


fig, ax = plt.subplots()
ax.plot(bins_mid, hist_all, zorder=2, lw=2)

# for t in range(10):
#     vec_x = s.mat_x[-t,:]
#     hist_snap, _ = np.histogram(vec_x, bins=bins, density=True)
#     ax.plot(bins_mid, hist_snap, color="gray")

for i in range(s.S):
    vec_x = s.mat_x[:,i]
    hist_ind, _ = np.histogram(vec_x, bins=bins, density=True)
    ax.plot(bins_mid, hist_ind, color="orange", alpha=0.4,lw=0.4, zorder=-1)

ax.set_yscale('log')
ax.set_xscale('log')

fig.savefig(f"{path}graph_hist")

gt.stop()