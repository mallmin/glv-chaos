import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import glv
import numpy as np
import matplotlib.pyplot as plt
from timer import Timer
from numpy.random import default_rng
from matplotlib.gridspec import GridSpec
import argparse

gt = Timer("Script").start()

S = 500
mu = 0.5 #0.4 / S
sig = 0.3 #1.5 / np.sqrt(S)
T = 10000
dt = 0.01
N = int(T / dt)
R = int(1./dt)

vec_x0 = default_rng().uniform(size=S) 

tm = Timer("logistic").start()
s = glv.sim.UnitGLVSystem(S=S, mu=mu, sig=sig, lam=10**-8, dt=dt)
s.initialize_stationary()
s.trajectory(N, record_interval=R, store=True, check_nan = True)
tm.stop()
glv.plot.stack(s, savepath="./glv-chaos/pres-may10/", tag=f"boom_logistic")
print(f"nan {np.isnan(s.mat_x).any()}")

tm = Timer("log").start()
s = glv.sim.UnitGLVSystem(S=S, mu=mu, sig=sig, lam=10**-8, dt=dt, scheme="log")
s.initialize_stationary()
s.trajectory(N, record_interval=R, store=True, check_nan = True)
tm.stop()
glv.plot.stack(s, savepath="./glv-chaos/pres-may10/", tag=f"boom_log")
print(f"nan {np.isnan(s.mat_x).any()}")
print(s.vec_t)



gt.stop()