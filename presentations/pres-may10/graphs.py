import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import glv
import numpy as np
import matplotlib.pyplot as plt
from timer import Timer
from numpy.random import default_rng
from matplotlib.gridspec import GridSpec
import argparse

gt = Timer("Script").start()
path = "./glv-chaos/pres-may10/"
plt.rc('font', size=16) 

# s = glv.sim.UnitGLVSystem(S=500, mu=0.5, sig=0.3, lam=10**-8, scheme='logistic')
# s.initialize_stationary()

# s.trajectory(100*50000, record_interval=100, store=True)
# s.save(f"{path}traj.npz")

# s = glv.sim.UnitGLVSystem.load(f"{path}traj.npz")

# stop=500
# glv.plot.stack(s, stop=stop, savepath=path, tag="demo")
# glv.plot.overlap(s, stop=stop, savepath=path, tag="demo", log=False, fill_between=False)
# glv.plot.overlap(s, stop=stop, savepath=path, tag="log_demo", log=True, fill_between=False)


# fig, ax = plt.subplots()
# fig.set_size_inches(6.4*2.4, 4.8 * 0.4)
# ax.plot(s.vec_t, s.mat_x[:,0])
# ax.set_xlabel("$t$")
# ax.set_ylabel("$x_0(t)$")
# fig.savefig(f"{path}graph_single_demo", dpi=300)


T = 200
oup = glv.sim.OUProcess(100,0.01)
oup.eta= 0.

vec_eta, vec_t = oup.trajectory(100*T)
vec_xi = np.zeros(100*T)
vec_xi[1:] = (default_rng().standard_normal(100*T-1))

fig, ax = plt.subplots()
fig.set_size_inches(6.4*1.3, 4.8*1.3)
ax.plot(vec_t, vec_xi)
ax.plot(vec_t[::10], vec_eta[::10])
ax.set_xlabel(r"$t$")
ax.set_ylabel(r"$\xi(t), \eta(t)$")
fig.savefig(f"{path}graph_eta_vs_xi")


fig, ax = plt.subplots()
fig.set_size_inches(6.4*1.3, 4.8*1.3)
ax.hist(vec_eta,bins=100)
ax.hist(vec_xi,bins=100)
ax.set_xlabel(r"$t$")
ax.set_ylabel(r"$\xi(t), \eta(t)$")
fig.savefig(f"{path}graph_noisehist")



gt.stop() 