import time
import datetime

class Timer:
    def __init__(self, msg=None, format="m:s", absolute=False):
        self.msg = msg
        self.format = format
        self.absolute = absolute
        self.t0 = None
    import datetime

    def start(self):
        self.t0 = time.time()
        str_absolute = f" at {datetime.datetime.now()}" if self.absolute else ""
        print(f"{self.msg}:\t started{str_absolute}")
        return self

    def stop(self, result_str=""):
        self.t1 = time.time()
        if self.t0 is None:
            raise RuntimeError("Cannot stop Timer that has not started.")

        dt = self.t1 - self.t0 
        str_absolute_time = f" at {datetime.datetime.now()}" if self.absolute else ""
        

        str_tot_time = None
        if self.format == "m:s":
            minutes = int(dt / 60)
            seconds = int(dt- minutes*60)
            str_tot_time = f" ({minutes}m{seconds}s)" if minutes > 0 else f" ({dt:.2f}s)"

            #if minutes > 0:
            #    print(f" {self.msg}:\t completed ({minutes}m{seconds}s)")
            #else:
            #    print(f"{self.msg}:\t completed ({dt:.2f}s)")
        elif self.format == "s":
            str_tot_time = f" ({dt}s)"
        
        print(f"{self.msg}:\t completed{str_tot_time}{str_absolute_time}")
        if result_str != "":
            print(f"{self.msg}:\t{result_str}")

        return dt

        
        


