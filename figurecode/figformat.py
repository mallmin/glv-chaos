'''
Contains specifications to make figure appearance conistent
'''

import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from os.path  import join as joinpath


DPI = 600 #
FONT_SIZE_LARGE = 9 
FONT_SIZE_MEDIUM = 7
FONT_SIZE_SMALL = 6
#FONT_SIZE_AXIS = 16

A4_HEIGHT_CM = 29.7
A4_WIDTH_CM = 21

SINGLE_COL_REL_PAGE_WIDTH = 0.4
DOUBLE_COL_REL_PAGE_WIDTH = 0.85

# Define color maps


# Utilities

A,B,C,D,E,F,G = [rf"$\bf{{{x}}}$" for x in ["A","B","C","D","E","F","G"]]

def place_A(x,y):
    plt.figtext(x,y,A,fontsize='large')
def place_B(x,y):
    plt.figtext(x,y,B,fontsize='large')   
def place_C(x,y):
    plt.figtext(x,y,C,fontsize='large')   
def place_D(x,y):
    plt.figtext(x,y,D,fontsize='large')
def place_E(x,y):
    plt.figtext(x,y,E,fontsize='large')      
def place_F(x,y):
    plt.figtext(x,y,F,fontsize='large')     

def letter(L,x,y,ax):
    ax.text(x,y,rf"$\bf{{{L}}}$",transform=ax.transAxes,fontsize=FONT_SIZE_LARGE)



def apply_defaults():
    plt.rc('font', size=FONT_SIZE_MEDIUM)
    #plt.rc('axes', titlesize=8)


def cm2in(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)

def adjust_fig_size(fig, scale_of_a4_width=SINGLE_COL_REL_PAGE_WIDTH,
                    aspect_ratio=1):
        width_inches = scale_of_a4_width * 21 / 2.54
        height_inches = width_inches / aspect_ratio
        fig.set_size_inches(width_inches, height_inches)

def no_box(ax,where=['top','right']):
    if type(where) is str:
         where = [where]
    for loc in where:
         ax.spines['top'].set_visible(False)


def savefig(fig, path, transparent=True):
    fig.savefig(path+".png", format="png", dpi=DPI, bbox_inches='tight', pad_inches=0.01, transparent=transparent)
    #fig.savefig(path+".svg", format="svg", bbox_inches='tight', pad_inches=0, transparent=True)
    fig.savefig(path+".eps", format="eps", bbox_inches='tight', pad_inches=0.01, dpi=DPI)
    fig.savefig(path+".pdf", format="pdf", bbox_inches='tight', pad_inches=0.01, dpi=DPI, transparent=transparent)


COLORS = ['dodgerblue','deeppink', 'gold','springgreen']

cmap_default = ListedColormap([COLORS])



#legacy
cmap_no1_3 = ListedColormap(['dodgerblue','deeppink', 'gold'])