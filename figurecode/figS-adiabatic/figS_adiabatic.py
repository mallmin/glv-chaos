import os, sys
sys.path.append('./')
sys.path.append('./figurecode/')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import glv
from matplotlib import colors
import numpy.ma as ma
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from simcode.hysteresis.hysteresis import AdiabaticLine
import figformat as fmt


###############################################################################
#   PANEL PLOT FUNCTIONS
###############################################################################

def plot_adiab(ax):
    
    # Load data
    df = pd.read_csv("./data/phase-adiab/data.csv")

    # Generate mesh data
    X,Y,Z_idx = glv.phase.meshgridXYZ(df, 'mu', 'sig', 'lim_idx')
    X,Y,Z_Neff = glv.phase.meshgridXYZ(df, 'mu', 'sig', 'Neff')

    # Apply masking to separate phases
    Z_div = ma.masked_where(Z_idx != -1, Z_Neff)
    Z_chaos = ma.masked_where(Z_idx != 1, Z_Neff)
    Z_multi = ma.masked_where((Z_idx != 0) | (Z_Neff > 10), Z_Neff)
    Z_ufp = ma.masked_where((Z_idx != 0) | (Z_Neff < 10), Z_Neff)
    Z_cycle = ma.masked_where(Z_idx <= 1, Z_Neff)

    # 
    cmap_chaos = colors.LinearSegmentedColormap.from_list('chaos', ['cornsilk', 'gold','darkgoldenrod'])
    cmap_multi = colors.LinearSegmentedColormap.from_list('multi', ['lavenderblush','deeppink','darkmagenta'])
    cmap_ufp = colors.LinearSegmentedColormap.from_list('ufp', ['lightskyblue', 'midnightblue'])

    pc_div = ax.pcolormesh(X,Y,Z_div, cmap= colors.ListedColormap(['whitesmoke']))
    pc_chaos = ax.pcolormesh(X,Y,Z_chaos, cmap=cmap_chaos, vmax=80)
    pc_multi = ax.pcolormesh(X,Y,Z_multi, cmap=cmap_multi)
    pc_ufp = ax.pcolormesh(X,Y,Z_ufp, cmap=cmap_ufp)
    pc_cycle = ax.pcolormesh(X,Y,Z_cycle, cmap=colors.ListedColormap(['mediumpurple']))

    # Legend / colorbars
    
    x_text = 0.28
    dy = 0.14
    y_top = 0.89
    y_text = 0.896

    axcb_multi = ax.inset_axes([0.06, y_top, 0.2, 0.05])
    cb = ax.figure.colorbar(pc_multi, cax=axcb_multi, orientation="horizontal",ticks=[2,4,6,8])
    cb.set_label(r"$S_{eff}$", labelpad=-34)
    ax.text(x_text, y_text, 'fixed point (few sp.)', ha='left', va='center',transform=ax.transAxes)

    axcb_chaos = ax.inset_axes([0.06, y_top - dy, 0.2, 0.05])
    cb = ax.figure.colorbar(pc_chaos, cax=axcb_chaos, orientation="horizontal", ticks=[1,20,40,60], extend="max")
    ax.text(x_text, y_text - dy, 'persistent chaos', ha='left', va='center',transform=ax.transAxes)

    axcb_ufp = ax.inset_axes([0.06, y_top - 2*dy, 0.2, 0.05])
    cb = ax.figure.colorbar(pc_ufp, cax=axcb_ufp, orientation="horizontal",ticks=[150,300,500])
    ax.text(x_text, y_text - 2*dy, 'fixed point (many sp.)', ha='left', va='center',transform=ax.transAxes)
    
    axcb_ufp = ax.inset_axes([0.06, y_top - 3*dy, 0.05, 0.05])
    cb = ax.figure.colorbar(pc_cycle, cax=axcb_ufp, orientation="horizontal",ticks=[])
    ax.text(x_text - 0.15, y_text - 3*dy, r"cycle$^*$", ha='left', va='center',transform=ax.transAxes)
    
    # Some formatting

    ax.set_aspect('equal', 'box')
    ax.set_title(r"Adiabatic phase diagram")
    ax.set_xlabel(r"interaction mean $\mu\quad$")
    ax.set_ylabel(r"interaction std $\sigma$")


def plot_sweep(ad, ax, order=False):

    # To plot a bar at each time point, need to calculate bar edges
    n = ad.range_mu.size 
    mat_x_plus = np.zeros((2 * n, ad.s.S))
    vec_mu_plus = np.zeros(2*n)
    delta_mu = np.diff(ad.range_mu)[0]
    eps = 0.01 * delta_mu 
    for i in range(n):
        mat_x_plus[2*i, :] = ad.mat_x[i,:]
        mat_x_plus[2*i+1, :] = ad.mat_x[i,:]
        vec_mu_plus[2*i] = ad.range_mu[i] - delta_mu/2 + eps
        vec_mu_plus[2*i+1] = ad.range_mu[i] + delta_mu/2 - eps

    # Plot the bars

    colors = glv.rand_cmap(ad.s.S)
    ax.stackplot(vec_mu_plus, mat_x_plus.T, colors=colors)
    if (not order) and (ad.range_mu[0] > ad.range_mu[-1]):
        ax.set_xlim(ax.get_xlim()[::-1])



###############################################################################
#   MAKE FIGURE
###############################################################################

fmt.apply_defaults()

fig = plt.figure(constrained_layout=True)

gs = GridSpec(2, 2, figure=fig, height_ratios=[1, 1])
fmt.adjust_fig_size(fig, fmt.DOUBLE_COL_REL_PAGE_WIDTH, 2/1)

# Plot the phase diagram

ax_phase = fig.add_subplot(gs[:, 0])
plot_adiab(ax_phase)


# Plot the sweeps

ad = AdiabaticLine.load("./data/hysteresis/ad.pkl")
ad_rev = AdiabaticLine.load("./data/hysteresis/ad_rev.pkl")

max_y = 2.5
arw_width = 0.1
arw_head_width = 0.3
arw_head_length = 0.05
arw_y = 0.9*max_y
arw_dy = 0.2

ax_sweep1 = fig.add_subplot(gs[0, 1])
plot_sweep(ad, ax_sweep1, True)
ax_sweep1.set_xticks(np.arange(0.6,1.5,0.1))
ax_sweep1.tick_params(labelbottom=False)
ax_sweep1.set_ylabel("abundance")
ax_sweep1.arrow(1.4, arw_y, -arw_dy, 0, length_includes_head=True, width=arw_width, head_width=arw_head_width, head_length=arw_head_length, color='k')
ax_sweep1.set_ylim([0, max_y])

ax_sweep1.set_title(r"Adiabatic sweep at $\sigma = 0.3$")

ax_sweep2 = fig.add_subplot(gs[1, 1])
plot_sweep(ad_rev, ax_sweep2, True)
ax_sweep2.set_xlabel(r"interaction mean $\mu\quad$") # (at fixed $\sigma=0.3$)
ax_sweep2.set_ylabel("abundance")
ax_sweep2.arrow(0.6, arw_y, arw_dy, 0, length_includes_head=True, width=arw_width, head_width=arw_head_width, head_length=arw_head_length, color='k')
ax_sweep2.set_ylim([0, max_y])

y1 = 0.96
x2 = 0.5
fmt.place_A(0.01,y1)
fmt.place_B(x2,y1)
#fmt.place_C(x2, 0.52)

fmt.savefig(fig, subpath("figS_adiabatic"))


