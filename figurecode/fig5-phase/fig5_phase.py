
import os, sys
sys.path.append('./')
sys.path.append('./figurecode/')

from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import argparse
import glv
from matplotlib import colors
import numpy.ma as ma
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import figformat as fmt



def plot_chaos_prob(ax):

    # Read data
    df = pd.read_csv("./data/phase-rand/data.csv")
    df['cp'] = df['n_chaos'] / df['N']
    df['dp'] = df['n_div'] / df['N']
    
    # Create data mesh
    X,Y,Z_chaos = glv.phase.meshgridXYZ(df, 'mu', 'sig', 'cp')
    X,Y,Z_div = glv.phase.meshgridXYZ(df, 'mu', 'sig', 'dp')
    Z_chaos =  ma.masked_where(Z_div == 1, Z_chaos)
    Z_div =  ma.masked_where(Z_div < 1, Z_div)

    # Colormaps
    cmap_ch = colors.LinearSegmentedColormap.from_list('chaos_strong',['papayawhip', 'orange','darkgoldenrod'])
    cmap_div = colors.ListedColormap(['whitesmoke'])

    # Plot the mesh
    ax.pcolormesh(X,Y,Z_div, cmap= cmap_div, rasterized=True)
    pc = ax.pcolormesh(X,Y,Z_chaos, cmap= cmap_ch, rasterized=True)
    #ax.set_facecolor('white')

    # Plot dashed line
    ax.plot([1.,0.45],[0,0.61],color='k',linestyle="dashed",linewidth=1.)

    # Colorbar 
    #cbax = inset_axes(ax, width="8%", height="3%", loc=2)
    axcb = ax.inset_axes([0.06, 0.85, 0.2, 0.05])
    cb = ax.figure.colorbar(pc, cax=axcb, orientation="horizontal", ticks=[0,1])
    cb.set_label("prob. of chaos", labelpad=-30)
    cb.set_ticklabels(['0%', '100%'])
    #ax.text(0.25, 0.9, 'prob. chaos', ha='left', va='center',transform=ax.transAxes)

    # Phase labels
    ax.text(0.45, 0.2, 'Chaos', ha='center', va='center',fontsize=9)
    ax.text(0.6, 0.95, 'Divergence', ha='center', va='center',fontsize=9)
    ax.text(1.1, 0.4, 'Multiple\nattractors', ha='center', va='center',fontsize=9)
    ax.text(1.12, 0.04, 'Exclusion', ha='left', va='center',fontsize=6)
    ax.text(0.02, 0.04, 'Unique FP', ha='left', va='center',fontsize=6)

    # Axes formating & label
    ax.set_aspect('equal', 'box')
    #ax.set_title(r"$\bf{(A)}$ Dynamical phase diagram")
    ax.set_xlabel(r"interaction mean $\mu$")
    ax.set_ylabel(r"interaction std $\sigma$")



fmt.apply_defaults()

fig, ax = plt.subplots()
plot_chaos_prob(ax)
utl.adjust_fig_size(fig, 0.4, 1.)
fmt.savefig(fig, subpath("fig5_phase"))
