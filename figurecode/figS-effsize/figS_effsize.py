'''
Show how the effective community size scales with number of species
'''

import os, sys
sys.path.append('./')
sys.path.append('./figurecode')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import matplotlib.pyplot as plt
import glv
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import figformat as fmt
import argparse
import palettable


parser = argparse.ArgumentParser()
parser.add_argument("-g", "--generate", action="store_true")
args = parser.parse_args()

if args.generate:
    vec_S = np.array([320, 640, 1280, 2560, 5120, 10240])
    vec_Seff = np.zeros(vec_S.size)
    for i,S in enumerate(vec_S):
        traj = glv.load_observable(f"data/traj-S-range/traj_S={S}.pkl")
        vec_Seff[i] = np.mean(traj.vec_Neff)
        del traj
    np.savez(subpath("effsize.npz"), vec_S=vec_S, vec_Seff=vec_Seff)
else:
    npz = np.load(subpath("effsize.npz"))
    vec_S = npz['vec_S']
    vec_Seff = npz['vec_Seff']

fig, ax = plt.subplots()

fmt.adjust_fig_size(fig)
fmt.apply_defaults()


colors = palettable.colorbrewer.sequential.PuRd_8.mpl_colors[::-1][:6][::-1]
ax.plot(vec_S, vec_Seff,zorder=-1, color='k')
ax.scatter(vec_S, vec_Seff, c=colors)
ax.set_xscale('log')

ax.set_title("Scaling of effective community size with richness")
ax.set_xlabel("species richness $S$")
ax.set_ylabel(r"avg. effective size $S_{eff}$")

fmt.savefig(fig, subpath("figS_eff"))