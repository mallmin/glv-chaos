'''
Show the role of pairwise correlations
'''

import os, sys
sys.path.append('./')
sys.path.append('./figurecode')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import matplotlib.pyplot as plt
import glv
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import figformat as fmt
import argparse
import palettable

###############################################################################
#   LOAD DATA  
###############################################################################

tr = glv.Tracker().start("Script")

parser = argparse.ArgumentParser()
parser.add_argument("-g", "--generate", action="store_true")
args = parser.parse_args()

traj = glv.load_observable("./data/traj-ref/traj_ref_org.pkl")    

if args.generate:
    tr2 = tr.start_sub("ccf")
    ten_ccf = glv.corr.crosscorr_fft(traj.mat_x, target='gpu')
    np.savez(subpath("ten_ccf.npz"), ten_ccf=ten_ccf)
    tr2.end()
else:
    ten_ccf = np.load(subpath("ten_ccf.npz"))['ten_ccf']


###############################################################################
#   PLOT FUNCTIONS  
###############################################################################

def plot_top(ax):
    mat_c = ten_ccf[0,:,:]
    S = mat_c.shape[0]
    
    #inset
    axi = ax.inset_axes([0.65, 0.5, 0.3,0.4])
    np.fill_diagonal(mat_c, np.nan)
    bins = np.linspace(-0.1,0.5,100)
    axi.hist(mat_c.flatten(), bins,color='lightgrey', density=True)
    axi.set_yticks([])
    axi.set_xlabel(r"$\phi_{ij}(0)$")
    axi.set_title("Correlation distribution",fontsize=6)


    # max    
    np.fill_diagonal(mat_c, -1)
    vec_idx = np.argmax(mat_c, axis=1)
    vec_c = mat_c[np.arange(S), vec_idx]
    vec_z = traj.mat_z[np.arange(S), vec_idx]
    ax.scatter(vec_z, vec_c,s=3, linewidth=0, c=fmt.COLORS[0], edgecolors=None)

    # min    
    np.fill_diagonal(mat_c, +1)
    vec_idx = np.argmin(mat_c, axis=1)
    vec_c = mat_c[np.arange(S), vec_idx]
    vec_z = traj.mat_z[np.arange(S), vec_idx]
    ax.scatter(vec_z, vec_c,s=3, linewidth=0, c=fmt.COLORS[1], edgecolors=None)

    # all
    np.fill_diagonal(mat_c, np.nan)
    ax.scatter(traj.mat_z.flatten(), mat_c.flatten(),zorder=-1,s=2, linewidth=0,c='lightgrey', edgecolors=None)

    # lag time
    #max
    mat_c = np.amax(ten_ccf, axis=0)
    np.fill_diagonal(mat_c, -1)
    vec_idx = np.argmax(mat_c, axis=1)
    vec_c = mat_c[np.arange(S), vec_idx]
    vec_z = traj.mat_z[np.arange(S), vec_idx]
    ax.scatter(vec_z, vec_c,s=3, linewidth=0, c='lightskyblue', alpha=0.5, edgecolors=None)

    # min
    mat_c = np.amin(ten_ccf, axis=0)    
    np.fill_diagonal(mat_c, +1)
    vec_idx = np.argmin(mat_c, axis=1)
    vec_c = mat_c[np.arange(S), vec_idx]
    vec_z = traj.mat_z[np.arange(S), vec_idx]
    ax.scatter(vec_z, vec_c,s=3, linewidth=0, c='pink', alpha=0.5, edgecolors=None)


    (xmin, xmax) = ax.get_xlim()
    (ymin, ymax) = ax.get_ylim()
    ax.set_xlim([xmin, xmax])
    ax.set_ylim([ymin, ymax])

    ax.hlines(0, xmin, xmax, color='k', linewidth=1)
    ax.vlines(0, ymin, ymax, color='k', linewidth=1)

    ax.set_title("Pairwise correlations versus interaction coefficients")
    ax.set_xlabel(r"rescaled interaction $z_{ij}$")
    ax.set_ylabel(r"correlation coeff. $\phi_{ij}$")

    ax.text(0.02,0.85,"each species'\nmax corr",transform=ax.transAxes,color='dodgerblue')
    ax.text(0.82,0.17,"each species'\nmax anti-corr",transform=ax.transAxes,color='deeppink')



###############################################################################
#   MAKE FIGURE  
###############################################################################

fig, ax = plt.subplots()
fmt.apply_defaults()

fmt.adjust_fig_size(fig, 0.7*fmt.DOUBLE_COL_REL_PAGE_WIDTH, 2)

#plot_zc(ax)
plot_top(ax)



fmt.savefig(fig, subpath("figS_corr"))

tr.end()