
import os, sys
sys.path.append('./')
sys.path.append('./figurecode/')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import argparse
import glv
import figformat as fmt


parser = argparse.ArgumentParser()
parser.add_argument("-g", "--generate", action="store_true")
args = parser.parse_args()

gt = utl.Timer("Script").start()


###############################################################################
#   LOAD DATA / SET VARIABLES
###############################################################################

T_snap = 5000*2
traj = glv.load_observable("data/traj-ref/traj_ref_org.pkl")
vec_snap = traj.mat_x[T_snap, :]
lam = traj.lam
e = -np.log10(lam)

if args.generate:
    bins = np.logspace(-e -2, 1, 100)
    obs = glv.obs.AbundanceDistributions(bins, powerlaw_cutoff=[100*lam,10**-2] )
    obs.from_trajectory(traj)
    obs.save(subpath("distribution.pkl"))
else:
    obs = glv.obs.AbundanceDistributions.load(subpath("distribution.pkl"))

#del traj

###############################################################################
#   PANEL PLOT FUNCTION  
###############################################################################

def plot_rad(ax):
    
    ts = np.int64(np.linspace(0,traj.mat_x.shape[0]-1,10))

    vec_rank = np.arange(500) +1
    vec_rad = np.sort(traj.mat_p[T_snap,:])[::-1]

    #ax.plot([0,500],[traj.lam,traj.lam], color='k', lw=1,linestyle='dashed')
    ax.margins(x=0)

    for t in ts:
        vec_p = np.sort(traj.mat_p[t,:])[::-1]
        lab = "snapshot" if t == ts[-1] else None
        ax.plot(vec_rank, vec_p, color='k', lw=0.2, label=lab)
        


    ax.plot(vec_rank, vec_rad, color='lightskyblue')
    ax.set_yscale("log")
    # ax.set_xscale("log")
    ax.set_yticks([1e-8,1e-6,1e-4,1e-2])

    #ax.legend(frameon=False, fontsize=fmt.FONT_SIZE_SMALL)

    ax.set_title("Snapshots of rank-abundance plot")
    ax.set_xlabel("rank")
    ax.set_ylabel("rel. abundance")
    fmt.letter('A', -0.14, 1.2, ax)
    

def plot_sad(ax):
    
    # Draw average
    ax.plot(obs.bins_mid, obs.vec_dist, lw=1, color='k', label='AFD (species avg.)')

    # Draw spread
    vec_std = np.std(obs.mat_dist, axis=0)    
    ax.fill_between(obs.bins_mid, obs.vec_dist - vec_std, obs.vec_dist + vec_std, color='lightgray', alpha=0.8,linewidth=0)

    # Draw snapshot
    bins = np.logspace(-10,1,50)
    ax.hist(vec_snap, bins=bins, density=True, edgecolor='k', facecolor='lightskyblue', lw=0.2, label='SAD (snapshot)', zorder=-1)

   

    # Format
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim([10**-9, 3])
    ax.set_yticks(10.**np.array([-5,-1,3,7]))

    # Text
    ax.set_title(f"Abundance distributions")
    fmt.letter('B', -0.14, 1.1, ax)
    ax.set_ylabel('prob. density')
    ax.set_xlabel('abs. abundance')
    #ax.set_title("abundance distribution")
    ax.text(0.38, 0.78, r"$P(x) \sim x^{-\nu}$", transform=ax.transAxes)

    y = ax.get_ylim()
    ax.vlines(traj.lam, y[0], y[1], color='k', lw=1,linestyle='dashed')
    ax.set_ylim(y)

    # Legend
    ax.legend(frameon=False, fontsize='small')

###############################################################################
#   MAKE FIGURE 
###############################################################################


fmt.apply_defaults()


# fig, ax = plt.subplots()
# plot_sad(ax)
# fmt.adjust_fig_size(fig, aspect_ratio=1.2)
# fig.tight_layout()
# fig.savefig(subpath("sad_presentation.png"),dpi=600)
# quit()

fig = plt.figure(constrained_layout=True)
fmt.adjust_fig_size(fig, aspect_ratio=1.2/1)

gs = GridSpec(2, 1, figure=fig, height_ratios=[1,1.5])

ax = fig.add_subplot(gs[0, 0])
plot_rad(ax)

ax = fig.add_subplot(gs[1, 0])
plot_sad(ax)

fmt.savefig(fig, subpath("fig2_distribution"))

gt.stop()



