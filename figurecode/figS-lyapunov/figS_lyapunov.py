import os, sys
sys.path.append('./')
sys.path.append('./figurecode')

from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import glv
from matplotlib import colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import figformat as fmt
import argparse

###############################################################################
#   LOAD DATA  
###############################################################################

tr = glv.Tracker().start("Script")



parser = argparse.ArgumentParser()
parser.add_argument("-g", "--generate", action="store_true")
args = parser.parse_args()

if args.generate:
    T = 10000
    n = 2
    f = 1
    
    s = glv.DisorderedLVSystem(S=500, mu=0.5, sig=0.3, lam=1e-8, dt=0.01/f)
    mle = glv.MLESeries(delta0=1e-13)
    traj = glv.DisorderedLVSeries()

    s.discard_transient(100*f*1000)
    s.generate_observables(mle, n*100, record_interval=n) # discard

    s.generate_observables([mle,traj], 100*T*f, record_interval=n)

    vec_mle = mle.vec_mle
    np.savez(subpath("mle.npz"), vec_mle=vec_mle, vec_t=traj.vec_t)
else:
    npz = np.load(subpath("mle.npz"))
    vec_mle = npz['vec_mle']
    vec_t = npz['vec_t']

###############################################################################
#   PLOT FUNCTIONS  
###############################################################################

def plot_mle(ax,T):
    ax.plot(vec_t, vec_mle, color=fmt.COLORS[0])

    ax.hlines(0,vec_t[0],vec_t[-1], color='k', zorder=-1, linewidth=1)
    ax.margins(x=0)

    ax.set_xticks(np.linspace(0,T,5))
    ax.set_xlim([0,T])
    ax.set_title("Maximal finite-time Lyapunov exponent")
    ax.set_xlabel("time")
    ax.set_ylabel("MLE $\Lambda$")

def plot_cumulative_mle(ax,T):
    vec_mle_cum = np.cumsum(vec_mle) / np.arange(1, 1 + vec_mle.size)
    print(vec_mle_cum[T-1])

    ax.hlines(0,vec_t[0],vec_t[-1], color='k', zorder=-1,linewidth=1)

    ax.set_xlim([0,T])

    ax.margins(x=0)
    
    ax.plot(vec_t, vec_mle_cum, color=fmt.COLORS[1], label="Cumulative average of FTLE")
    ax.hlines(vec_mle_cum[-1], vec_t[0], vec_t[-1], linestyle='dashed', color='k', label="Maximal Lyapunov Exponent")

    ax.set_xticks(np.linspace(0,T,5))
    ax.set_title("Convergence of maximal Lyapunov exponent")
    ax.set_xlabel("time")
    ax.set_ylabel("MLE $\Lambda$")

    ax.legend(frameon=False)

###############################################################################
#   MAKE FIGURE  
###############################################################################


fig, (ax1, ax2) = plt.subplots(1,2)

fmt.adjust_fig_size(fig, fmt.DOUBLE_COL_REL_PAGE_WIDTH, aspect_ratio=3.5)
fmt.apply_defaults()

plot_mle(ax1,200)
plot_cumulative_mle(ax2,2000)

fmt.place_A(0.03, 0.95)
fmt.place_B(0.49, 0.95)


fig.tight_layout()

fmt.savefig(fig, subpath("figS_lyapunov"))



tr.end()