import os, sys
sys.path.append('./')
sys.path.append('./figurecode')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
import myutils as utl
import glv
from matplotlib import colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import myutils
import figformat as fmt



###############################################################################
#   LOAD DATA  
###############################################################################

# Read the data from the chaotic phase
df = pd.read_csv("data/phase-chaos/data.csv")


df['nu_ucna'] = - 1 - df['k'] / (df['u']**2 * df['tau_n'])  # add UCNA prediction

# Read the image defining the chaotic phase
#img = plt.imread("simcode/phase-chaos/chaos_colored.png")
img = plt.imread(joinpath(currentdir, "chaos_col2.png"))
C = img[::-1,:,:] #horizontal flip
X, Y, Z = glv.phase.meshgridXYZ(df,'mu','sig','chaos')


vec_mu = df['mu'].to_numpy()
idx_perm = np.random.permutation(np.arange(vec_mu.shape[0])) #permute so that order of points plotted in scatter is random
vec_mu = vec_mu[idx_perm]
vec_sig = df['sig'].to_numpy()[idx_perm]
vec_u = df['u'].to_numpy()[idx_perm]
vec_k = df['k'].to_numpy()[idx_perm]
vec_tau_n = df['tau_n'].to_numpy()[idx_perm]
vec_tau_x = df['tau_x'].to_numpy()[idx_perm]
vec_nu = -df['nu'].to_numpy()[idx_perm] #- 1
vec_nu_eff = - df['nu_eff'].to_numpy()[idx_perm] #-1
vec_nu_ucna = - df['nu_ucna'].to_numpy()[idx_perm] #- 1
vec_X = df['X'].to_numpy()[idx_perm]
vec_Neff = df['Neff'].to_numpy()[idx_perm]
vec_rho = df['rho'].to_numpy()[idx_perm]

#For S, lam sacaling
S_range = [320, 640, 1280, 2560, 5120, 10240]
lam_exponents = [6,8,10,12,14,16,20,24,28, 48]
lam_range = 10.**-np.array(lam_exponents)


# position of labels
x = -0.2 
y = 1.13

###############################################################################
#   UTILITIES  
###############################################################################


def img_phase_color():
    '''
    Makes a .png colouring the chaotic phase
    '''
    imgb = plt.imread("simcode/phase-chaos/chaos_borders.png")
    Cb = imgb[::-1,:,:] #horizontal flip

    west = []
    south = []
    east = []

    for i in range(Cb.shape[0]):
        for j in range(Cb.shape[1]):            
            if not (Cb[i,j] == [0.,0.,0.,1.]).all():
                if (Cb[i,j] == [1.,0.,0.,1.]).all():
                    south.append([i,j])
                elif (Cb[i,j] == [0.,1.,0.,1.]).all():
                    west.append([i,j])
                elif (Cb[i,j] == [0.,0.,1.,1.]).all():
                    east.append([i,j])
    mat_w = np.array(west)
    mat_s = np.array(south)
    mat_e = np.array(east)

    C = np.zeros(Cb.shape)

    max_dw = 0.
    max_de = 0.
    max_ds = 0.
    for i in range(C.shape[0]):
        for j in range(C.shape[1]):
            if not (Cb[i,j] == [0.,0.,0.,1.]).all():
                # calculate distances
                dw = np.amin(np.linalg.norm(mat_w - np.array([i,j]), axis=1))
                de = np.amin(np.linalg.norm(mat_e - np.array([i,j]), axis=1))
                ds = np.amin(np.linalg.norm(mat_s - np.array([i,j]), axis=1))
                if ds > max_ds:
                    max_ds = ds
                if de > max_de:
                    max_de = de
                if dw > max_dw:
                    max_dw = dw
                C[i,j] = np.array([ds, dw, de, 1.])

    # Normalize colours
    for i in range(C.shape[0]):
        for j in range(C.shape[1]):
            if not (Cb[i,j] == [0.,0.,0.,1.]).all():
                C[i,j] = np.minimum(3 * C[i,j] / [max_ds, max_dw, max_de, 1.], [1.,1.,1.,1.])

    plt.imsave(joinpath(currentdir,"chaos_col2.png"), C[::-1,:,:])
    

def phase_color():
    '''
    Nx4 matrix of colors of the flattened phase
    '''
    N = vec_mu.shape[0]
    mat_pc = np.zeros((N,4))
    for n in range(N):
        mu = vec_mu[n]
        sig = vec_sig[n]
        j = int(np.round((mu+0.1)/0.01))
        i = int(np.round(sig/0.01))
        if np.sum(C[i,j,:]) == 4.:
            mat_pc[n,:] = 0
        else:
            mat_pc[n,:] = C[i,j,:]
    return mat_pc

mat_pc = phase_color()


###############################################################################
#   PLOT FUNCTIONS  
###############################################################################


def plot_nu(ax, inset=False):
    '''Scatter nu_pred vs nu'''

    # Plot
    ax.scatter(vec_nu, vec_nu_eff, c=mat_pc, s=2, edgecolors='none')

    ax.set_xlim([0.97, 1.5])
    ax.set_ylim([0.97, 1.5])
    ax.set_aspect(1)

    ax.plot(ax.get_xlim(),ax.get_ylim(),color='k',linestyle='dashed',lw=1)

    # Text    
    #fmt.letter('B', x,y, ax)
    ax.set_title(fr"Power-law exponent")
    ax.set_facecolor("whitesmoke")
    ax.set_ylabel(r"$\nu_{foc}$")
    ax.set_xlabel(r"$\nu$")

    # r = vec_nu_eff/vec_nu
    # r = r[~np.isnan(r)]
    # print(f"nu_eff/nu = {np.mean(r)} +- {np.std(r)}")

    # inset
    if inset:
        axi = ax.inset_axes([0.55,0.10,0.4,0.35])
        vec_err = (vec_nu_eff - 1)/ (vec_nu -1) - 1    
        axi.hist(vec_err, bins=1000, density=True, color='grey')
        axi.set_xlim([-0.6,0.6])
        axi.set_yticks([])
        #axi.text(0.05,0.75, r"$\delta \nu^* / \delta\nu - 1$", transform=axi.transAxes)
        axi.set_title(r"$\delta \nu^* / \delta\nu - 1$",fontsize=6)


def plot_phase(ax):
    '''Color code for the chaotic phase'''
    i0 = 85
    ie = 141
    j0 = 10
    je = 115

    mus = [X[0,j] for j in range(img.shape[1])]
    sigs = [Y[i,0] for i in range(img.shape[0])][::-1]

    # Plot
    ax.imshow(img[i0:ie,j0:je,:], interpolation = 'nearest')
    
    # Format
    ax.set_facecolor("none")
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    yr = list(range(15,ie-i0+15,20))
    #yr = [15,35,55]   
    xr = range(0,je-j0,20)
    mur = mus[j0:je:20]
    sigr = sigs[i0+15:ie+15:20]
    #print(sigr)
    #sigr = [sigs[ie-i+5] for i in yr]
    ax.set_xticks(xr, mur)
    ax.set_yticks(yr, sigr)

    # Text
    ax.set_xlabel(r"$\mu$")
    ax.set_ylabel(r"$\sigma$")
    ax.set_title("Color legend\nof chaotic phase")

def plot_u_k(ax, inset=False):
    '''
    Scatter u vs k
    '''
    
    xlim = [-0.05,1.1]
    ylim = xlim

    # Plot
    ax.scatter(vec_k, vec_u, c=mat_pc,s=2, edgecolors='none')

    ax.plot([-0.05,1.1],[-0.05,1.1],color='k',linestyle='dashed',lw=1)
    
    # Format
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_box_aspect(1)
    ax.set_facecolor('whitesmoke')
    
    # Text
    ax.set_title(39*" " + "Dependence between effective parameters")
    #fmt.letter('A', x-0.03, y, ax)
    ax.set_ylabel("$u$")
    ax.set_xlabel("$k$")
    
    # Some related data
    # r = vec_u / vec_k
    # r = r[~np.isnan(r)]
    # print(f"u/k = {np.mean(r)} +- {np.std(r)}")
   
    # inset
    if inset:
        axi = ax.inset_axes([0.55,0.10,0.4,0.35])
        vec_f = vec_u / vec_k    
        axi.hist(vec_f, bins=3*1500, density=True, color='grey')
        axi.set_xlim([0,3])
        axi.set_yticks([])
        axi.set_xticks([0,1,2])
        axi.set_title(r"$u^*/k^*$",fontsize=6)


def plot_u_tau(ax, inset=False):
    '''Scatter u vs 1/tau_n'''

    # Plot
    #ax.scatter(1/vec_tau_n, vec_u, c=mat_pc,s=2, edgecolors='none')
    ax.scatter(1/vec_tau_n, vec_u, c=mat_pc,s=2, edgecolors='none')
    # don't plot line anymore
    #ax.plot([0,0.1],[0,1],color='k',linestyle='dashed',lw=1)

    # Format
    ax.set_xlim([-0.005,0.12])
    ax.set_box_aspect(1)
    ax.tick_params(labelleft=False)
    ax.set_facecolor('whitesmoke')

    # Text
    ax.set_xlabel(r"$1/\tau$")

    # inset
    if inset:
        axi = ax.inset_axes([0.55,0.10,0.4,0.35])
        vec_f = vec_u *  vec_tau_n    
        axi.hist(vec_f, bins=5000, density=True, color='grey')
        axi.set_xlim([5,20])
        axi.set_yticks([])
        #axi.set_xticks([0,1,2])
        axi.set_title(r"$u^* \tau^*$",fontsize=6)





###############################################################################
#   MAKE FIGURE  
###############################################################################


fmt.apply_defaults()

fig = plt.figure()
fmt.adjust_fig_size(fig, fmt.DOUBLE_COL_REL_PAGE_WIDTH, aspect_ratio=3.1)

gs = GridSpec(1, 3, figure=fig, width_ratios=[0.8,2.2,1])

#left panel
ax_mid = fig.add_subplot(gs[0, 0])
plot_phase(ax_mid)

# mid panel
gs_a = gs[0,1].subgridspec(1,2, wspace=0.1)
ax_bl = fig.add_subplot(gs_a[0, 0])
plot_u_k(ax_bl)
ax_br = fig.add_subplot(gs_a[0, 1], sharey=ax_bl)
plot_u_tau(ax_br)

# right panel
ax_b = fig.add_subplot(gs[0, 2])
plot_nu(ax_b)

y = 0.89
fmt.place_A(0.01,y)
fmt.place_B(0.25,y)
fmt.place_C(0.74,y)

fig.tight_layout()
fig.set_facecolor("none")

fmt.savefig(fig, subpath("fig6_relations"), transparent=False)

