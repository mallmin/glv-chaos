
import os, sys
sys.path.append('./')
sys.path.append('./figurecode/')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import argparse
import glv
import figformat as fmt
import palettable

gt = glv.Tracker().start("Script")

###############################################################################
#   LOAD DATA  
###############################################################################


parser = argparse.ArgumentParser()
parser.add_argument("-g", "--generate", action="store_true")
args = parser.parse_args()

S_range = [320, 640, 1280, 2560, 5120, 10240]
lam_exponents = [8,12,16,20,24,28,32,128]
lam_range = 10.**-np.array(lam_exponents)

if args.generate:
    tr = gt.start_sub("Generate data")
    vec_k_S = np.zeros(len(S_range))
    vec_u_S = np.zeros(len(S_range))
    vec_k_lam = np.zeros(len(lam_range))
    vec_u_lam = np.zeros(len(lam_range))

    for i,S in enumerate(S_range):
        traj = glv.load_observable(f"data/traj-S-range/traj_S={S}.pkl")
        # eff = glv.EffectiveNoise()
        # eff.from_trajectory(traj)
        # vec_k_S[i] = eff.k
        # vec_u_S[i] = eff.u
        vec_k_S[i] = - np.mean(traj.vec_a)
        vec_u_S[i] = np.mean(traj.vec_b)
        
        del traj
        #del eff

    for i,lam_exp in enumerate(lam_exponents):
        traj = glv.load_observable(f"data/traj-lam-range/traj_lam=1e-{lam_exp}.pkl")
        #eff = glv.EffectiveNoise()
        #eff.from_trajectory(traj)
        vec_k_S[i] = - np.mean(traj.vec_a)
        vec_u_S[i] = np.mean(traj.vec_b)

        del traj
        #del eff

    np.savez(subpath("ku.npz"),vec_k_S=vec_k_S,vec_u_S=vec_u_S,vec_k_lam=vec_k_lam,vec_u_lam=vec_u_lam)
    tr.end()
else:
    npz = np.load(subpath("ku.npz"))
    vec_k_S = npz["vec_k_S"]
    vec_u_S = npz["vec_u_S"]
    vec_k_lam = npz["vec_k_lam"]
    vec_u_lam = npz["vec_u_lam"]



###############################################################################
#   PLOT PANELS  
###############################################################################

def plot_S(ax):
    # Graph
    #ax.plot(S_range, nus_S, color='k', linestyle='-', marker='o', lw=2, markersize=4)
    
    ax.plot(S_range, vec_u_S/vec_k_S, color='k', zorder=-1)
    colors = palettable.colorbrewer.sequential.PuRd_8.mpl_colors[::-1][:len(S_range)][::-1]
    ax.scatter(S_range, vec_u_S/vec_k_S, c=colors)

    # Format
    ax.set_xscale('log')
    (ymin,ymax) = ax.get_ylim()
    ax.set_yticks(np.arange(.7,1.1,0.1))
    ax.set_ylim([ymin, ymax])

    #ax.set_yticks([1, 1.2, 1.4])
    #ax.set_yticks([1.1, 1.3], [], minor=True)
    #ax.set_xlim([0.8*10**2, ax.get_xlim()[1]])


    # Text
    #ax.set_title(r"$\nu$ vs $\log S$")
    ax.set_title(r"Scaling of $u/k$ ratio vs $S$")
    ax.set_xlabel("$S$")
    ax.set_ylabel(r"$u/k$")

def plot_lam(ax):
    # Graph
    vec_uk = (vec_u_lam/vec_k_lam)[:-1]
    ax.plot(lam_range[:-1], vec_uk, color='k', zorder=-1)
    
    colors = palettable.colorbrewer.sequential. YlGn_8.mpl_colors[::-1][:len(lam_range)-1]#[::-1]
    ax.scatter(lam_range[:-1], vec_uk, c=colors)
    
    # Format 
    ax.set_xscale('log')
    ax.set_xticks(lam_range[0:-1:2])

    # Text
    ax.set_title(r"Scaling of $u/k$ ratio with $\lambda$")
    ax.set_xlabel(r"$\lambda$")
    ax.set_ylabel(r"$u/k$")


###############################################################################
#   MAKE FIGURE  
###############################################################################

fig, (ax1, ax2) = plt.subplots(1,2)

fmt.adjust_fig_size(fig, 0.8*fmt.DOUBLE_COL_REL_PAGE_WIDTH, 2.5)
fmt.apply_defaults()


plot_S(ax1)
plot_lam(ax2)

fig.tight_layout()

fmt.place_A(0.05,0.92)
fmt.place_B(0.51,0.92)

fmt.savefig(fig, subpath("figS_kuscaling"))



gt.end()



