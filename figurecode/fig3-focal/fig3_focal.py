import os, sys
sys.path.append('./')
sys.path.append('./figurecode')

from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import glv
from matplotlib import colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import figformat as fmt

#parser = argparse.ArgumentParser()
#parser.add_argument("-p", "--generate", action="store_true")
#args = parser.parse_args()

###############################################################################
#   LOAD DATA  
###############################################################################

# traj = glv.obs.UnitGLVSeries.load("./data/traj_strong_ref.pkl")
# traj_eff = glv.obs.AbundanceSeries.load("./data/traj_eff_ref.pkl")
traj = glv.load_observable("./data/traj-ref/traj_ref_org.pkl")
traj_eff = glv.obs.AbundanceSeries.load("./data/traj-ref/traj_eff_ref.pkl")

bins = np.logspace(-10, 1, 100)

ad = glv.obs.AbundanceDistributions(bins)
ad.from_trajectory(traj)

ad_eff = glv.obs.AbundanceDistributions(bins)
ad_eff.from_trajectory(traj_eff)

r = glv.focal.StochasticFocal(traj_eff.k,traj_eff.u,traj_eff.tau,traj_eff.lam)
P = r.ucna()
vec_ucna =  np.array([P(x) for x in ad_eff.bins_mid])

# Check normalization
# print(np.sum(vec_ucna * np.diff(bins)))
# print(np.sum(ad.vec_dist * np.diff(bins)))

###############################################################################
#   PANEL PLOT FUNCTIONS  
###############################################################################

def plot_traj(ax):
    
    t0 = 1000
    T = 2500
    i = 0 # species 
    vec_t = traj.vec_t[:T]

    ax.hlines(traj.lam / traj_eff.k, vec_t[0], vec_t[-1], color='k', linestyle='dashed' )

    ax.plot(vec_t, traj.mat_x[t0:t0+T,i], color='dodgerblue',label='Disordered LV')
    ax.plot(vec_t, traj_eff.mat_x[t0:t0+T,0], color='deeppink',label='Stochastic model')
    ax.set_yscale('log')

    ax.tick_params(axis='y', which='minor', left=False)

    ax.margins(x=0)
    ax.set_title("Focal species time series")
    fmt.letter('A', -0.14, 1.2, ax)


    leg = ax.legend(frameon=True, fancybox=False, framealpha=0.8, fontsize=fmt.FONT_SIZE_SMALL,
              loc='upper right') 
    leg.get_frame().set_linewidth(0.0)

    ax.set_xlabel("time")
    ax.set_ylabel("abundance")

    ax.set_yticks([1e-7,1e-5,1e-3])
    

def plot_dist(ax):

    ax.plot(ad.bins_mid, ad.vec_dist, color='k',label='Species avg. in disordered LV')
    ax.plot(ad_eff.bins_mid, ad_eff.vec_dist, color='deeppink',label='Stochastic focal-species model')
    ax.set_xscale('log')
    ax.set_yscale('log')
    x = ax.get_xlim()
    y = ax.get_ylim()
    ax.plot(ad_eff.bins_mid, vec_ucna, color='deeppink', linestyle='dashed',label='coloured noise approximation')
    ax.set_xlim([10**-9,5])
    ax.set_ylim(y)
    ax.set_yticks(10.**np.array([-5,-1,3,7]))


    #ax.set_aspect(4/3)

    ax.set_title(f"Abundance distributions")
    fmt.letter('B', -0.14, 1.1, ax)
    ax.set_xlabel(r"abundance")
    ax.set_ylabel(r"density")
    
    ax.legend(frameon=False,fontsize=fmt.FONT_SIZE_SMALL,loc=[0.12,0.08]) 
    
###############################################################################
#   MAKE FIGURE 
###############################################################################    

fmt.apply_defaults()


fig = plt.figure(constrained_layout=True)
fmt.adjust_fig_size(fig, aspect_ratio=1.2/1)

gs = GridSpec(2, 1, figure=fig, height_ratios=[1,1.5])

ax = fig.add_subplot(gs[0, 0])
plot_traj(ax)

ax = fig.add_subplot(gs[1, 0])
plot_dist(ax)


fmt.savefig(fig, subpath("fig3_focal"))




