import os, sys
sys.path.append('./')
sys.path.append('./figurecode')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glv
from mpl_toolkits.axes_grid1 import make_axes_locatable
import figformat as fmt

###############################################################################
#   LOAD DATA  
###############################################################################

df = pd.read_csv("./data/phase-adiab/data.csv")

df['dX_rel'] = df['dX'] / df['X'] 
df['drho_rel'] = df['drho'] / df['rho']
df['dNeff_rel'] = df['dNeff'] / df['Neff']

###############################################################################
#   PLOT PANELS  
###############################################################################

def plot_panel(ax, prop, title, vmin=None, vmax=None, extend=None, left=False, bottom=False, log=False):
    X,Y,Z = glv.phase.meshgridXYZ(df, 'mu', 'sig', prop)
    if log:
        Z = np.log10(Z)
    pc = ax.pcolormesh(X,Y,Z, cmap='viridis', vmin=vmin, vmax=vmax)
    cax = make_axes_locatable(ax).append_axes("right", size="5%", pad=0.05)
    cb = ax.figure.colorbar(pc, orientation="vertical", cax=cax, extend=extend)
        
    ax.set_title(title)
    ax.set_aspect('equal', adjustable='box')
    ax.margins(x=0,y=0)
    ax.set_xticks(np.arange(0,1.5,0.4))
    ax.set_yticks(np.arange(0,1.5,0.4))
    if bottom:
        ax.set_xlabel(r"$\mu$")
    else:
        ax.tick_params(labelbottom=False)
    if left:
        ax.set_ylabel(r"$\sigma$")
    else:
        ax.tick_params(labelleft=False)


def plot_X(ax): plot_panel(ax, 'X', r"$\overline{X}$", 0, 20, extend='max', left=True)
def plot_Xrel(ax): plot_panel(ax, 'dX_rel', r"$std(X)/\overline{X}$", 0, 0.2, extend='max', left=True, bottom=True)
def plot_Neff(ax): plot_panel(ax, 'Neff', r"$\log_{10}\overline{S}_{eff}$", 0, log=True)
def plot_Neffrel(ax): plot_panel(ax, 'dNeff_rel', r"$std(S_{eff})/\overline{S}_{eff}$", extend='max', bottom=True)
def plot_rho(ax): plot_panel(ax, 'rho', r"$\overline{\rho}$", 0, 1.6)
def plot_rhorel(ax): plot_panel(ax, 'drho_rel', r"$std(\rho)/\overline{\rho}$", 0, 0.4, extend='max', bottom=True)

###############################################################################
#   MAKE FIGURE  
###############################################################################

fig, axs = plt.subplots(2,3)
fmt.adjust_fig_size(fig, fmt.DOUBLE_COL_REL_PAGE_WIDTH, 1.7)

# plot_X(axs[0,0], axs[1,0])
# plot_Neff(axs[0,1], axs[1,1])
# plot_rho(axs[0,2], axs[1,2])

plot_X(axs[0,0])
plot_Xrel(axs[1,0])
plot_Neff(axs[0,1])
plot_Neffrel(axs[1,1])
plot_rho(axs[0,2])
plot_rhorel(axs[1,2])

x1 = 0.04
x2 = 0.38
x3 = 0.68
y1 = 0.94
y2 = 0.50
fmt.place_A(x1,y1)
fmt.place_B(x2,y1)
fmt.place_C(x3,y1)
fmt.place_D(x1,y2)
fmt.place_E(x2,y2)
fmt.place_F(x3,y2)



fig.tight_layout()

fmt.savefig(fig, subpath("figS_phasevar"))