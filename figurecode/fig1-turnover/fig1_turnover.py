'''
Fig 1.
Top: Stack
Bottom: closeness to equilibrium

'''

import os, sys
sys.path.append('./')
sys.path.append('./figurecode/')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import glv
import argparse
import figformat as fmt

parser = argparse.ArgumentParser()
parser.add_argument("-g", "--generate", action="store_true")
#parser.add_argument("-o", "--output")
args = parser.parse_args()

gt = utl.Timer("Script").start()


T = 250

###############################################################################
#   LOAD DATA  
###############################################################################

traj = glv.load_observable("data/traj-ref/traj_ref_org.pkl")
traj.mat_x = traj.mat_x[:T,:]
traj.vec_t = traj.vec_t[:T]

colors = glv.rand_cmap(traj.S, type='soft')

# Generate or load closeness time series
if args.generate:
    s = glv.DisorderedLVSystem(S=traj.S, mu=traj.mu, sig=traj.sig, lam=0, mat_alpha=traj.mat_alpha)
    qe = glv.QuasiEquilibria(s,1)
    qe.from_trajectory(traj) 
    qe.save(subpath("quasi_eq.pkl"))
else:
    qe = glv.load_observable(subpath("quasi_eq.pkl"))
    #print(qe.vec_fix)
      

###############################################################################
#   PLOT FUNCTIONS  
###############################################################################

def plot_stack(ax):
    '''
    Stacked abundances
    '''
    
    # Graph
    ax.stackplot(traj.vec_t, traj.mat_x.T, colors=colors, rasterized=False)
    
    # Format
    ax.set_xticks(range(0,T+1,50))
    ax.set_yticks(range(0,4))
    ax.tick_params(labelbottom=False)
    ax.margins(x=0)
    
    # Text
    ax.set_ylabel("abundance")
    #ax.set_title(f"{fmt.A}                  Stacked abundance time series                          ")#,horizontalalignment='right')
    ax.set_title(f"Stacked abundance time series")#,horizontalalignment='right')
    fmt.letter('A', -0.14, 1.05, ax)


# middle panel for appendix version
def plot_equilibrium_panel(ax, rel=False):
    # Graph
    mat_fp =  glv.obs.relative_abundance(qe.mat_xfp) if rel else qe.mat_xfp
    ax.stackplot(traj.vec_t,mat_fp.T, colors=colors)
    
    # Format 
    ax.set_xticks(range(0,T+1,50))
    ax.tick_params(labelbottom=False)
    ax.margins(x=0)
    
    # Text
    if rel:
        ax.set_ylabel("rel. abd.")
    else:
        ax.set_ylabel("abundance")

    ax.set_title("equilibrium when rare species removed")


def plot_closeness_panel(ax):
    #Graph
    ax.plot(traj.vec_t, qe.vec_bc[0:T], color='darkblue')
    
    # Format
    ax.set_ylim([0,1])
    ax.set_yticks([0,1],["0%","100%"])
    ax.margins(x=0)


    # Text
    ax.set_title(f"Similarity to few-species equilibrium")
    fmt.letter('B', -0.14, 1.3, ax)
    ax.set_xlabel("time")


###############################################################################
#   MAKE FIGURE  
###############################################################################


fmt.apply_defaults()

fig = plt.figure(constrained_layout=True)
fmt.adjust_fig_size(fig, aspect_ratio=4/3)

gs = GridSpec(2, 1, figure=fig, height_ratios=[1, 0.2])

ax = fig.add_subplot(gs[0, 0])
plot_stack(ax)
ax = fig.add_subplot(gs[1, 0])
plot_closeness_panel(ax)

fmt.savefig(fig, subpath("fig1_turnover"))


gt.stop()
