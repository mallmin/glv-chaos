
import os, sys
sys.path.append('./')
sys.path.append('./figurecode/')

from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.gridspec import GridSpecFromSubplotSpec
from timer import Timer
import argparse
import glv
import myutils as utl
import figformat as fmt
import palettable


parser = argparse.ArgumentParser()
parser.add_argument("-g", action="store_true")
args = parser.parse_args()

tr = glv.track("Figure")


###############################################################################
#   LOAD DATA / SET VARIABLES
###############################################################################


# Data for spike plot

traj = glv.load_observable("data/traj-ref/traj_ref_org.pkl")
dom = glv.load_observable("data/traj-ref/dom_ref.pkl")
afd = glv.load_observable("data/traj-ref/afd_ref.pkl")
vec_typicality = afd.vec_typicality

idxs_ranked = glv.rank_idx(dom.vec_domfrac)
first_mid_last = idxs_ranked[[0,round(traj.S/2 - 1),-1]]
mat_x = traj.mat_x[:, first_mid_last]
vec_t = traj.vec_t
del traj, dom, afd

# Data for dominance time 

S_range = np.array([320, 640, 1280, 2560, 5120, 10240])
ls_medians = []
ls_rank_scaled = []
ls_domfrac_scaled = []

for S in S_range:
    dom = glv.load_observable(f"data/traj-S-range/dom_S={S}.pkl")
    
    m = np.median(dom.vec_domfrac)
    ls_medians.append(m)
    
    vec_r, vec_f = glv.ranks(dom.vec_domfrac)
    vec_r = vec_r / S
    vec_f = vec_f / m
    ls_rank_scaled.append(vec_r)
    ls_domfrac_scaled.append(vec_f)

    del dom

# # Data for interaction
dom = glv.load_observable("data/traj-ref/dom_ref.pkl")
afd = glv.load_observable("data/traj-ref/afd_ref.pkl")

vec_typicality = afd.vec_typicality
vec_f = dom.vec_domfrac / np.median(dom.vec_domfrac)
vec_z = dom.vec_z.copy()

del dom, afd


###############################################################################
#   PANEL PLOT FUNCTIONS
###############################################################################

def plot_series(ax):
   
    ax.set_prop_cycle(color=fmt.COLORS)
    ax.plot(vec_t, mat_x, label=["first rank","mid rank","last rank"], lw=1)

    # Format 
    ax.margins(x=0, y=0)
    ax.set_xticks([20000,40000,60000,80000],["20k","40k","60k","80k"])
    ax.set_yticks([0,0.5,1])

    # Text
    leg = ax.legend(frameon=True, fancybox=False, framealpha=0.8, fontsize=fmt.FONT_SIZE_SMALL,
              loc='upper right') 
    leg.get_frame().set_linewidth(0.0)
    ax.set_title(f"Differences in 'boom' frequency over long time")# ($S=500$, $\lambda=1e-8$)")
    #ax.set_xlabel("time")
    ax.set_ylabel("abundance")

def plot_median(ax):
        colors = palettable.colorbrewer.sequential.PuRd_8.mpl_colors[::-1][:6][::-1]

        f = glv.linear_fit(np.array(ls_medians), 1/S_range)
        c = f['c']
        k = f['k']

        ax.plot(1/S_range, 1/S_range * k + c, color='k', linestyle = 'dashed',lw=0.5, zorder=-1)
        ax.scatter(1/S_range, ls_medians, s=8, c=colors)
        
        # Format
        ax.set_xticks([])
        ax.set_xticks([0, 1/1000, 1/500, .003], ["0",".001", ".002",""], minor=False)
        ax.set_yticks([])
        ax.set_yticks([0, 0.01, 0.02], ["0",".01", ".02"], minor=False)
        
        # Text
        ax.set_xlabel("$1/S$")
        #ax.set_ylabel("median\ndominance time")
        ax.set_ylabel("median")

def plot_rank(ax):

    colors = palettable.colorbrewer.sequential.PuRd_8.mpl_colors[::-1][:6][::-1]
    ax.set_prop_cycle('color',  colors)
    
    for i,S in enumerate(S_range):
        
        ax.plot(ls_rank_scaled[i], ls_domfrac_scaled[i], lw=1, label=f"S={S}")
    
    # Format
    ax.margins(x=0.05, y=0.05)
    ax.set_xlim(ax.get_xlim())
    ax.set_ylim([0, ax.get_ylim()[1]])
    ax.set_xticks([0,.5,1])
    
    ax.plot(ax.get_xlim(), [1,1],color='k',linestyle='dashed', lw=0.5)

    # Text
    #ax.set_xlabel("rank / $S$")
    ax.set_xlabel("relative rank")
    #ax.set_ylabel("dominance time \n/ median")
    ax.set_ylabel("dominance bias")
    ax.legend(frameon=False,prop={'size': fmt.FONT_SIZE_SMALL})


def plot_spread(ax, fig):
    
    sc = ax.scatter(vec_z, vec_f, s=0.3, cmap="magma_r", c=vec_typicality, vmax=1)

    axbar = ax.inset_axes([0.55, 0.83, 0.4,0.08])
    cb = ax.figure.colorbar(sc, cax=axbar, orientation="horizontal")
    cb.set_label(r"typicality $\theta_i$")
    #cb.set_ticks([0.95,1.],["0.95","1"])

    # Format
    ax.margins(x=0.05, y=0.05)
    
    # Text
    ax.set_title(f"Role of total interactions")
    ax.set_xlabel(r"net interaction bias $z_i$")
    ax.set_ylabel("dominance bias")
    #ax.set_ylabel("dominance time \n/ median")


###############################################################################
#   MAKE FIGURE
###############################################################################


fmt.apply_defaults()

fig = plt.figure()
gs = GridSpec(2, 3, figure=fig, height_ratios=[0.7, 1], width_ratios=[0.8,0.8,1])
fmt.adjust_fig_size(fig, 0.7, aspect_ratio=2)

#  A - Spike plot
ax = fig.add_subplot(gs[0, :])
plot_series(ax)
ax.text(0.02, -0.24, "time", transform=ax.transAxes)

# B & C - Dominance time
ax2 = fig.add_subplot(gs[1, :2])
ax2.set_frame_on(False)
ax2.axis('off')
ax21 = ax2.inset_axes([0,0,0.42,1])
ax22 = ax2.inset_axes([0.6,0,0.5,1])
ax21.set_title("Scaling of median dom. time")
ax22.set_title("Limiting dist. of biases")

#ax2.set_title(f"Dominance time versus $S$")
plot_median(ax21)
plot_rank(ax22)

# Interactions
ax = fig.add_subplot(gs[1, 2])
plot_spread(ax, fig)

fig.tight_layout()

# Place figure letters
fmt.place_A(0.025, 0.95)
fmt.place_B(0.025, 0.545)
fmt.place_C(0.38, 0.545)
fmt.place_D(0.7, 0.545)

# Save
fmt.savefig(fig, subpath("fig4_bias"))



tr.end()