import os, sys
sys.path.append('./')
sys.path.append('./figurecode')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
import myutils as utl
import glv
from matplotlib import colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import myutils
import figformat as fmt


###############################################################################
#   LOAD DATA  
###############################################################################

T = 1000

# Make systems

# Reference system
g1 = glv.DisorderedLVSystem(S=500, lam=10**-8)
g1.set_gaussian_interactions(seed=12345)
g1.scheme = 'log'
traj1 = g1.trajectory(steps=100*T, transient=100*1000, record_interval=100)
vec_x0 = traj1.mat_x[0,:]

# Different integration scheme
g2 = glv.DisorderedLVSystem(S=500, lam=10**-8)
g2.mat_alpha = g1.mat_alpha
g2.scheme = 'logistic'
g2.vec_x = vec_x0
traj2 = g2.trajectory(steps=100*T, transient=0, record_interval=100)

# Perturbed interactions
g3 = glv.DisorderedLVSystem(S=500, lam=10**-8)
g3.mat_alpha = g1.mat_alpha + 1e-6 * glv.gaussian_random_matrix(g3.S,0,1,seed=12121)
g3.scheme = 'log'
g3.vec_x = vec_x0
traj3 = g3.trajectory(steps=100*T, transient=0, record_interval=100)

# Perturbed initial condition
g4 = glv.DisorderedLVSystem(S=500, lam=10**-8)
g4.mat_alpha = g1.mat_alpha
g4.scheme = 'log'
g4.vec_x = vec_x0 + 1e-8 * np.random.random(g3.S)
traj4 = g4.trajectory(steps=100*T, transient=0, record_interval=100)


###############################################################################
#   MAKE FIGURE  
###############################################################################

fig, axs = plt.subplots(5,1)
fmt.adjust_fig_size(fig, fmt.DOUBLE_COL_REL_PAGE_WIDTH, 4/3)

fmt.apply_defaults()

colors = traj1.colors()

axs[0].stackplot(traj1.vec_t, traj1.mat_x.T, colors=colors)
axs[1].stackplot(traj2.vec_t, traj2.mat_x.T, colors=colors)
axs[2].stackplot(traj3.vec_t, traj3.mat_x.T, colors=colors)
axs[3].stackplot(traj4.vec_t, traj4.mat_x.T, colors=colors)

axs[0].set_title("Sensitive dependence on model details")
axs[2].set_ylabel("stacked abd.")

for ax in axs:
    ax.margins(x=0)

labels = ["reference","different integration scheme","perturbed interactions","purturbed initial condition"]
label_color = ['k',fmt.COLORS[0],fmt.COLORS[1],fmt.COLORS[2]]

for i, ax in enumerate(axs[0:4]):
    #t = ax.text(0.7,0.8, labels[i], transform=ax.transAxes)
    #t.set_bbox(dict(facecolor='red', alpha=0.5, edgecolor='red'))

    ax.annotate(labels[i], xy=(1, 1), xytext=(-10, -37), xycoords='axes fraction', textcoords='offset points', fontsize=fmt.FONT_SIZE_MEDIUM, color=label_color[i],
        bbox=dict(facecolor='white', alpha=0.9, edgecolor='none'), horizontalalignment='right', verticalalignment='top')

    ax.tick_params(labelbottom=False)
    ax.set_ylim([0,3.2])

vec_bc2 = glv.metrics.bray_curtis(traj1.mat_x, traj2.mat_x)
vec_bc3 = glv.metrics.bray_curtis(traj1.mat_x, traj3.mat_x)
vec_bc4 = glv.metrics.bray_curtis(traj1.mat_x, traj4.mat_x)

ax = axs[4]
ax.plot(vec_bc2, label=labels[1], color=fmt.COLORS[0])
ax.plot(vec_bc3, label=labels[2], color=fmt.COLORS[1])
ax.plot(vec_bc4, label=labels[3], color=fmt.COLORS[2])
ax.set_ylabel("index")
ax.set_xlabel("time")
ax.annotate("Bray-Curtis similarity to reference", xy=(1, 1), xytext=(-10, -10), xycoords='axes fraction', fontsize=fmt.FONT_SIZE_MEDIUM, textcoords='offset points', color='k',
        bbox=dict(facecolor='white', alpha=0.9, edgecolor='none'), horizontalalignment='right', verticalalignment='top')

x = 0.13
fmt.place_A(x, 0.85)
fmt.place_B(x, 0.685)
fmt.place_C(x, 0.53)
fmt.place_D(x, 0.37)
fmt.place_E(x, 0.21)


fmt.savefig(fig, subpath("figS_sensitivity"))

