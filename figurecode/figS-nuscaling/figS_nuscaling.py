
import os, sys
sys.path.append('./')
sys.path.append('./figurecode/')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import argparse
import glv
import figformat as fmt
import palettable
from mpl_toolkits.axes_grid1 import make_axes_locatable


gt = utl.Timer("Script").start()

###############################################################################
#   LOAD DATA  
###############################################################################


S_range = [320, 640, 1280, 2560, 5120, 10240]
lam_exponents = [8,12,16,20,24,28,32,128]
lam_range = 10.**-np.array(lam_exponents)

nus_S = [ -glv.load_observable(f"data/traj-S-range/afd_S={S}.pkl").nu for S in S_range]
nus_lam = [-glv.load_observable(f"data/traj-lam-range/afd_lam=1e-{lam_x}.pkl").nu for lam_x in lam_exponents]


###############################################################################
#   PLOT PANELS  
###############################################################################

def plot_S(ax):
    # Graph
    #ax.plot(S_range, nus_S, color='k', linestyle='-', marker='o', lw=2, markersize=4)
    
    ax.plot(S_range, nus_S, color='k', zorder=-1)
    colors = palettable.colorbrewer.sequential.PuRd_8.mpl_colors[::-1][:len(S_range)][::-1]
    ax.scatter(S_range, nus_S, c=colors)


    # Format
    ax.set_xscale('log')
    #ax.set_yticks([1, 1.2, 1.4])
    #ax.set_yticks([1.1, 1.3], [], minor=True)
    #ax.set_xlim([0.8*10**2, ax.get_xlim()[1]])


    # Text
    #ax.set_title(r"$\nu$ vs $\log S$")
    ax.set_title(r"Scaling of $\nu$ vs $S$")
    ax.set_xlabel("$S$")
    ax.set_ylabel(r"$\nu$")

def plot_lam(ax):
    # Graph
    #ax.plot(- 1 / np.log(lam_range), nus_lam, color='k', linestyle='-', marker='o', lw=2, markersize=4)

    ax.plot(- 1 / np.log(lam_range), nus_lam, color='k', zorder=-1)
    
    colors = palettable.colorbrewer.sequential. YlGn_8.mpl_colors[::-1][:len(lam_range)]#[::-1]
    ax.scatter(- 1 / np.log(lam_range), nus_lam, c=colors)
    

    ax.plot([0, - 1 / np.log(lam_range[0])],[1, nus_lam[0]], color='grey', linestyle='dashed', zorder=-1)


    # Format 
    ax.set_xlim([0,0.06])
    ax.set_ylim([1,1.18])
    ax.set_xticks([0, .025, .050], ["0", "0.025", "0.05"])
    #ax.set_yticks([1, 1.2])
    #ax.set_yticks([1.1, 1.3], [], minor=True)

    # Text
    ax.set_title(r"Scaling of $\nu$ with $\lambda$")
    ax.set_xlabel("$-1/\log(\lambda)$")
    ax.set_ylabel(r"$\nu$")


###############################################################################
#   MAKE FIGURE  
###############################################################################

fig, (ax1, ax2) = plt.subplots(1,2)

fmt.adjust_fig_size(fig, 0.8*fmt.DOUBLE_COL_REL_PAGE_WIDTH, 2.5)
fmt.apply_defaults()


plot_S(ax1)
plot_lam(ax2)

fig.tight_layout()

fmt.place_A(0.05,0.92)
fmt.place_B(0.51,0.92)

fmt.savefig(fig, subpath("figS_nuscaling"))



gt.stop()



