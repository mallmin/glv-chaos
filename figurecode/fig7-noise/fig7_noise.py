import os, sys
sys.path.append('./')
sys.path.append('./figurecode')

from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import glv
from matplotlib import colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import figformat as fmt

###############################################################################
#   LOAD DATA  
###############################################################################

traj = glv.load_observable("./data/traj-ref/traj_ref_org.pkl")

eta = glv.focal.EffectiveNoise()
eta.from_trajectory(traj)

vec_mean = np.mean(traj.mat_x, axis=0)
idxs_ranked = np.argsort(vec_mean)[::-1]
first_ranked = idxs_ranked[0]
mid_ranked = idxs_ranked[round(traj.S/2 - 1)]
last_ranked = idxs_ranked[-1]

###############################################################################
#   PLOT FUNCTIONS  
###############################################################################

def plot_dist(axl, axr):
    '''
    Plots the following
    axl:
        noise distribution across all time and species
        noise distribution for one arbitrary time    
    axr:     
        overlapping noise distributions for first-, mid-, and last-ranked species
    '''

    for ax in [axr,axl]:
        ax.margins(x=0)
        ax.set_xlim([-3,3])
        ax.set_ylim([0,0.5])

    i = [first_ranked, mid_ranked, last_ranked]
    t = 10000


    bins = np.linspace(-3,3,25)
    bins_mid = (bins[1:] + bins[:-1] ) / 2
    vec_gauss = np.exp(- bins_mid**2 / 2 )/ np.sqrt(2*np.pi)
    
    # LEFT PLOT
    ax = axl
    ax.hist(eta.mat_eta.flatten(), bins=200, density=True, color='lightgrey')
    ax.hist(eta.mat_eta[t,:], bins=bins, density=True, color='palegreen', alpha=0.8)
    ax.set_title(r"$\eta$ snapshot distr.")

    # RIGHT PLOT
    ax = axr
    alpha=0.8
    ax.hist(eta.mat_eta[:, i[0]], bins = bins, density=True, color=fmt.cmap_no1_3(0/3), alpha=alpha)#, label="first")
    ax.hist(eta.mat_eta[:, i[1]], bins = bins, density=True, color=fmt.cmap_no1_3(1/3), alpha=alpha)#, label="mid")
    ax.hist(eta.mat_eta[:, i[2]], bins = bins, density=True, color=fmt.cmap_no1_3(2/3), alpha=alpha)#, label="last")
    ax.plot(bins_mid, vec_gauss, color='k', linestyle='dashed')
    ax.set_yticks([])
    ax.set_title(r"$\eta_i$ distr.")
    ax.scatter([],[], color=fmt.cmap_no1_3(0/3), label="first", marker='s', s=2)
    ax.scatter([],[], color=fmt.cmap_no1_3(1/3), label="mid", marker='s', s=2)
    ax.scatter([],[], color=fmt.cmap_no1_3(2/3), label="last", marker='s', s=2)
    ax.legend(frameon=False,loc='upper right', borderpad=-0.1, handletextpad=-0.02)
    

def plot_ab(axl, axr):
    '''
    Plots relative fluctuations in a(t) and b(t)
    '''

    for ax in [axr,axl]:
        ax.margins(x=0)

    t = 0
    T = 1000
    vec_a_rel = eta.vec_a / np.mean(eta.vec_a) - 1
    vec_b_rel = eta.vec_b / np.mean(eta.vec_b) - 1

    print(np.amax(eta.vec_a))

    col1 = 'lightcoral'
    col2 = 'brown'

    ax = axl
    ax.plot(traj.vec_t[:T], vec_a_rel[t:t+T], color=col1)
    ax.plot(traj.vec_t[:T], vec_b_rel[t:t+T], color=col2)
    ax.set_ylim([-1.2,1.2])
    ax.set_title(r"$a_{rel},b_{rel}$ series")

    ax = axr
    ax.hist(vec_a_rel,bins=25,density=True, color=col1, alpha=0.7)
    ax.hist(vec_b_rel,bins=25,density=True, color=col2, alpha=0.7)
    ax.set_title(r"$a_{rel},b_{rel}$ distr.")
    
    ax.scatter([],[], color=col1, label=r"$a_{rel}$", marker='s', s=2)
    ax.scatter([],[], color=col2, label=r"$b_{rel}$", marker='s', s=2)
    ax.legend(frameon=False,loc='upper right',handletextpad=0.05)



def plot_acf(ax):
    lags=500
    vec_t = traj.vec_t[:lags]
    colors=glv.rand_cmap(traj.S, type='soft')
    for i in range(500):        
        ax.plot(vec_t, eta.mat_acf[:lags,i], color='lightgrey', alpha=0.3)
    ax.plot(vec_t, 0 * vec_t, color='k', lw=0.5)

    ax.plot(vec_t, eta.mat_acf[:lags,first_ranked], color=fmt.cmap_no1_3(0/3))#, lw=.5)
    ax.plot(vec_t, eta.mat_acf[:lags,mid_ranked], color=fmt.cmap_no1_3(1/3))#, lw=.5)
    ax.plot(vec_t, eta.mat_acf[:lags,last_ranked], color=fmt.cmap_no1_3(2/3))#, lw=.5)
    ax.plot(vec_t, eta.vec_acf, color='k')

    ax.margins(x=0)

    ax.set_title(r"Autocorrelation function of the noise")
        

def plot_acf_fit(ax):
    lags=300
    vec_t = traj.vec_t[:lags]
    vec_exp = np.exp(- vec_t / eta.tau)

    # autocorr x
    mat_acf = glv.corr.autocorr_fft(traj.mat_x, lags=lags, target='cpu')
    vec_std = np.std(traj.mat_x,axis=0)
    vec_w = vec_std / np.mean(vec_std) 
    vec_acf_x = np.mean(mat_acf * vec_w, axis=1)

    ax.plot(vec_t, vec_acf_x, color='chartreuse',lw=1, label=r"$\phi_x$")
    ax.plot(vec_t, eta.vec_acf[:lags], color='k',lw=1, label=r"$\phi_\eta$")

    ax.plot(vec_t, vec_exp, color='r', linestyle='dashed', label=r"$e^{-t/\tau}$ fit", lw=1)
    #ax.set_yscale("log")
    ax.legend(frameon=False, loc='upper right',borderpad=-0.1)
    ax.margins(x=0)

    #ax.set_tilte("")

def plot_acf_x(ax):
    lags=500
    vec_t = traj.vec_t[:lags]

    #acf of mat_x
    mat_acf = glv.corr.autocorr_fft(traj.mat_x, lags=lags, target='cpu')
    vec_var = np.std(traj.mat_x,axis=0)
    vec_w = vec_var / np.mean(vec_var) 
    vec_acf = np.mean(mat_acf * vec_w, axis=1)
    edf = glv.fit.exponential_decay_fit(vec_acf, vec_t)

    ax.plot(vec_t, eta.vec_acf)
    ax.plot(vec_t, vec_acf)
    ax.plot(vec_t, eta.vec_acfw)


def plot_tau(ax):
    vec_tau = eta.all_tau(300)
    ax.hist(vec_tau, bins=25, color='lightgray', density=True)
    ymax=0.2
    ax.set_ylim([0,ymax])

    ax.plot(np.array([1,1]) * vec_tau[first_ranked], [0,1.1*ymax], color=fmt.cmap_no1_3(0/3), linestyle='dashed')
    ax.plot(np.array([1,1]) * vec_tau[mid_ranked], [0,1.1*ymax], color=fmt.cmap_no1_3(1/3), linestyle='dashed')
    ax.plot(np.array([1,1]) * vec_tau[last_ranked], [0,1.1*ymax], color=fmt.cmap_no1_3(2/3), linestyle='dashed')
    ax.plot(np.array([1,1]) * eta.tau, [0,60], color='k', linestyle='dashed')

    ax.margins(x=0)

    ax.set_title(r"$\tau$ fit dist", fontsize=6)


def plot_full_dist(ax):
    vec_g = 1 - traj.mat_x @ traj.mat_alpha.T
    print(vec_g.shape)

    lim1 = -eta.k - 4 * eta.u
    lim2 = -eta.k + 4 * eta.u
    vec_x = np.linspace(lim1,lim2,100)
    vec_g_approx = np.exp(- (vec_x + eta.k)**2 / (2*eta.u * eta.u)) / np.sqrt(2*np.pi*eta.u * eta.u)

    bins = np.linspace(lim1,lim2,40)
    ax.hist(vec_g.flatten(), bins=bins, density=True, color='steelblue', label="$g$ (all times\n& species)")
    ax.plot(vec_x, vec_g_approx, color='k', label=r"$\mathcal{N}(\bar{a}, \bar{b})$")
    ax.legend(frameon=False)
    ax.margins(x=0)

    ax.set_title(r"Empirical dist. of $g$ vs Gaussian approx.")
    ax.set_xlim([lim1,lim2])
    ax.set_xticks(np.arange(-1.2,lim2,0.3))


###############################################################################
#   MAKE FIGURE  
###############################################################################

# fig, ax = plt.subplots()
# plot_full_dist(ax)
# plt.show()

# quit()

fmt.apply_defaults()

fig = plt.figure()
fmt.adjust_fig_size(fig, fmt.SINGLE_COL_REL_PAGE_WIDTH, 0.58) #0.75

gs = GridSpec(4, 6, figure=fig, height_ratios=[0.7,1,1,1.9])

ax11 = fig.add_subplot(gs[0, :4])
ax12 = fig.add_subplot(gs[0, 4:])

plot_ab(ax11, ax12)


ax21 = fig.add_subplot(gs[1, 0:3])
ax22 = fig.add_subplot(gs[1, 3:])

plot_dist(ax21, ax22)

ax3 = fig.add_subplot(gs[2, :])
plot_full_dist(ax3)

ax4 = fig.add_subplot(gs[3, :])

plot_acf(ax4)

dx = 0.32
dy = 0.38

axi = ax4.inset_axes([0.2,0.46,0.32,0.4])
plot_acf_fit(axi)

axi = ax4.inset_axes([0.65,0.46,0.32,0.4])
plot_tau(axi)


# ax = fig.add_subplot(gs[0, 1])
# plot_dist(ax)

xl=0.02
y1=0.96
y2=0.77
fmt.place_A(xl,y1)
fmt.place_B(0.66,y1)
fmt.place_C(xl,y2)
fmt.place_D(0.53,y2)
fmt.place_E(xl,0.56)
fmt.place_F(xl, 0.34)

fig.tight_layout()

fmt.savefig(fig, subpath("fig7_noise"))

