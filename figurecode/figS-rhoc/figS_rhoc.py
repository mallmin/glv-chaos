import os, sys
sys.path.append('./')
sys.path.append('./figurecode')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import argparse
import glv
from matplotlib import colors
import numpy.ma as ma
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1 import make_axes_locatable

import figformat as fmt

###############################################################################
#   LOAD DATA  
###############################################################################

df = pd.read_csv("data/phase-chaos/data.csv")

df['nu_err'] = (df['nu_eff'] - df['nu'] ) / df['nu']

df['rho_c'] = (1 - df['mu']) / df['sig'] / df['Neff']

df['rho_norm'] = df['rho'] / df['rho_c']


###############################################################################
#   PLOT PANELS  
###############################################################################

def plot_panel(ax, prop, title, vmin=None, vmax=None, extend=None, left=False, bottom=True, log=False):
    X,Y,Z = glv.phase.meshgridXYZ(df, 'mu', 'sig', prop)
    if log:
        Z = np.log10(Z)
    pc = ax.pcolormesh(X,Y,Z, cmap='viridis', vmin=vmin, vmax=vmax)
    cax = make_axes_locatable(ax).append_axes("right", size="5%", pad=0.05)
    cb = ax.figure.colorbar(pc, orientation="vertical", cax=cax, extend=extend)
    
        
    ax.set_title(title)
    ax.set_aspect('equal', adjustable='box')
    ax.margins(x=0,y=0)
    ax.set_xlim([-0.1,1.0])
    #ax.set_xticks(np.arange(0,1.2,0.2))
    ax.set_ylim([0,0.6])
    #ax.set_yticks(np.arange(0,0.6,0.2))
    if bottom:
        ax.set_xlabel(r"$\mu$")
    else:
        ax.tick_params(labelbottom=False)
    if left:
        ax.set_ylabel(r"$\sigma$")
    else:
        ax.tick_params(labelleft=False)
    return cb

def plot_rho(ax): plot_panel(ax, 'rho', r"$\overline{\rho}$", left=True)
def plot_rho_c(ax): plot_panel(ax, 'rho_c', r"$\rho_c$")
def plot_rho_norm(ax): 
    cb = plot_panel(ax, 'rho_norm', r"$\overline{\rho}/\rho_c$", None,5, extend='max')
    cb.ax.set_yticks(np.arange(1,5))

###############################################################################
#   MAKE FIGURE  
###############################################################################

fig, (ax1, ax2, ax3) = plt.subplots(1,3)
fmt.adjust_fig_size(fig, 1*fmt.DOUBLE_COL_REL_PAGE_WIDTH, 2.7)

plot_rho(ax1)
plot_rho_c(ax2)
plot_rho_norm(ax3)

y = 0.7
fmt.place_A(0.03,y)
fmt.place_B(0.38, y)
fmt.place_C(0.7, y)

fig.tight_layout()

fmt.savefig(fig, subpath("figS_rhoc"))