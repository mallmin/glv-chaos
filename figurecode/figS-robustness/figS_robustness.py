'''
Figure showing chaotic dynamics for a variety of model variations
'''

import os, sys
sys.path.append('./')
sys.path.append('./figurecode/')
from os.path  import join as joinpath
currentdir = os.path.dirname(os.path.realpath(__file__))
def subpath(*args): return joinpath(currentdir, *args)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import myutils as utl
import glv
import argparse
import figformat as fmt

tr = glv.Tracker().start("Script")

parser = argparse.ArgumentParser()
parser.add_argument("-g", "--generate", action="store_true")
#parser.add_argument("-o", "--output")
args = parser.parse_args()


transient = 1000
T = 10000

if args.generate:

    tr1 = tr.start_sub("Growth rates")
    # Different growth rates
    g1 = glv.model.DisorderedLVSystem(S=500, lam=10**-8)
    g1.set_gaussian_interactions(seed=12345)
    g1.vec_r = np.random.random(g1.S)
    traj1 = g1.trajectory(steps=100*T, transient=100*transient, record_interval=100)
    tr1.end()

    # Different carrying capacity
    tr2 = tr.start_sub("Carry cap")
    g2 = glv.model.DisorderedLVSystem(S=500, lam=10**-8)
    g2.set_gaussian_interactions(seed=12345)
    g2.vec_K = np.exp( 0.1*  np.random.standard_normal(size=g2.S) )
    traj2 = g2.trajectory(steps=100*T, transient=100*transient, record_interval=100)
    tr2.end()

    # Sparsity of interactions
    tr3 = tr.start_sub("Sparsity")
    g3 = glv.model.DisorderedLVSystem(S=500, lam=10**-8)
    p = 0.1
    mu_eff = 0.5
    sig_eff = 0.3
    mu = mu_eff #/ p
    #sig = np.sqrt( sig_eff**2 / p - (1-p)*((mu_eff / p)**2) ) 
    g3.set_gaussian_interactions(mu=0.5, sig=0.3, seed=12345)
    mat_c = np.random.choice(2,size=500*500, p=np.array([1-p,p])).reshape(500,500)
    g3.mat_alpha = g3.mat_alpha * mat_c
    print(np.mean(g3.mat_alpha))
    print(np.std(g3.mat_alpha))
    traj3 = g3.trajectory(steps=100*T, transient=100*transient, record_interval=100)
    tr3.end()

    # Gamma > 0
    tr4 = tr.start_sub("Gamma > 0")
    g4 = glv.model.DisorderedLVSystem(S=500, lam=10**-8)
    g4.set_gaussian_interactions(mu=0.5, sig=0.3, gam=0.2, seed=None)
    traj4 = g4.trajectory(steps=100*T, transient=100*transient, record_interval=100)
    tr4.end()

    # Gamma < 0 
    tr5 = tr.start_sub("Gamma < 0")
    g5 = glv.model.DisorderedLVSystem(S=500, lam=10**-8)
    g5.set_gaussian_interactions(mu=0.5, sig=0.3,gam=-0.3, seed=None)
    traj5 = g5.trajectory(steps=100*T, transient=100*transient, record_interval=100)
    tr5.end()

    traj1.save(subpath("traj1.pkl"))
    traj2.save(subpath("traj2.pkl"))
    traj3.save(subpath("traj3.pkl"))
    traj4.save(subpath("traj4.pkl"))
    traj5.save(subpath("traj5.pkl"))

else:
    trl = tr.start_sub("Loading")
    traj1 = glv.load_observable(subpath("traj1.pkl"))
    traj2 = glv.load_observable(subpath("traj2.pkl")) 
    traj3 = glv.load_observable(subpath("traj3.pkl")) 
    traj4 = glv.load_observable(subpath("traj4.pkl")) 
    traj5 = glv.load_observable(subpath("traj5.pkl")) 
    trl.end()

colors = traj1.colors()
fig, axs = plt.subplots(5,1)

fmt.apply_defaults()
fmt.adjust_fig_size(fig, fmt.DOUBLE_COL_REL_PAGE_WIDTH, 4/3)


trp = tr.start_sub("Plotting")
axs[0].stackplot(traj1.vec_t, traj1.mat_x.T, colors=colors)
axs[1].stackplot(traj2.vec_t, traj2.mat_x.T, colors=colors)
axs[2].stackplot(traj3.vec_t, traj3.mat_x.T, colors=colors)
axs[3].stackplot(traj4.vec_t, traj4.mat_x.T, colors=colors)
axs[4].stackplot(traj5.vec_t, traj5.mat_x.T, colors=colors)
trp = tr.end()

axs[0].set_title("Chaotic dynamics for a variety of model assumptions")
axs[2].set_ylabel("stacked abd.")
for ax in axs:
    ax.margins(x=0)
labels = [r"random growth rates $r_i$",r"random carry. cap. $K_i$",r"sparsity of interactions", r"Symmetry $\gamma=0.2$",r"Anti-symmetry $\gamma=-0.3$"]
#label_color = ['k',fmt.COLORS[0],fmt.COLORS[1],fmt.COLORS[2]]
for i, ax in enumerate(axs):
    ax.annotate(labels[i], xy=(1, 1), xytext=(-10, -10), xycoords='axes fraction', textcoords='offset points', color='k',
        bbox=dict(facecolor='white', alpha=0.7, edgecolor='none'), horizontalalignment='right', verticalalignment='top')
    
    if ax != axs[-1]:
        ax.tick_params(labelbottom=False)

x = 0.13
fmt.place_A(x,0.86)
fmt.place_B(x,0.7)
fmt.place_C(x,0.54)
fmt.place_D(x,0.38)
fmt.place_E(x,0.22)

fmt.savefig(fig, subpath("figS_robustness"))
